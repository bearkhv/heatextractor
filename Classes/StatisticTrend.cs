﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
    public class StatisticTrend
    {
        public double C { get; set; }
        public double T { get; set; }
        public double TempInBoiler { get; set; }
        public double DeltaTempinBoiler { get; set; }
        public double DeltaTempInBlow { get; set; }
        public DateTime Time { get; set; }
        public DateTime TimeC { get; set; }
        public double UniversalCPlusResult { get; set; }

    }
}
