﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
    /// <summary>
    /// Замер
    /// </summary>
    public class Zond
    {
        /// <summary>
        /// Старт зонда
        /// </summary>
        public DateTime ZondStart { get; set; }
        /// <summary>
        /// Углерод зонд
        /// </summary>
        public double ZondC { get; set; }
        /// <summary>
        /// Температура зонда
        /// </summary>
        public double ZondTemp { get; set; }

        public double Oxigen;
    }
}
