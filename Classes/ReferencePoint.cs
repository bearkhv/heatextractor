﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
   public class ReferencePoint
    {
      /// <summary>
      /// Время записи в БД
      /// </summary>
      public DateTime TimeRefPoint { get; set; }
      /// <summary>
      /// Время от начала момента продувки в сек.
      /// </summary>
      public int O2 { get; set; }
      /// <summary>
      /// С, кг
      /// </summary>
      public double C { get; set; }
    }
}
