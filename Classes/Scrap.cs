﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
    /// <summary>
    /// Скрап
    /// </summary>
    public class Scrap
    {
        /// <summary>
        /// Время завалки лома
        /// </summary>
        public DateTime Time { get; set; }

        public int ScrapNo { get; set; }
        /// <summary>
        /// Состав лома
        /// </summary>
        public string ScrapName { get; set; }

        public string ScrapType { get; set; }
        /// <summary>
        /// Вес долей лома
        /// </summary>
        public double ScrapWaight { get; set; }
        /// <summary>
        /// Идентификатор совка
        /// </summary>
        public int BucketId { get; set; }
    }
}
