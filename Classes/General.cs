﻿using System;
using System.Collections.Generic;

namespace HeatExtractor.Classes
{
    /// <summary>
    /// Общая таблица
    /// </summary>
    public class General
    {
        /// <summary>
        /// № Плавки
        /// </summary>
        public int HeatNumber { get; set; }
        /// <summary>
        ///	Время плавки
        /// </summary>
        public Duration HeatDuration { get; set; }
        /// <summary>
        /// Марка стали
        /// </summary>
        public string Grade { get; set; }

        public bool IsDynamicGradeSteel
        {
            get { return Grade == "Д3Ю" || Grade == "Д3ЮП" || Grade == "2212" || Grade == "2216" || Grade == "М-450-50Е"; }
        }

        public string GradeGroup;
        /// <summary>
        /// Режим работы фурмы
        /// </summary>
        public int LanceMode { get; set; }
        /// <summary>
        /// Режим работы фурмы
        /// </summary>
        public int AvtoDovodka { get; set; }
        /// <summary>
        /// Режим работы тракта
        /// </summary>
        public int TractMode { get; set; }
        /// <summary>
        /// Целевые на плавку: углерод (С)
        /// </summary>
        public double TargetC { get; set; }
        /// <summary>
        /// Целевые на плавку: фосфор (P)
        /// </summary>
        public double TargetP { get; set; }
        /// <summary>
        /// Целевые на плавку: сера(S)
        /// </summary>
        public double TargetS { get; set; }
        /// <summary>
        /// Целевые на плавку: (Cr)
        /// </summary>
        public double TargetCr { get; set; }	
        /// <summary>
        ///	Целевые на плавку: (Ni)
        /// </summary>
        public double TargetNi { get; set; }
        /// <summary>
        /// Целевые на плавку: (Cu)
        /// </summary>
        public double TargetCu { get; set; }	
        /// <summary>
        /// Целевые на плавку: (Mn)
        /// </summary>
        public double TargetMn { get; set; }
	    /// <summary>
	    /// Целевые на плавку: (MgO)
	    /// </summary>
        public double TargetMgO { get; set; }
        /// <summary>
        /// Целевые на плавку: (FeO)
        /// </summary>
        public double TargetFeO { get; set; }
        /// <summary>
        /// Целевые на плавку: (CaO)
        /// </summary>
	    public double TargetCaO { get; set; }
        /// <summary>
        /// Целевые на плавку: (T)
        /// </summary>
        public double TargetT { get; set; }
        /// <summary>
        /// Целевые на плавку: (CaO/SiO2)
        /// </summary>
        public double TargetCaOdivSiO2 { get; set ; }
        /// <summary>
        /// Смена
        /// </summary>
        public string Gang { get; set; }
        /// <summary>
        /// Бригада
        /// </summary>
        public string Team { get; set; }
        /// <summary>
        /// Время заливки чугуна
        /// </summary>
        public DateTime HotMetalTime { get; set; }
        /// <summary>
        /// Температура чугуна
        /// </summary>
        public double HotMetalTemp { get; set; }
        /// <summary>
        /// Номер ковша
        /// </summary>
        public int HotMetalLadle { get; set; }
        /// <summary>
        /// Вес чугуна и шлака в ковше
        /// </summary>
        public double HotMetalLadleWeight { get; set; }
        /// <summary>
        /// Время взвешивания
        /// </summary>
        public DateTime HotMetalWeightTime { get; set; }
        /// <summary>
        /// Номер крана
        /// </summary>
        public int HotMetalKranNumber { get; set; }
        /// <summary>
        /// Вес слитого чугуна
        /// </summary>
        public double HotMetalWeight { get; set; }
        /// <summary>
        /// Время завалки лома
        /// </summary>
        public DateTime ScrapTime { get; set; }
        /// <summary>
        /// Общий вес лома
        /// </summary>
        public double ScrapWeight { get; set; }
        /// <summary>
        /// Состав лома
        /// </summary>
        public string ScrapMix  { get; set; }
        /// <summary>
        /// Вес долей лома
        /// </summary>
        public string ScrapMixWeight { get; set; }
        /// <summary>
        /// Количество совков
        /// </summary>
        public int ScrapBucket  { get; set; }
        /// <summary>
        ///	Время. Выпуск стали
        /// </summary>
        public Duration OutDuration { get; set; }
        /// <summary>
        /// Время. Выпуск стали ОТК
        /// </summary>
        public Duration OutOtkDuration { get; set; }
        /// <summary>
        /// Номер фурмы
        /// </summary>
        public int LanceNumber { get; set; }
        /// <summary>
        /// Стойкость фурмы конвертора
        /// </summary>
        public int ConverterLife { get; set; }
        /// <summary>
        /// Количество продувок
        /// </summary>
        public int BlowingCount { get; set; }
        /// <summary>
        /// Продувка сумарная	
        /// </summary>
        public Blowing  Blowing { get; set; }
        /// <summary>
        /// Время. Слив шлака
        /// </summary>
        public Duration SlagOutDuration { get; set; }
        /// <summary>
        /// Зонд. Замер1 
        /// </summary>
        public Zond Zond1 { get; set; }
        /// <summary>
        /// Зонд. Замер2
        /// </summary>
        public Zond Zond2  { get; set; }
        /// <summary>
        /// Расчетный углерод зонд (Куркин)
        /// </summary>
        public double ZondCulcC { get; set; }
        /// <summary>
        /// Расчетна температура зонда (Чех)
        /// </summary>
        public double ZondCulcTemp { get; set; }
        /// <summary>
        /// Продувка 1
        /// </summary>
        public Blowing Blowing1 { get; set; }
        /// <summary>
        /// Продувка 2
        /// </summary>
        public Blowing Blowing2 { get; set; }
        /// <summary>
        /// Продувка 3
        /// </summary>
        public Blowing Blowing3 { get; set; }
        /// <summary>
        /// Коррекция. Делта Т
        /// </summary>
        public double CorrDeltaT { get; set; }
        /// <summary>
        /// Коррекция. Делта C
        /// </summary>
        public double  CorrDeltaC { get; set; }
        /// <summary>
        /// Коррекция. Кислород
        /// </summary>
        public double CorrO2 { get; set; }
        /// <summary>
        /// Коррекция. Доломит
        /// </summary>
        public double CorrDolomit { get; set; }
        /// <summary>
        /// Чугун. Проба1
        /// </summary>
        public HotMetalXim HotMetalXim1 { get; set; }
        /// <summary>
        /// Чугун. Проба2
        /// </summary>
        public HotMetalXim HotMetalXim2 { get; set; }
        /// <summary>
        /// Чугун. Проба3
        /// </summary>
        public HotMetalXim HotMetalXim3 { get; set; }
        /// <summary>
        /// Чугун. Проба4
        /// </summary>
        public HotMetalXim HotMetalXim4 { get; set; }
        /// <summary>
        /// Химия стали. Проба1.
        /// </summary>
        public SteelXim SteelXim1 { get; set; }
        /// <summary>
        /// Химия стали. Проба2.
        /// </summary>
        public SteelXim SteelXim2 { get; set; }
        /// <summary>
        /// Химия стали. Расчет.
        /// </summary>
        public SteelXim SteelXimCulc { get; set; }
        /*
        углерод план	
        фосфор план	
        температура выпуска план	
        хим состав метала, который был взят зондом C, Si, Mn, P, S	
        Масса стали	
        масса шлака	
        metal yield		
        */
        /// <summary>
        /// Химия шлака. Проба1
        /// </summary>
        public SlagXim SlagXim1 { get; set; }
        /// <summary>
        /// Химия шлака. Проба2
        /// </summary>
        public SlagXim SlagXim2 { get; set; }
        /// <summary>
        /// Химия шлака. Расчет (Чех)
        /// </summary>
        public SlagXim SlagXimCulc { get; set; }
        /// <summary>
        /// Материалы на торкретирование
        /// KOKS, ИЗВЕСТ, ДОЛМИТ, ДОЛОМС, МАХГ, CAC
        /// </summary>
        public Additon Gunning { get; set; }
         /// <summary>
        /// Материалы в конвертер
        /// KOKS, ИЗВЕСТ, ДОЛМИТ, ДОЛОМС, МАХГ, CU, NI LOM,	ФОМ, ALКонц, АГЛОМ, MENISP, СМГ, 
        /// FERUDA, FLGL, TERAMR, Доломит мягкого обжига, MMKN75, RANT70, FLMG-1
        /// </summary>
        public Additon Converter { get; set; }
         /// <summary>
        /// Материалы в ковш
        /// KOKS, ИЗВЕСТЬ, CU, NI LOM, SMN17, FESI65, AL2, AL, KAT, FEMN82, FEMN78, ALSECH, NI, 
        /// FEV50, FESICR, MN95, FEMO60, FEP, FECR, SICA, FENB, SIC, AL, BR, FEV80, FESI75, ALPIR, 
        /// MNBRIK, SIC, BR, FEMN88, GRMAGN, ЛИГАТ2, FENI, MNAZOT, MO99, ABK65, FETI30, FENB50	
        /// </summary>
        public Additon Bucket { get; set; }
        /*
        масса фома	
        масса извести	
        масса известниака	
        масса доломита	
        основность	
        */
        /// <summary>
        /// выбросы (нет, малые, стредние, большие)	
        /// </summary>
        public string SlagOutbust { get; set; }
        /// <summary>
        /// Кнопка зажигание плавки (нет, хорошо, плохо)
        /// </summary>
        public string Ignition { get; set; }
        public List<HeatInTime> HeatInTime { get; set; }

        #region Вспомогтельные идентификаторы
        /// <summary>
        /// Идентификатор плавки
        /// </summary>
        public int HeatId { get; set; }
        /// <summary>
        /// Идентификатор марки стали
        /// </summary>
        public int GradeId { get; set; }
        /// <summary>
        /// Идентификатор ковша
        /// </summary>
        public int LadleId { get; set; }
        /// <summary>
        /// Номер конвертера
        /// </summary>
        public int CnvNo { get; set; }

        public Double SteelMass { get; set; }

        public QualityControl Quality { get; set; }

        public HeatQuality HeatQuality { get; set; }

        public List<Scrap> Scraps;

        public ModelInfo ModelInfo;

        public Charge Charge;

       // public ReferencePoint ReferencePoint;

        public List<AdditionalChemestry> AdditionalChemestries = new List<AdditionalChemestry>();
 
        public List<ScrapChemestry> ScrapChemestries = new List<ScrapChemestry>(); 

        #endregion

    }
}
