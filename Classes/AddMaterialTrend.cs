﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
    public class AddMaterialTrend
    {
        public string MaterialName;
        public double Weight;
        public DateTime Time;
    }
}
