﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using HeatExtractor.Classes;
using HeatExtractor.DBLayers;
using HeatExtractor.Excel;
using Implements;

namespace HeatExtractor
{
         
    public partial class HeatExtractor : Form
    {
        public  const  bool flag_variant = false;// 2 празных методя для определения УВМ и Автодоводки (true - старый метод)
        private ExcelExport _excelExport;
        public HeatExtractor()
        {
            InitializeComponent();
            InstantLogger.LogMessage += LogMessage;
            ExcelExport.HeatInTimeChanged += OnHeatInTime;
        }

        private void LogMessage(string msg)
        {
           this.Invoke( new Action(()=> StateListBox.Items.Add(msg)));
        }

        private void Form1Load(object sender, EventArgs e)
        {

        }

        private void Clear()
        {
            textBox1.Visible = false;
            textBox2.Visible = false;
            DateTimePickerStart.Visible = false;
            DateTimePickerEnd.Visible = false;
            groupBox1.Visible = false;
            checkBox3.Checked = true;
            checkBox4.Checked = true;
            checkBox5.Checked = true;
        }

        private void RadioButton1CheckedChanged(object sender, EventArgs e)
        {
            Clear();
            textBox1.Visible = true;
        }

        private void RadioButton2CheckedChanged(object sender, EventArgs e)
        {
            Clear();
            textBox1.Visible = true;
            textBox2.Visible = true;
        }

        private void RadioButton3CheckedChanged(object sender, EventArgs e)
        {
            Clear();
            DateTimePickerStart.Visible = true;
            groupBox1.Visible = true;
        }

        private void RadioButton4CheckedChanged(object sender, EventArgs e)
        {
            Clear();
            DateTimePickerStart.Visible = true;
            DateTimePickerEnd.Visible = true;
            groupBox1.Visible = true;
        }

        public TimeSpan time1;
        private void Button1Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            var dbSmk = new DbLayerSmk();
            var dbBof = new DbLayerBof();
            var cnvNo = "";
            var general = new List<General>();
            if (radioButton1.Checked)
            {
                general = dbSmk.GetHeatId(Convert.ToInt32(textBox1.Text), Convert.ToInt32(textBox1.Text));    
            }
            if (radioButton2.Checked)
            {
                general = dbSmk.GetHeatId(Convert.ToInt32(textBox1.Text), Convert.ToInt32(textBox2.Text));
                
            }
            if (radioButton3.Checked || radioButton4.Checked)
            {
                if (checkBox3.Checked) { cnvNo = cnvNo + ", 1"; }
                if (checkBox4.Checked) { cnvNo = cnvNo + ", 2"; }
                if (checkBox5.Checked) { cnvNo = cnvNo + ", 3"; }
                if (checkBox3.Checked || checkBox4.Checked ||checkBox5.Checked)
                {
                    cnvNo = cnvNo.Remove(0, 2);    
                }
                if (radioButton3.Checked)
                {
                    general = dbSmk.GetHeatId(DateTimePickerStart.Value.Date, DateTimePickerStart.Value.AddDays(1).Date, cnvNo);
                }
                if (radioButton4.Checked)
                {
                    general = dbSmk.GetHeatId(DateTimePickerStart.Value, DateTimePickerEnd.Value, cnvNo);
                }
            }
            progressBar1.Maximum = general.Count*100;
            heatCountLbl.Text = general.Count.ToString();
            _excelExport = new ExcelExport();
            var csv = new List<string>();//new string[general.Count + 1];
            var qG = 0;
            var q1 = 0;
            var q2 = 0; 
            var q12 = 0;
            timeToEndLbl.Text = "Расчитываю";
                Button2Click(sender, e);
           
            try
            {
                if (textBox3.Text == "")
                    return;
        
                button3.Enabled = true;
              Thread1 = new Thread(() =>
                    {
                        try
                        {
                            var additionalsChemestry = dbBof.GetAdditionalChemestries();
                            var scrapChemesty = dbBof.GetScrapChemestries();
                          
                            _excelExport.ExcelOpen();
                            csv.Add(CsvExport.GetHeaderGeneral());
                            int progress = 0;
                            for (var index = 0; index < general.Count; index++)
                            {
                                try
                                {
                                    var startTime = DateTime.Now;
                                    progress = (index ) * 100;
                                    SetProgress(progress);
                                    var gen = general[index];
                                   InstantLogger.log("Начало павки");
                                    Invoke(new Action(() => { heatNoLbl.Text = gen.HeatNumber.ToString(); }));
                                  var g1 = dbBof.GetFinalHotMetal(gen);
                                    //    if ((int)g1.HotMetalLadleWeight % 5000 == 0)
                                        //continue;
                                    var heat = dbSmk.GetSmkData(gen);
                                    progress += 10;
                                    SetProgress(progress);
                                    heat = dbBof.GetBofDataGeneral(heat);
                                    progress += 10;
                                    SetProgress(progress);
                                 // if (! ExcelExport.IsQ3(heat))
                                 //       continue;
                                    if (checkBox2.Checked)
                                        heat = dbBof.GetBofDataInTime(heat);
                                    progress += 30;
                                    SetProgress(progress);
                                    qG = _excelExport.ExcelSheetGenerel(heat, qG);
                                    progress += 5;
                                    SetProgress(progress);
                                    q1 = _excelExport.ExcelSheetQ1(heat, q1);
                                    progress += 2;
                                    SetProgress(progress);
                                    q2 = _excelExport.ExcelSheetQ2(heat, q2);
                                    progress += 3;
                                    SetProgress(progress);
                                    q12 = _excelExport.ExcelSheetQ3(heat, q12);
                                    progress += 5;
                                    SetProgress(progress);
                                    _excelExport.ExcelSheetContents(qG, q1, q2, q12);
                                    csv.Add(CsvExport.GetStringGeneral(heat));
                                    if (checkBox2.Checked)
                                    {
                                        _excelExport.ExcelSheetsHeatInTime(heat, qG == general.Count);
                                        CsvExport.SaverHeatInTime(heat, textBox3.Text);
                                    }
                                    progress +=25;
                                    SetProgress(progress);
                                    _excelExport.ExcelSheetQualityControl(heat, qG);
                                    progress += 3;
                                    SetProgress(progress);
                                    _excelExport.ExcelSheetHeatQualityControl(heat, qG);
                                    progress += 3;
                                    SetProgress(progress);
                                    _excelExport.ExcelSheetHeatModel(heat, qG);
                                    _excelExport.ExcelSheetModelChemestry(heat, qG);
                                    _excelExport.ExcelSheetCharge(heat, qG + 1);
                                    _excelExport.ExcelSheetChargeMaterial(heat, qG + 1);
                                    progress += 4;
                                    SetProgress(progress);
                                    Invoke(new Action(() => { heatNoLbl.Text = gen.HeatNumber.ToString(); }));
                                    progress += 4;
                                    progress = (index + 1)*100;
                                    SetProgress(progress);
                                    var endTime = DateTime.Now;
                                    var span = (endTime - startTime);
                                    span = new TimeSpan(span.Ticks* (general.Count-index));
                                    Invoke(new Action(() => { timeToEndLbl.Text = span.ToString();
                                                                processedLbl.Text = index.ToString();
                                    }));
                                    
                                    //  SetProgress(qG);

                                }
                                catch (Exception exception)
                                {
                                    InstantLogger.err(exception.ToString());

                                }

                            }
                            _excelExport.ExportChemestries(additionalsChemestry, scrapChemesty);
                            CsvExport.SaveCsvFile(textBox3.Text, "Общая", csv.ToArray());
                            _excelExport.ExcelSave(textBox3.Text);
                            InstantLogger.msg("Export fine");
                            _excelExport.ExcelClose();
                            Invoke(new Action(() =>
                                {
                                    button1.Enabled = true;
                                    button3.Enabled = false;
                                    //  progressBar1.Value = 100;
                                }));
                        }
                        catch(Exception exx)
                        {

                            _excelExport.ExcelClose();
                            InstantLogger.err(exx.ToString());
                            Invoke(new Action(() =>
                            {
                                button1.Enabled = true;
                                button3.Enabled = false;
                               // progressBar1.Value = 100;
                            }));
                        }

                    });
                Thread1.Start();

            }
            catch (Exception)
            {
              // 
                
                throw;
            }
            finally
            {
               // ex.ExcelClose();
            }
            
        }
        int currentValueProgess = 0;
        private void OnHeatInTime(int count, int current)
        {
            Invoke(new Action(() =>
                {
                    var x = (((double)current / (double)count));
                    if (current == 0)
                        currentValueProgess = progressBar1.Value;
                    x = x*25;
                    x = Math.Round(x);
                   var curval = x;
                    progressBar1.Value = currentValueProgess + (int)x;

                }));
        }

        private void SetProgress(int qG)
        {
            Invoke(new Action(() => { progressBar1.Value = qG; }));
        }

        protected Thread Thread1 { get; set; }

        private void Button2Click(object sender, EventArgs e)
        {
            textBox3.Text = "";
            var saveFileDialog1 = new SaveFileDialog
                                      {
                                          Filter = "Excel files (*.xls)|*.xls|All files (*.*)|*.*",
                                          FilterIndex = 1,
                                          RestoreDirectory = true
                                      };


            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox3.Text = saveFileDialog1.FileName;
               
            }
             
        }

        private void DateTimePickerStart_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
        //   _excelExport.ExcelClose();
            if (Thread1!=null)
            {
                Thread1.Interrupt();
                Thread1.Abort();
                Thread1 = null;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

    }
}
