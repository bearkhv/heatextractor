﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
    public class SublanceControlInTimeExport
    {
        public DateTime Time;
        public double Delay = -1;
        public int BathDelay = -1;
        public int LanceBath = -1;
        public int O2Bath = -1;
        public int LanceMes = -1;
        public int O2Mes = -1;
    }
}
