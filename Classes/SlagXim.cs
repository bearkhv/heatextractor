﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
    /// <summary>
    /// Химия шлака
    /// </summary>
    public class SlagXim
    {
        /// <summary>
        /// Дата ввода
        /// </summary>
        public DateTime Time { get; set; }
        /// <summary>
        /// CAO
        /// </summary>
        public double CaO { get; set; }
        /// <summary>
        /// SIO2
        /// </summary>
        public double SiO2 { get; set; }
        /// <summary>
        /// FEO
        /// </summary>
        public double FeO { get; set; }
        /// <summary>
        /// MGO
        /// </summary>
        public double MgO { get; set; }
        /// <summary>
        /// MNO
        /// </summary>
        public double MnO { get; set; }
        /// <summary>
        /// AL2O3
        /// </summary>
        public double Al2O3 { get; set; }
         /// <summary>
        /// S
        /// </summary>
        public double S { get; set; }
         /// <summary>
        /// P2O5
        /// </summary>
        public double P2O5 { get; set; }
         /// <summary>
        /// CaO/SiO2
        /// </summary>
        public double CaOdivSiO2 { get; set; }
       									
        
    }
}
