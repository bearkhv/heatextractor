﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Converter;

namespace HeatExtractor.Classes
{
    public class HeatInTime
    {
        public DateTime Time { get; set; }
        public LanceEvent Lance { get; set; }
        public OffGasTrend OffGas { get; set; }
        public OffGasAnalysis OffGasAnalysis { get; set; }
        public StatisticTrend Statistic { get; set; }
        public StatisticTrend UniversalCPl { get; set; }
        public TempLiner_Result TempLiner { get; set; }
        public CorrectionCT_Result CorrectionCT { get; set; }
        public BalanceTrend Balance { get; set; }
        public ModesTrend Modes { get; set; }
        public GasFlow GasFlow { get; set; }
        public AddMaterialTrend AddMaterial { get; set; }
        public StatisticTrend Curkin { get; set; }
        public Template Template{ get; set; }
        public ZondInTime Zond { get; set; }
        public SublanceControlInTimeExport SublanceControlInTime;
        public ReferencePoint RefPoint{ get; set; }

    }
}
