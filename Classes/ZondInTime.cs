﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
    public class ZondInTime
    {
        public DateTime Time;
        public int ZondAutoStartCommand;
        public int ZondStartCommand;
        public int Level;
        public int T;
        private readonly Color _startCommandColor = Color.CornflowerBlue;
        private readonly Color _startColor = Color.OrangeRed;
        private readonly Color _levelColor = Color.GreenYellow;
        private readonly Color _tcolor = Color.SandyBrown;
        private readonly Color _def = Color.Transparent;
        public Color GetColor()
        {
            if (ZondAutoStartCommand>0)
            {
                return _startCommandColor;
            }
            if (ZondStartCommand>0)
            {
                return _startColor;
            }
            if (T > 0) 
                return _tcolor;
            if (Level>0)
            {
                return _levelColor;
            }

            return _def;
        }
    }
}
