﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;

namespace HeatExtractor.Classes
{
    public class ModelInfo
    {
        public string CurrentModelType; 
        //прогноз выброса от модели	№ плавки	
        public string Danger;
        //фиксация времени замера зондом команда "Старт зонда"	
        public DateTime ZondDate;
        // фиксация команды от УВМ на старт зонда
        public DateTime ZondStartCommandDate;
       // суммарный кислород на плавку  от стат модели
        public float RecommendMeteringB;
       //суммарный кислород на плавку  от стат модели
        public float RecommendBalanceBlow;
        //кислород на доводку от стат модели
        public double TotalBlowingO2;
        // от баланс модели	кислород на доводку от баланс модели
        public float RecommendedBalanceBlow;
        //фиксация времени события - рекомендации углерода Куркина В. М.
        public DateTime CurkinCFix;
        //значение суммарного кислорода
        public float O2Total;

        public float TargetC;

        public float TargetCu;
        //текущий углерод Куркина
        public float CurrentCurkin;
        //текущий углерод Чеха
        public double CurrentCCheh;

        public double RecommededModelO2;

        public double StartCommandZondO2;
        
        //нейросетевой углерод
        public double NeuralC;

        //новая многофакторная модель
        public double CPlusFix;
        
        // старая многофакторная модель
        public double SMFCarbon;

        //переключатель углеродов
        public double CarbonSwitcher ;
        
        //автозонд и автодоводка
        public string AutoZond;
        public string AutoZondMode;
        public string AutoDovodka;
        public double UniversalCPlusDateFix { get; set; }
        public string AutodovodkaModel;
        /// <summary>
        /// рекомендация ДОЛОМС на охлаждение от стат модели
        /// </summary>
        public double DolomsStatModel{ get; set; }
        /// <summary>
        /// текущий T
        /// </summary>
        public int CurrentT { get; set; }
        /// <summary>
        /// целевой T
        /// </summary>
        public int TargetT { get; set; }
        /// <summary>
        /// T стат модели на момент замера
        /// </summary>
        public double T_TempLinerStartZond { get; set; }
        /// <summary>
        /// T алгоритма доводки при окончании додувки
        /// </summary>
        public double T_TempLinerZondT { get; set; }

        public DateTime TimeZondStartCommand { get; set; }

        public DateTime TimeZondT { get; set; }
        
        // Химии от моделей чеха
        public List<ModelChemisry> Chemisries = new List<ModelChemisry>();

        public ModelChemisry GetChemestry(string type)
        {
          return  Chemisries.FirstOrDefault(x => x.Type == type);
        }

        public void SetChemestry(OracleDataReader param )
        {
            for (int i = 2; i < 9; i++)
            {
                 if (param[i]== null)
                     continue;
               if (Chemisries.Count(x=>x.Type == param[0].ToString())==0)
               {
                   Chemisries.Add(new ModelChemisry{Type =param[0].ToString()});
               }
                var chem = Chemisries.First(x => x.Type == param[0].ToString());
                
                var s = param[i].ToString().Split(':');
                if (s.Length == 2)
                {
                    if (!chem.Values.ContainsKey(s[0]))
                    chem.Values.Add(s[0],s[1]);
                }

            }
        }
        
    }
}
