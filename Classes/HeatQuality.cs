﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;

namespace HeatExtractor.Classes
{
    public class HeatQuality
    {

        public List<string> LanceCorrection = new List<string>();
        public List<string> IntenceCorrection = new List<string>();
        public List<string> O2Correction = new List<string>();
        public List<int> Scraps = new List<int>();
        public bool IsQualityScrap
        {
            get
            {
               return Scraps.Count(x => x == 8 || x == 9 || x == 18 || x == 24 || x == 25 || x ==
                                                                                             37 || x == 38 || x == 66 ||
                                        x == 89 || x == 90 || x == 91)>0;
            }
        }

        public double IronSi;

        public bool IronSi07 { get { return IronSi > 0.7; } }
        public bool IronSi025 { get { return IronSi > 0.025; } }
        public double SlagCao;

        public double HeatIzvestWeight;

        public double HeatIzvWeightCalc{get
        {
            if (HeatIzvestWeight == 0)
                return 0;
            return SlagCao/HeatIzvestWeight;
        }}

        public float MetalLevelByZond;
        public float MetalLevelByHandInput;
        public bool MetalLevelInRange
        {get {return  (((MetalLevelByZond<1180||MetalLevelByZond<1250)&&(MetalLevelByZond!=0))||(MetalLevelByHandInput<1180||MetalLevelByHandInput<1250)&&(MetalLevelByHandInput!=0));
                
        }}

      
       
        public double AlConWeight;
        public double AlConInRange1;
        public double AlConInRange2;

       

        public string SlagButton;

        public int MaxConverterAngle;

        public bool ConverterAngleInRange;

        public double GoodMlnz;
        public double ColdMlnz;

        public double TransADelay;
        public double TransBDelay;
        public double TransDelay;
        public double Diag;

        public List<double> SublanceControl = new List<double>();

    }
}
