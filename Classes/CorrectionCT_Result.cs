﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
     public class CorrectionCT_Result
    {
         /// <summary>
         /// Углерод по алгоритму доводки
         /// </summary>
         public double C { get; set; }

         public DateTime Time { get; set; }
    }
}
