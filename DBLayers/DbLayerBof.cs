﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using Converter;
using HeatExtractor.Classes;
using Implements;
using Oracle.DataAccess.Client;

namespace HeatExtractor.DBLayers
{
    public class  DbLayerBof: DbLayer
    {
       
        
        public  DbLayerBof()
        {
           // Instance = new DbLayerBof();
        }
        
        //public static DbLayerBof Instance { get; private set; }

        private double GetMassStal(int heatNum)
        {
            double result = -1;
           
                const string sql = "SELECT  NETTO FROM WEIGHT_VOS WHERE HEAT_NO = :HEAT_NO ORDER BY INSERTTIME DESC";
                var param = new List<OracleParameter> { SetParams("HEAT_NO", heatNum) };
                var read = Execute(sql, param);
            try
            {
                if (read.Read())
                {
                    result = CheckNubmerForNullAsDouble(read[0].ToString());
                }
                read.Close();

            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
            return result;
        }

        private General GetTargetValues(General general)
        {
            
                const string sql = "SELECT  C, P,CU,MGO,FEO,CAO,T,CAOSIO2, STEELGROUP FROM TARGET_EVENTS WHERE HEAT_ID = :HEAT_ID  ORDER BY INSERTTIME DESC";
                var param = new List<OracleParameter> { SetParams("HEAT_ID", general.HeatId) };
                var read = Execute(sql, param);
           try
           {
                if (read.Read())
                {
                    general.TargetC = CheckNubmerForNullAsDouble(read[0].ToString());
                    general.TargetP = CheckNubmerForNullAsDouble(read[1].ToString());
                    general.TargetCu = CheckNubmerForNullAsDouble(read[2].ToString());
                    general.TargetMgO = CheckNubmerForNullAsDouble(read[3].ToString());
                    general.TargetFeO = CheckNubmerForNullAsDouble(read[4].ToString());
                    general.TargetCaO = CheckNubmerForNullAsDouble(read[5].ToString());
                    general.TargetT = CheckNubmerForNullAsDouble(read[6].ToString());
                    general.TargetCaOdivSiO2 = CheckNubmerForNullAsDouble(read[7].ToString());
                    general.GradeGroup = read[8].ToString();
                }
                read.Close();
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
            return general;
        }

        public QualityControl GetQuality(General general)
        {
            
            var result = new QualityControl();
           
                string sql = "SELECT MAX(FLOW), MIN(FLOW)" +
                             "FROM TREND_OFFGAS WHERE CNV_NO = :CNV_NO AND INSERTTIME BETWEEN :DSTART AND :DEND ";
                var param = MandatoryParams(general.CnvNo, general.HeatDuration.Start, general.HeatDuration.End);
                var reader = Execute(sql, param);
         try
         {
                if (reader.Read())
                {
                    result.MaxOffGasFlow = CheckNubmerForNullAsDouble(reader[0].ToString());
                    result.MinOffGasFlow = CheckNubmerForNullAsDouble(reader[1].ToString());
                }
                reader.Close();

                sql =
                    "SELECT MAX(LEFTWATERTEMPINP), MIN(LEFTWATERTEMPINP), MAX(LEFTWATERTEMPOUTP), MIN(LEFTWATERTEMPOUTP), " +
                    "MAX(RIGHTWATERTEMPINP), MIN(RIGHTWATERTEMPINP), MAX(RIGHTWATERTEMPOUTP), MIN(RIGHTWATERTEMPOUTP) " +
                    "FROM TREND_LANCE WHERE CNV_NO = :CNV_NO AND INSERTTIME BETWEEN :DSTART AND :DEND ";
                reader = Execute(sql, param);
                if (reader.Read())
                {
                    result.MaxLanceTempIn = Math.Max(CheckNubmerForNullAsDouble(reader[0].ToString()),
                                                     CheckNubmerForNullAsDouble(reader[4].ToString()));
                    result.MinLanceTempIn = Math.Min(CheckNubmerForNullAsDouble(reader[1].ToString()),
                                                     CheckNubmerForNullAsDouble(reader[5].ToString()));
                    result.MaxLanceTempOut = Math.Max(CheckNubmerForNullAsDouble(reader[2].ToString()),
                                                      CheckNubmerForNullAsDouble(reader[6].ToString()));
                    result.MinLanceTempOut = Math.Min(CheckNubmerForNullAsDouble(reader[3].ToString()),
                                                      CheckNubmerForNullAsDouble(reader[7].ToString()));
                }
                reader.Close();
                param.Clear();
                sql = "select MAX(PAR6)" +
                      "FROM BOF_TELEGRAMS WHERE HEAT_NO = :HEAT_NO AND OPER_ID=1020 ";
                param.Add(SetParams("HEAT_NO", general.HeatNumber));
                reader = Execute(sql, param);
                if (reader.Read())
                {
                    result.IsAlarmStrop = reader[0].ToString();
                }
                reader.Close();

                var time1 = DateTime.Now;
                var time2 = time1;
                var count = 0;
                param.Clear();
                sql =
                    "SELECT MAX(INSERTTIME) FROM TREND_LANCE WHERE INSERTTIME> :TIME1 AND INSERTTIME< :TIME2 AND CNV_NO  = :CNV_NO AND O2TOTALVOL<16000";

                param.Add(SetParams("TIME1", general.HeatDuration.Start));
                param.Add(SetParams("TIME2", general.HeatDuration.End));
                param.Add(SetParams("CNV_NO", general.CnvNo));
                reader = Execute(sql, param);
                DateTime time0 = DateTime.MinValue;
                if (reader.Read())
                {
                    // count = CheckNubmerForNullAsInt(reader[0].ToString());
                    time0 = CheckDateForNull(reader[0].ToString());
                    // time2 = CheckDateForNull(reader[2].ToString());
                }
                reader.Close();
                param.Clear();
                sql =
                    " SELECT COUNT(T1.INSERTTIME), MAX(T1.INSERTTIME), MIN(T1.INSERTTIME)  FROM TREND_OFFGASANALYSE t1 " +
                    "WHERE  t1.INSERTTIME> :TIME1 AND t1.INSERTTIME<:TIME2 AND t1.CNV_NO  = :CNV_NO";
                param.Add(SetParams("TIME1", general.HeatDuration.Start));
                param.Add(SetParams("TIME2", time0));
                param.Add(SetParams("CNV_NO", general.CnvNo));
                reader = Execute(sql, param);
                if (reader.Read())
                {
                    count = CheckNubmerForNullAsInt(reader[0].ToString());
                    time1 = CheckDateForNull(reader[1].ToString());
                    time2 = CheckDateForNull(reader[2].ToString());
                }
                var total = time1 - time2;

                result.OffGasQuality = total.TotalSeconds - (count - 1) < 5;


                param.Clear();

                param.Add(SetParams("TIME1", time0));
                param.Add(SetParams("TIME2", general.HeatDuration.End));
                param.Add(SetParams("CNV_NO", general.CnvNo));
                reader = Execute(sql, param);
                if (reader.Read())
                {
                    count = CheckNubmerForNullAsInt(reader[0].ToString());
                    time1 = CheckDateForNull(reader[1].ToString());
                    time2 = CheckDateForNull(reader[2].ToString());
                }
                reader.Close();
                total = time1 - time2;

                result.OffGasQuality = result.OffGasQuality & total.TotalSeconds - (count - 1) < 2;
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return result;
        }

        public General GetBofDataGeneral(General general)
        {
            InstantLogger.log("", string.Format("{0}. GET ADDITIONAL DATA", general.HeatNumber));
            if (HeatExtractor.flag_variant)
            {
                InstantLogger.msg("{0}. get lance mode", general.HeatNumber);
                general.LanceMode = GetLanceMode(general.CnvNo, general.HeatDuration.Start, general.HeatDuration.End);
            }
            else
            {

                InstantLogger.msg("{0}. get lance mode", general.HeatNumber);
                general.LanceMode = GetLanceModeT(general.CnvNo, general.HeatDuration.Start, general.HeatDuration.End, general.HeatId);
                InstantLogger.msg("{0}. get lance mode", general.HeatNumber);
                general.AvtoDovodka = GetLAutoDovodka(general.CnvNo, general.HeatDuration.Start, general.HeatDuration.End, general.HeatNumber);
            }

            InstantLogger.msg("{0}. get tract mode", general.HeatNumber);
            general.TractMode = GetTractMode(general.CnvNo, general.HeatDuration.Start, general.HeatDuration.End);
            
            InstantLogger.msg("{0}. get final hot metal", general.HeatNumber);
            
            general = GetFinalHotMetal(general);
            InstantLogger.msg("{0}. get target values", general.HeatNumber);
            
            general = GetTargetValues(general);
            InstantLogger.msg("{0}. get mass steel", general.HeatNumber);
            general.SteelMass = GetMassStal(general.HeatNumber);
            InstantLogger.msg("{0}. get quality", general.HeatNumber);
            general.Quality = GetQuality(general);
            InstantLogger.msg("{0}. get ignition", general.HeatNumber);
            var ign = GetIgnition(general.CnvNo, general.HeatDuration.Start, general.HeatDuration.End);
            switch (ign)
            {
                case 3:
                    general.Ignition = "Плохо зажигается";
                    break;
                case 2:
                    general.Ignition = "Хорошо зажигается";
                    break;
                case 1:
                    general.Ignition = "Очень хорошо зажигается";
                    break;
                default:
                    general.Ignition = "Нет данных";
                    break;
            }
            InstantLogger.msg("{0}. get slag out", general.HeatNumber);
            var slout = GetSlagOut(general.CnvNo, general.HeatDuration.Start, general.HeatDuration.End);
            switch (slout)
            {
                case 0:
                    general.SlagOutbust = "Нет";
                    break;
                case 1:
                    general.SlagOutbust = "Мало";
                    break;
                case 2:
                    general.SlagOutbust = "Много";
                    break;
                default:
                    general.SlagOutbust = "Очень много";
                    break;
            }
            InstantLogger.msg("{0}. get hot metal xim", general.HeatNumber);
            general.HotMetalXim1 = GetHotMetalXim(general.HeatNumber, 1);
            general.HotMetalXim2 = GetHotMetalXim(general.HeatNumber, 2);
            general.HotMetalXim3 = GetHotMetalXim(general.HeatNumber, 3);
            general.HotMetalXim4 = GetHotMetalXim(general.HeatNumber, 4);
            var hotMetalXim = general.HotMetalXim1;//GetHotMetalXim(general.HeatNumber, 1);
            var hotMetalXimCulc = GetHotMetalXim(general.HeatNumber);
            if (hotMetalXimCulc != null)
            {
                
                hotMetalXim.C = hotMetalXimCulc.C;
                hotMetalXim.Mn = hotMetalXimCulc.Mn;
                hotMetalXim.P = hotMetalXimCulc.P;
                hotMetalXim.S = hotMetalXimCulc.S;
                hotMetalXim.Si = hotMetalXimCulc.Si;
           }
            

            general.HotMetalXim1 = hotMetalXim;
            InstantLogger.msg("{0}. get steel xim", general.HeatNumber);
            general.SteelXim1 = GetSteelXim(general.HeatNumber, 1);
            general.SteelXim2 = GetSteelXim(general.HeatNumber, 2);
            general.SteelXimCulc = GetSteelXim(general.HeatNumber);
            InstantLogger.msg("{0}. get slag xim", general.HeatNumber);
            general.SlagXim1 = GetSlagXim(general.HeatNumber, 1);
            general.SlagXim2 = GetSlagXim(general.HeatNumber, 2);
            general.SlagXimCulc = GetSlagXim(general.HeatNumber);
            InstantLogger.msg("{0}. get heat quality", general.HeatNumber);
            general.HeatQuality = GetHeatQuality(general);
            InstantLogger.msg("{0}. get models data", general.HeatNumber);
            general.ModelInfo = GetModelInfo(general);///////////////////////////////////////////////
            InstantLogger.msg("{0}. get charge data", general.HeatNumber);
            general.Charge = GetCharge(general);
           
            InstantLogger.log("", string.Format("{0}. GET ADDITIONAL DATA. DONE", general.HeatNumber));
            
            return general;
        }

        public List<AdditionalChemestry> GetAdditionalChemestries()
        {
            var list = new List<AdditionalChemestry>();
           
                const string sql =
                    "SELECT       ADDITION.NAME AS ANAME, ADDITION.DESCRIPTION AS BNAME, ELEMENT.NAME AS N, ELEMENT.VALUE AS V"
                    + " FROM            ADDITION, ELEMENT"
                    + " WHERE        ADDITION.ID = ELEMENT.SID"
                    + " ORDER BY ANAME";
                var param = new List<OracleParameter>();
                var read = Execute(sql, param);
           try
           {
                while (read.Read())
                {
                    list.Add(new AdditionalChemestry
                        {
                            Code = read[0].ToString(),
                            Name = read[1].ToString(),
                            Element = read[2].ToString(),
                            Value = CheckNubmerForNullAsDouble(read[3].ToString())
                        });
                }
                read.Close();
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
            return list;
        }
        public  List<ScrapChemestry> GetScrapChemestries()
        {
            var list = new List<ScrapChemestry>();
            
                const string sql =
                    "SELECT        SCRAP.CODE AS SCODE, SCRAP.DESCRIPTION, ELEMENT.NAME AS N, ELEMENT.VALUE AS V"
                    + " FROM            SCRAP, ELEMENT"
                    + " WHERE        SCRAP.ID = ELEMENT.SID"
                    + " ORDER BY SCODE";
                var param = new List<OracleParameter>();
                var read = Execute(sql, param);
          try
          {
                while (read.Read())
                {
                    list.Add(new ScrapChemestry
                        {
                            Code = CheckNubmerForNullAsInt(read[0].ToString()),
                            Name = read[1].ToString(),
                            Element = read[2].ToString(),
                            Value = CheckNubmerForNullAsDouble(read[3].ToString())
                        });
                }
                read.Close();
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
            return list;
        }


        private Duration GetTor(General general)
        {
            
                const string sql = "SELECT STARTTIME, ENDTIME FROM  SLAGBLOWING  WHERE  HEAT_ID = :HEAT_ID ";
                var param = new List<OracleParameter> {SetParams("HEAT_ID", general.HeatId)};
                var read = Execute(sql, param);
           try
           {
                if (read.Read())
                {
                    return new Duration
                        {
                            Start = CheckDateForNull(read[0].ToString()),
                            End = CheckDateForNull(read[1].ToString())
                        };
                }
                read.Close();
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
            return null;
        }
        private double GetAlconcAdditionCv(General general, int min,int max)
        {
            var result = 0.0d;
            
                string sql;
                var i = 0;
                var param = new List<OracleParameter>();
                var tor = GetTor(general);
                if (tor != null)
                {
                    sql = "SELECT SUM(PORTION_WGT) FROM HEAT_ADDITIONS_ACT " +
                          "WHERE DESTINATION_AGGNO = 'CV' AND INSERTTIME > :DEND AND HEAT_ID = :HEAT_ID " +
                          " AND O2VOL_TOTAL>:Min1 AND O2VOL_TOTAL<:Max1 AND MAT_ID = 264";
                    param.Add(SetParams("DEND", tor.End));
                    param.Add(SetParams("HEAT_ID", general.HeatId));
                }
                else
                {
                    sql = "SELECT SUM(PORTION_WGT) FROM HEAT_ADDITIONS_ACT " +
                          "WHERE DESTINATION_AGGNO = 'CV' AND HEAT_ID = :HEAT_ID " +
                          "AND O2VOL_TOTAL>:Min1 AND O2VOL_TOTAL<:Max1 AND MAT_ID = 264";
                    param.Add(SetParams("HEAT_ID", general.HeatId));
                }
                param.Add(SetParams("Min1", min));
                param.Add(SetParams("Max1", max));
                var read = Execute(sql, param);
            try
            {
                while (read.Read())
                {
                    result = CheckNubmerForNullAsDouble(read[0].ToString());
                }
                read.Close();
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
            return result;
        }
        private HeatQuality GetMetalRange(HeatQuality quality, General general)
        {
           
                const string sql =
                    "SELECT MAX(PAR1), MAX(PAR2) FROM BOF_TELEGRAMS WHERE  HEAT_NO = :HEAT_NO AND OPER_ID = 1190";
                var param = new List<OracleParameter> {SetParams("HEAT_NO", general.HeatNumber)};
                var read = Execute(sql, param);
           try
           {
                if (read.Read())
                {
                    quality.MetalLevelByZond = CheckNubmerForNullAsFloat(read[0].ToString());
                    quality.MetalLevelByHandInput = CheckNubmerForNullAsFloat(read[1].ToString());
                }
                read.Close();
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
            return quality;
        }

        

        private ModelInfo GetModelInfo(General general)
        {
            var modelInfo = new ModelInfo();
           
                string sql =
                    "SELECT EVENTTIME, PAR2 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 1";
                var param = new List<OracleParameter>
                    {
                        SetParams("HEAT_NO", general.HeatNumber)

                    };
                var read = Execute(sql, param);
           try
           {
                if (read.Read())
                {
                    modelInfo.Danger = read[1].ToString();
                }
                read.Close();

                sql =
                    "SELECT PAR1 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 14 ORDER BY EVENTTIME";
                read = Execute(sql, param);
                if (read.Read())
                {
                    modelInfo.CurrentModelType = read[0].ToString();
                }
                read.Close();

                sql =
                    "SELECT EVENTTIME, PAR2 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 9 AND PAR1=1 ORDER BY EVENTTIME";
                read = Execute(sql, param);
                if (read.Read())
                {
                    modelInfo.ZondStartCommandDate = CheckDateForNull(read[0].ToString());
                    modelInfo.StartCommandZondO2 = CheckNubmerForNullAsFloat(read[1].ToString());
                }
                read.Close();

                sql = "SELECT  PAR1 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 2";
                read = Execute(sql, param);
                if (read.Read())
                {
                    modelInfo.RecommendMeteringB = CheckNubmerForNullAsFloat(read[0].ToString());
                }
                read.Close();

                sql = "SELECT  PAR1, PAR4, PAR5, PAR6 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 3";
                read = Execute(sql, param);
                if (read.Read())
                {
                    modelInfo.RecommendBalanceBlow = CheckNubmerForNullAsFloat(read[0].ToString());
                    modelInfo.CurrentT = CheckNubmerForNullAsInt(read[1].ToString());
                    modelInfo.TargetT = CheckNubmerForNullAsInt(read[2].ToString());
                    if (modelInfo.RecommendBalanceBlow == -3)
                         modelInfo.DolomsStatModel = CheckNubmerForNullAsDouble(read[3].ToString());
                }
                read.Close();

                sql = "SELECT  PAR1 FROM  MODEL_TELEGRAMS WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 4";
                read = Execute(sql, param);
                if (read.Read())
                {
                    modelInfo.TotalBlowingO2 = CheckNubmerForNullAsFloat(read[0].ToString());
                }
                read.Close();



                sql = "SELECT  PAR2 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 5";
                read = Execute(sql, param);
                if (read.Read())
                {
                    modelInfo.RecommendedBalanceBlow = CheckNubmerForNullAsFloat(read[0].ToString());
                }
                read.Close();

                sql = "SELECT  EVENTTIME, PAR1 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 6";
                read = Execute(sql, param);
                if (read.Read())
                {
                    // modelInfo.CurkinCFix = CheckDateForNull(read[0].ToString());
                    modelInfo.CPlusFix = CheckNubmerForNullAsDouble(read[1].ToString());
                }
                read.Close();



                sql = "SELECT  EVENTTIME, PAR1 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 17";
                read = Execute(sql, param);
                if (read.Read())
                {
                    modelInfo.NeuralC = CheckNubmerForNullAsDouble(read[1].ToString());
                }
                read.Close();

                sql = "SELECT  EVENTTIME, PAR1 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 18";
                read = Execute(sql, param);
                if (read.Read())
                {
                    modelInfo.CarbonSwitcher = CheckNubmerForNullAsDouble(read[1].ToString());
                }
                read.Close();

                sql = "SELECT  EVENTTIME, PAR1 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 19";
                read = Execute(sql, param);
                if (read.Read())
                {
                    modelInfo.SMFCarbon = CheckNubmerForNullAsDouble(read[1].ToString());
                }
                read.Close();

                DateTime evtTime = DateTime.MinValue;
                sql =
                    "SELECT  PAR1, PAR2, PAR3,PAR5, EVENTTIME FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID =8 ";
                read = Execute(sql, param);
                if (read.Read())
                {
                    modelInfo.TargetC = CheckNubmerForNullAsFloat(read[0].ToString());
                    modelInfo.TargetCu = CheckNubmerForNullAsFloat(read[1].ToString());
                    modelInfo.CurrentCurkin = CheckNubmerForNullAsFloat(read[2].ToString());
                    modelInfo.O2Total = CheckNubmerForNullAsFloat(read[3].ToString());
                    modelInfo.CurkinCFix = CheckDateForNull(read[4].ToString());
                }
                read.Close();

                sql = "SELECT EVENTTIME FROM  HEAT_EVENTS " +
                      " WHERE  HEAT_ID = :HEAT_ID AND TYPE = 'SUBS' ORDER BY EVENTTIME";
                param.Clear();
                param.Add(SetParams("HEAT_ID", general.HeatId));
                read = Execute(sql, param);
                if (read.Read())
                {
                    modelInfo.ZondDate = CheckDateForNull(read[0].ToString());
                }

                read.Close();
                param.Clear();





                param.Clear();
                sql =
                    "SELECT  PAR1, PAR2, PAR3, PAR4, PAR5, PAR6, PAR7, PAR8, PAR9 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 20 ORDER BY PAR1, PAR2 ";
                param = new List<OracleParameter>
                    {
                        SetParams("HEAT_NO", general.HeatNumber)

                    };
                read = Execute(sql, param);
                while (read.Read())
                {
                    modelInfo.SetChemestry(read);
                }


                read.Close();

                sql =
                    "SELECT  PAR1, PAR2 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 21 ORDER BY PAR1, PAR2 ";

                read = Execute(sql, param);
                if (read.Read())
                {
                    modelInfo.AutoZond = read[0].ToString();
                    modelInfo.AutoZondMode = read[1].ToString();

                }


                read.Close();
                sql =
                    "SELECT  PAR1, PAR2 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 22 ORDER BY PAR1, PAR2 ";

                read = Execute(sql, param);
                if (read.Read())
                {
                    modelInfo.AutoDovodka = read[0].ToString();
                    modelInfo.AutodovodkaModel = read[1].ToString();

                }


                read.Close();

                sql =
                    "SELECT  PAR1 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 28 ORDER BY PAR1 ";

                read = Execute(sql, param);
                if (read.Read())
                {
                    modelInfo.UniversalCPlusDateFix = CheckNubmerForNullAsDouble(read[0].ToString());
                }
                read.Close();

                sql = "SELECT EVENTTIME FROM BOF_TELEGRAMS WHERE OPER_ID=1160 AND HEAT_NO=:HEAT_NO ORDER BY EVENTTIME";
                read = Execute(sql, param);
               int count = 0;
                while (read.Read())
                {
                    count++;
                    if(count == 2)
                         modelInfo.TimeZondT= CheckDateForNull(read[0].ToString());
                }
                read.Close();

                sql = "SELECT EVENTTIME FROM  HEAT_EVENTS " +
                      " WHERE  HEAT_ID = :HEAT_ID AND TYPE = 'SUBS' ORDER BY EVENTTIME";
                param.Clear();
                param.Add(SetParams("HEAT_ID", general.HeatId));
                read = Execute(sql, param);
               modelInfo.TimeZondStartCommand = DateTime.MinValue;
                if (read.Read())
                {
                    modelInfo.TimeZondStartCommand = CheckDateForNull(read[0].ToString());
                }
                read.Close();


                sql = "SELECT PAR2, EVENTTIME FROM MODEL_TRENDS WHERE OPERATION_ID=35 AND HEAT_NO=:HEAT_NO AND EVENTTIME = :EVENTTIME ";
                param.Clear();
               if (modelInfo.TimeZondStartCommand > DateTime.MinValue)
               {
                   param.Add(SetParams("HEAT_NO", general.HeatNumber));
                   param.Add(SetParams("EVENTTIME", modelInfo.TimeZondStartCommand.AddSeconds(-3)));
                   read = Execute(sql, param);
                   while (read.Read())
                   {
                       modelInfo.T_TempLinerStartZond = CheckNubmerForNullAsDouble(read[0].ToString());
                   }
               }
               read.Close();
               DateTime time = GetTempLinerZondT(general.CnvNo, general.HeatDuration.Start, general.HeatDuration.End,
                                        general.HeatId);
               if (time != DateTime.MinValue)
               {
                   sql =
                       "SELECT PAR2, EVENTTIME FROM MODEL_TRENDS WHERE OPERATION_ID=35 AND HEAT_NO=:HEAT_NO AND EVENTTIME = :EVENTTIME ";
                   param.Clear();
                   param.Add(SetParams("HEAT_NO", general.HeatNumber));
                   param.Add(SetParams("EVENTTIME", time));
                   read = Execute(sql, param);
                   while (read.Read())
                   {
                       modelInfo.T_TempLinerZondT = CheckNubmerForNullAsDouble(read[0].ToString());
                   }

                   read.Close();
               }


               if (general.SteelXimCulc != null)
                    modelInfo.CurrentCCheh = general.SteelXimCulc.C;
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
            return modelInfo;
        }



        private Charge GetCharge(General general)
        {
            var charge = new Charge();
           
                string sql =
                    "SELECT EVENTTIME, PAR4, PAR5, PAR6, PAR7, PAR8, PAR9 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 29";
                var param = new List<OracleParameter>
                    {
                        SetParams("HEAT_NO", general.HeatNumber)
                    };
                var read = Execute(sql, param);
           try
           {
                if (read.Read())
                {
                    charge.SteelGroup = CheckNubmerForNullAsInt(read[1].ToString());
                    charge.Target_C = CheckNubmerForNullAsDouble(read[2].ToString());
                    charge.Target_P = CheckNubmerForNullAsDouble(read[3].ToString());
                    charge.Target_FeO = CheckNubmerForNullAsDouble(read[4].ToString());
                    charge.Target_MgO = CheckNubmerForNullAsDouble(read[5].ToString());
                    charge.Target_CaOdivSiO2 = CheckNubmerForNullAsDouble(read[6].ToString());
                }
                read.Close();

                sql =
                    "SELECT PAR1, PAR2, PAR3, PAR4, PAR5, PAR6 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 30";
                read = Execute(sql, param);
                if (read.Read())
                {
                    charge.Target_Temperature = CheckNubmerForNullAsDouble(read[0].ToString());
                    charge.Result_Iron = CheckNubmerForNullAsDouble(read[1].ToString());
                    charge.Result_Scrap = CheckNubmerForNullAsDouble(read[2].ToString());
                    charge.Charge_ = CheckNubmerForNullAsInt(read[3].ToString());
                    charge.Balance_Iron = CheckNubmerForNullAsDouble(read[4].ToString());
                    charge.Balance_Scrap = CheckNubmerForNullAsDouble(read[5].ToString());
                }
                read.Close();

                sql =
                    "SELECT PAR1, PAR2, PAR3, PAR4 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 31";
                read = Execute(sql, param);
                if (read.Read())
                {
                    charge.Statistic_K_Iron = CheckNubmerForNullAsDouble(read[0].ToString());
                    charge.Statistic_K_Scrap = CheckNubmerForNullAsDouble(read[1].ToString());
                    charge.Statistic_AB_Iron = CheckNubmerForNullAsDouble(read[2].ToString());
                    charge.Statistic_AB_Scrap = CheckNubmerForNullAsDouble(read[3].ToString());
                }
                read.Close();

                sql =
                    "SELECT   PAR4, PAR5 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 32 ORDER BY EVENTTIME DESC";
                read = Execute(sql, param);
                if (read.Read())
                {
                    charge.FluxWeight1_2 = CheckNubmerForNullAsDouble(read[0].ToString());
                    charge.FluxWeight2_2 = CheckNubmerForNullAsDouble(read[1].ToString());
                }
                read.Close();

                sql =
                    "SELECT   PAR4, PAR5 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 33 ORDER BY EVENTTIME DESC";
                read = Execute(sql, param);
                if (read.Read())
                {
                    charge.FluxWeight1_7 = CheckNubmerForNullAsDouble(read[0].ToString());
                    charge.FluxWeight2_7 = CheckNubmerForNullAsDouble(read[1].ToString());
                }
                read.Close();

                sql =
    "SELECT   STEEL_GROUP FROM  TARGET_EVENTS  WHERE  HEAT_ID = :HEAT_ID";
                param.Clear();
                param.Add(SetParams("HEAT_ID", general.HeatId));
                read = Execute(sql, param);
                if (read.Read())
                {
                    charge.SteelGroupCell = CheckNubmerForNullAsInt(read[0].ToString());
                }
                read.Close();
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
            return charge;
        }


       


        private HeatQuality GetHeatQuality(General general)
        {
            var heatQuality = new HeatQuality();
            try
            {
                if (general.HotMetalXim1 != null)
                    heatQuality.IronSi = general.HotMetalXim1.Si;
                if (general.SlagXim1 != null)
                    heatQuality.SlagCao = general.SlagXim1.CaO;
                if (general.Converter != null)
                {
                    heatQuality.HeatIzvestWeight = general.Converter.Izvest;
                    heatQuality.AlConWeight = general.Converter.AlKonc;


                }

                heatQuality = GetMetalRange(heatQuality, general);
                if (general.Scraps == null) general.Scraps = new List<Scrap>();
                foreach (var scrap in general.Scraps)
                {
                    string[] s = scrap.ScrapType.Split('-');
                    if (s.Last() != "")
                        heatQuality.Scraps.Add(int.Parse(s.Last()));

                }
                heatQuality.AlConInRange1 = GetAlconcAdditionCv(general, 8000, 13000);
                heatQuality.AlConInRange2 = GetAlconcAdditionCv(general, 13000, 17000);
                heatQuality.SlagButton = general.SlagOutbust;
                heatQuality.MaxConverterAngle = GetModesOnHeatQuality(general.CnvNo, general.SlagOutDuration.Start,
                                                                      general.SlagOutDuration.End);
                //-5 некоретная работа фурнм -3 тербуется охлаждение                                             general.HeatDuration.End);

                heatQuality.ConverterAngleInRange = GetModesOnHeatQuality(general.CnvNo,
                                                                          general.SlagOutDuration.Start,
                                                                          general.SlagOutDuration.End) > 145;

                heatQuality = GetVisValues(heatQuality, general);

                heatQuality = GetTransDelay(heatQuality, general);
                heatQuality = GetMlnz(heatQuality, general);
            }
            catch (Exception exception)
            {
                InstantLogger.err(exception.ToString());
            }
            return heatQuality;

        }

        private HeatQuality GetMlnz(HeatQuality heatQuality, General general)
        {
           
                var sql = "SELECT H.HEAT_NO " +
                          ",H.DT_BEGIN, P.NUNRS, PCK_UNRS_UTILS.FGHWEIGHT(S1.STRAND_ID) + PCK_UNRS_UTILS.FGHWEIGHT(S2.STRAND_ID) AS GHWEIGHT_TOTAL " +
                          ",PCK_UNRS_UTILS.FRHWEIGHT(S1.STRAND_ID) + PCK_UNRS_UTILS.FRHWEIGHT(S2.STRAND_ID) AS RHWEIGHT_TOTAL " +
                          " FROM HEATS       H ,UNRS_PASP   P ,UNRS_STRAND S1 ,UNRS_STRAND S2 WHERE H.DT_BEGIN > SYSDATE - 200" +
                          " AND P.NPLAVKA = SUBSTR(H.HEAT_NO, 1, 2) || SUBSTR(H.HEAT_NO, 4, 4) AND S1.IDD_NO = P.IDD_NO AND S1.NSTRAND = P.NUNRS * 2" +
                          " AND S2.IDD_NO = P.IDD_NO AND S2.NSTRAND = P.NUNRS * 2 AND HEAT_NO=:HEAT_NO";
                var param = new List<OracleParameter>
                    {
                        SetParams("HEAT_NO", general.HeatNumber)
                    };
                var reader = Execute(sql, param);
           try
           {
                if (reader.Read())
                {
                    heatQuality.GoodMlnz = CheckNubmerForNullAsDouble(reader[3].ToString());
                    heatQuality.ColdMlnz = CheckNubmerForNullAsDouble(reader[4].ToString());
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return heatQuality;
        }

        private HeatQuality GetTransDelay(HeatQuality heatQuality,General general)
        {
            
                string sql =
                    "SELECT MAX(TRANSPORTDELAY1) FROM TREND_OFFGASANALYSE WHERE   BRANCH=1 AND CNV_NO = :CNV_NO AND INSERTTIME BETWEEN :DSTART AND :DEND";
                var param = MandatoryParams(general.CnvNo, general.HeatDuration.End.AddMinutes(-2),
                                            general.HeatDuration.End.AddMinutes(10));
                var reader = Execute(sql, param);
           try
           {
                double delay1 = -1;
                double delay2 = -1;
                if (reader.Read())
                {
                    delay1 = CheckNubmerForNullAsDouble(reader[0].ToString());
                }
                reader.Close();
                sql =
                    "SELECT MAX(TRANSPORTDELAY2) FROM TREND_OFFGASANALYSE WHERE   BRANCH=2 AND CNV_NO = :CNV_NO AND INSERTTIME BETWEEN :DSTART AND :DEND";
                reader = Execute(sql, param);
                if (reader.Read())
                {

                    delay2 = CheckNubmerForNullAsDouble(reader[0].ToString());
                }
                reader.Close();
                heatQuality.TransDelay = Math.Max(delay1, delay2);
                sql =
                    "SELECT MAX(TRANSPORTDELAY1), MAX(TRANSPORTDELAY2) FROM TREND_OFFGASANALYSE WHERE CNV_NO = :CNV_NO AND INSERTTIME BETWEEN :DSTART AND :DEND";
                reader = Execute(sql, param);
                if (reader.Read())
                {
                    delay1 = CheckNubmerForNullAsDouble(reader[0].ToString());
                    delay2 = CheckNubmerForNullAsDouble(reader[1].ToString());
                }
                heatQuality.TransADelay = delay2;
                heatQuality.TransBDelay = delay1;
                param.Clear();
                param = MandatoryParams(general.CnvNo, general.HeatDuration.Start, general.HeatDuration.End);

                sql =
                    "SELECT MAX(DIAGBYTE) FROM TREND_OFFGASANALYSE WHERE  CNV_NO = :CNV_NO AND INSERTTIME BETWEEN :DSTART AND :DEND";

                reader = Execute(sql, param);
                if (reader.Read())
                {

                    heatQuality.Diag = CheckNubmerForNullAsDouble(reader[0].ToString());
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return heatQuality;
        }

        private HeatQuality GetVisValues(HeatQuality heatQuality, General general)
        {
           
                var sql =
                    "SELECT PAR1, EVENTTIME FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 11 ORDER BY EVENTTIME";
                var param = new List<OracleParameter>
                    {
                        SetParams("HEAT_NO", general.HeatNumber)

                    };
                var read = Execute(sql, param);
           try
           {
                while (read.Read())
                {
                    heatQuality.LanceCorrection.Add("(Значение = " + read[0] + " Время " +
                                                    CheckDateForNull(read[1].ToString()).TimeOfDay.ToString() + ")");
                }



                read.Close();

                sql =
                    "SELECT PAR1,PAR2,PAR3,PAR4,PAR5,PAR6,PAR7,PAR8 EVENTTIME FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 24 ORDER BY EVENTTIME DESC";
                param = new List<OracleParameter>
                    {
                        SetParams("HEAT_NO", general.HeatNumber)

                    };
                read = Execute(sql, param);
                if (read.Read())
                {
                    for (int i = 0; i < 8; i++)
                    {
                        heatQuality.SublanceControl.Add(CheckNubmerForNullAsDouble(read[i].ToString()));
                    }

                }



                read.Close();

                sql =
                    "SELECT PAR1, EVENTTIME FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 12 ORDER BY EVENTTIME";


                read = Execute(sql, param);
                while (read.Read())
                {
                    heatQuality.IntenceCorrection.Add("(Значение = " + read[0] + " Время " +
                                                      CheckDateForNull(read[1].ToString()).TimeOfDay.ToString() + ")");
                }



                read.Close();

                sql =
                    "SELECT PAR1, EVENTTIME FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 13 ORDER BY EVENTTIME";


                read = Execute(sql, param);
                while (read.Read())
                {
                    heatQuality.O2Correction.Add("(Значение = " + read[0] + " Время " +
                                                 CheckDateForNull(read[1].ToString()).TimeOfDay.ToString() + ")");
                }
                read.Close();
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
            return heatQuality;
        }

        private int GetModesOnHeatQuality(int iCnvNr, DateTime start, DateTime end)
        {
            var res = 0;
           
                const string sql = "SELECT  MAX(ANGLE) " +
                                   "FROM TREND_MODES WHERE CNV_NO = :CNV_NO AND INSERTTIME BETWEEN :DSTART AND :DEND ";
                var param = MandatoryParams(iCnvNr, start, end);
                var reader = Execute(sql, param);
           try
           {
                while (reader.Read())
                {
                    res = CheckNubmerForNullAsInt(reader[0].ToString());
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return res;
        }

        public General GetBofDataInTime(General general)
        {
            InstantLogger.log("",string.Format("{0} GET DATA IN TIME",general.HeatNumber) );
            general.HeatInTime = GetHeatinTime(general);
            InstantLogger.log("", string.Format("{0} GET DATA IN TIME. DONE", general.HeatNumber));
            return general;
        }

        
        private List<AddMaterialTrend> GetAdditionsMatTrends(int iCnvNr, DateTime start, DateTime end)
        {
            var list = new List<AddMaterialTrend>();
            
                const string sql = "SELECT PAR1, PAR3, EVENTTIME " +
                                   "FROM BOF_TELEGRAMS WHERE  OPER_ID=1010 AND PAR2 = 'CV' AND CV_NO = :CNV_NO AND EVENTTIME BETWEEN :DSTART AND :DEND ";
                var param = MandatoryParams(iCnvNr, start, end);
                var reader = Execute(sql, param);
           try
           {
                while (reader.Read())
                {
                    list.Add(new AddMaterialTrend
                        {
                            MaterialName = reader[0].ToString(),
                            Weight = CheckNubmerForNullAsInt(reader[1].ToString()),
                            Time = CheckDateForNull(reader[2].ToString())
                        });
                }
                reader.Close();
                foreach (var mat in list)
                {
                    foreach (var addMaterialTrend in list)
                    {
                        if (addMaterialTrend != mat)
                            if (addMaterialTrend.Time == mat.Time)
                                addMaterialTrend.Time = addMaterialTrend.Time.AddSeconds(1);

                    }
                }
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return list;
        }


        public List<StatisticTrend> GetCurkUniversalC(int iCnvNr, DateTime start, DateTime end)
        {
            var list = new List<StatisticTrend>();
            
                const string sql = "SELECT PAR1, EVENTTIME " +
                                   "FROM MODEL_TRENDS WHERE OPERATION_ID=27 AND CV_NO = :CNV_NO AND EVENTTIME BETWEEN :DSTART AND :DEND ";

                var param = MandatoryParams(iCnvNr, start, end);
                var reader = Execute(sql, param);
            try
            {
                while (reader.Read())
                {
                    list.Add(new StatisticTrend
                        {
                            UniversalCPlusResult = CheckNubmerForNullAsDouble(reader[0].ToString()),
                            TimeC = CheckDateForNull(reader[1].ToString())
                        });
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }

            return list;
        }



        public List<TempLiner_Result> GetTemperatureLinear(int iCnvNr, DateTime start, DateTime end)
        {
            var list = new List<TempLiner_Result>();

            const string sql = "SELECT PAR2, EVENTTIME " +
                               "FROM MODEL_TRENDS WHERE OPERATION_ID=35 AND CV_NO = :CNV_NO AND EVENTTIME BETWEEN :DSTART AND :DEND ";

            var param = MandatoryParams(iCnvNr, start, end);
            var reader = Execute(sql, param);
            try
            {
                while (reader.Read())
                {
                    list.Add(new TempLiner_Result
                    {
                        Temperature = CheckNubmerForNullAsDouble(reader[0].ToString()),
                        TimeTemp = CheckDateForNull(reader[1].ToString())
                    });
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }

            return list;
        }



        public List<CorrectionCT_Result> GetCorrectionCT_Result(int iCnvNr, DateTime start, DateTime end)
        {
            var list = new List<CorrectionCT_Result>();

            const string sql = "SELECT PAR1, EVENTTIME " +
                               "FROM MODEL_TRENDS WHERE OPERATION_ID=37 AND CV_NO = :CNV_NO AND EVENTTIME BETWEEN :DSTART AND :DEND ";

            var param = MandatoryParams(iCnvNr, start, end);
            var reader = Execute(sql, param);
            try
            {
                while (reader.Read())
                {
                    list.Add(new CorrectionCT_Result
                    {
                        C = CheckNubmerForNullAsDouble(reader[0].ToString()),
                        Time = CheckDateForNull(reader[1].ToString())
                    });
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }

            return list;
        }


    /*    private Charge GetCharge(General general)
        {
            var charge = new Charge();

            string sql =
                "SELECT EVENTTIME, PAR4, PAR5, PAR6, PAR7, PAR8, PAR9 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 29";
            var param = new List<OracleParameter>
                    {
                        SetParams("HEAT_NO", general.HeatNumber)
                    };
            var read = Execute(sql, param);
            try
            {
                if (read.Read())
                {
                    charge.SteelGroup = CheckNubmerForNullAsInt(read[1].ToString());
                    charge.Target_C = CheckNubmerForNullAsDouble(read[2].ToString());
                    charge.Target_P = CheckNubmerForNullAsDouble(read[3].ToString());
                    charge.Target_FeO = CheckNubmerForNullAsDouble(read[4].ToString());
                    charge.Target_MgO = CheckNubmerForNullAsDouble(read[5].ToString());
                    charge.Target_CaOdivSiO2 = CheckNubmerForNullAsDouble(read[6].ToString());
                }
                read.Close();

                sql =
                    "SELECT PAR1, PAR2, PAR3, PAR4, PAR5, PAR6 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 30";
                read = Execute(sql, param);
                if (read.Read())
                {
                    charge.Target_Temperature = CheckNubmerForNullAsDouble(read[0].ToString());
                    charge.Result_Iron = CheckNubmerForNullAsDouble(read[1].ToString());
                    charge.Result_Scrap = CheckNubmerForNullAsDouble(read[2].ToString());
                    charge.Charge_ = CheckNubmerForNullAsInt(read[3].ToString());
                    charge.Balance_Iron = CheckNubmerForNullAsDouble(read[4].ToString());
                    charge.Balance_Scrap = CheckNubmerForNullAsDouble(read[5].ToString());
                }
                read.Close();

                sql =
                    "SELECT PAR1, PAR2, PAR3, PAR4 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 31";
                read = Execute(sql, param);
                if (read.Read())
                {
                    charge.Statistic_K_Iron = CheckNubmerForNullAsDouble(read[0].ToString());
                    charge.Statistic_K_Scrap = CheckNubmerForNullAsDouble(read[1].ToString());
                    charge.Statistic_AB_Iron = CheckNubmerForNullAsDouble(read[2].ToString());
                    charge.Statistic_AB_Scrap = CheckNubmerForNullAsDouble(read[3].ToString());
                }
                read.Close();

                sql =
                    "SELECT   PAR4, PAR5 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 32 ORDER BY EVENTTIME DESC";
                read = Execute(sql, param);
                if (read.Read())
                {
                    charge.FluxWeight1_2 = CheckNubmerForNullAsDouble(read[0].ToString());
                    charge.FluxWeight2_2 = CheckNubmerForNullAsDouble(read[1].ToString());
                }
                read.Close();

                sql =
                    "SELECT   PAR4, PAR5 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 33 ORDER BY EVENTTIME DESC";
                read = Execute(sql, param);
                if (read.Read())
                {
                    charge.FluxWeight1_7 = CheckNubmerForNullAsDouble(read[0].ToString());
                    charge.FluxWeight2_7 = CheckNubmerForNullAsDouble(read[1].ToString());
                }
                read.Close();

                sql =
    "SELECT   STEEL_GROUP FROM  TARGET_EVENTS  WHERE  HEAT_ID = :HEAT_ID";
                param.Clear();
                param.Add(SetParams("HEAT_ID", general.HeatId));
                read = Execute(sql, param);
                if (read.Read())
                {
                    charge.SteelGroupCell = CheckNubmerForNullAsInt(read[0].ToString());
                }
                read.Close();
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
            return charge;
        }*/

        private ReferencePoint GetReferencePoint(General general)
        {
            var referencePoint = new ReferencePoint();

            string sql =
                "SELECT EVENTTIME, PAR2, PAR3 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 36";
            var param = new List<OracleParameter>
                    {
                        SetParams("HEAT_NO", general.HeatNumber)
                    };
            var read = Execute(sql, param);
            try
            {
                if (read.Read())
                {
                    referencePoint.TimeRefPoint = CheckDateForNull(read[0].ToString());
                    referencePoint.O2 = CheckNubmerForNullAsInt(read[1].ToString());
                    referencePoint.C = CheckNubmerForNullAsDouble(read[2].ToString());
                }
                read.Close();


            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
            return referencePoint; 
        }

     /*   private List<ReferencePoint> GetReferencePoint(int iCnvNr, DateTime start, DateTime end)
        {
            var list = new List<ReferencePoint>();

            const string sql =
                "SELECT EVENTTIME, PAR2, PAR3 FROM  MODEL_TELEGRAMS  WHERE  OPERATION_ID = 36 AND CV_NO = :CNV_NO AND EVENTTIME BETWEEN :DSTART AND :DEND";

            var param = MandatoryParams(iCnvNr, start, end);
            var reader = Execute(sql, param);
            try
            {
                while (reader.Read())
                {
                    list.Add(new ReferencePoint
                    {
                        TimeRefPoint = CheckDateForNull(reader[0].ToString()),
                        O2= CheckNubmerForNullAsInt(reader[1].ToString()),
                        C = CheckNubmerForNullAsDouble(reader[2].ToString())
                    });
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }

            return list;





/*

            var referencePoint = new ReferencePoint();

            string sql =
                "SELECT EVENTTIME, PAR2, PAR3 FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 36";
            var param = new List<OracleParameter>
                    {
                        SetParams("HEAT_NO", general.HeatNumber)
                    };
            var read = Execute(sql, param);
            try
            {
                if (read.Read())
                {
                    referencePoint.TimeRefPoint = CheckDateForNull(read[0].ToString());
                    referencePoint.O2 = CheckNubmerForNullAsInt(read[1].ToString());
                    referencePoint.C = CheckNubmerForNullAsDouble(read[2].ToString());
                }
                read.Close();


            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
            return referencePoint;
 * */
       // }


        public List<StatisticTrend> GetCurk(int iCnvNr, DateTime start, DateTime end)
        {
            var list = new List<StatisticTrend>();
            
                const string sql = "SELECT PAR1, EVENTTIME " +
                                   "FROM MODEL_TRENDS WHERE OPERATION_ID=7 AND CV_NO = :CNV_NO AND EVENTTIME BETWEEN :DSTART AND :DEND ";
                var param = MandatoryParams(iCnvNr, start, end);
                var reader = Execute(sql, param);
           try
           {
                while (reader.Read())
                {
                    list.Add(new StatisticTrend
                        {
                            C = CheckNubmerForNullAsDouble(reader[0].ToString()),
                            Time = CheckDateForNull(reader[1].ToString())
                        });
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return list;
        }

        public List<Template> GetTemplates(General general,List<LanceEvent> lance)
        {
            var result = new List<Template>();
            
                var materials = new List<Materials>();
                string sql = "SELECT PAR1,PAR2, PAR5, PAR6 " +
                             "FROM MODEL_TELEGRAMS WHERE OPERATION_ID=15 AND HEAT_NO=:HEAT_NO ORDER BY PAR2";
                var param = new List<OracleParameter>
                    {
                        SetParams("HEAT_NO", general.HeatNumber),
                    };
                var reader = Execute(sql, param);
           try
           {
                while (reader.Read())
                {
                    result.Add(new Template
                        {
                            TotalO2Flow = CheckNubmerForNullAsInt(reader[1].ToString()),
                            Step = CheckNubmerForNullAsInt(reader[0].ToString()),
                            LancePosition = CheckNubmerForNullAsInt(reader[2].ToString()),
                            O2Flow = CheckNubmerForNullAsInt(reader[3].ToString()),

                        });
                }
                reader.Close();
                sql = "SELECT PAR1,PAR2,PAR3,PAR4, PAR5, PAR6 " +
                      "FROM MODEL_TELEGRAMS WHERE OPERATION_ID=16 AND HEAT_NO=:HEAT_NO ORDER BY PAR2";
                reader = Execute(sql, param);
                while (reader.Read())
                {
                    var step = CheckNubmerForNullAsInt(reader[0].ToString());
                    var mat = new Materials
                        {
                            Bunker = CheckNubmerForNullAsInt(reader[1].ToString()),
                            MaterialName = reader[2].ToString(),
                            Weight = CheckNubmerForNullAsDouble(reader[3].ToString()),
                            AllowToAdd = reader[4].ToString(),
                            NotToGive = reader[5].ToString()
                        };
                    var step1 = result.FirstOrDefault(x => x.Step == step);
                    if (step1 != null)
                    {
                        step1.Materials.Add(mat);
                    }
                }
                reader.Close();


                foreach (var template in result)
                {
                    var time =
                        lance.Where(x => x.O2TotalVol >= template.TotalO2Flow).OrderBy(x => x.Time).Select(x => x.Time).
                              FirstOrDefault();
                    template.Time = time;
                    if (time == DateTime.MinValue)
                    {
                        template.Time = lance.Where(x => x.O2TotalVol < template.TotalO2Flow)
                                             .OrderByDescending(x => x.Time)
                                             .Select(x => x.Time)
                                             .
                                              FirstOrDefault().AddSeconds(-(result.Count - template.Step - 1));
                    }
                }
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return result;
        }
        public ZondInTime GetZondInTime (List<ZondInTime> zond,DateTime time )
        {
            if (zond.Count(x=>x.Time == time)>0)
            {
                return zond.FirstOrDefault(x => x.Time == time);
            }
            var z = new ZondInTime
                {
                    Level = -1,
                    T = -1,
                    Time = time,
                    ZondAutoStartCommand = -1,
                    ZondStartCommand = -1

                };
            zond.Add(z);
            return z;
        }
        public List<ZondInTime> GetZondInTime(General general)
        {
            List<ZondInTime> zond = new List<ZondInTime>();
           
                var sql = "SELECT EVENTTIME, PAR1 " +
                          "FROM BOF_TELEGRAMS WHERE OPER_ID=1160 AND HEAT_NO=:HEAT_NO ORDER BY EVENTTIME";
                var param = new List<OracleParameter>
                    {
                        SetParams("HEAT_NO", general.HeatNumber),
                    };
                var reader = Execute(sql, param);
         try
         {
                while (reader.Read())
                {
                    var time = CheckDateForNull(reader[0].ToString());
                    var z = GetZondInTime(zond, time);
                    z.T = CheckNubmerForNullAsInt(reader[1].ToString());
                }
                reader.Close();
                sql = "SELECT EVENTTIME, PAR1 " +
                      "FROM BOF_TELEGRAMS WHERE OPER_ID=1190 AND HEAT_NO=:HEAT_NO AND PAR1>0 ORDER BY EVENTTIME";
                reader = Execute(sql, param);

                while (reader.Read())
                {
                    var time = CheckDateForNull(reader[0].ToString());
                    var z = GetZondInTime(zond, time);
                    z.Level = CheckNubmerForNullAsInt(reader[1].ToString());
                }
                reader.Close();

                sql =
                    "SELECT EVENTTIME FROM  MODEL_TELEGRAMS  WHERE  HEAT_NO = :HEAT_NO AND OPERATION_ID = 9 AND PAR1=1 ORDER BY EVENTTIME";
                reader = Execute(sql, param);
                while (reader.Read())
                {
                    var time = CheckDateForNull(reader[0].ToString());
                    var z = GetZondInTime(zond, time);
                    z.ZondAutoStartCommand = 1;
                }
                reader.Close();

                sql = "SELECT EVENTTIME FROM  HEAT_EVENTS " +
                      " WHERE  HEAT_ID = :HEAT_ID AND TYPE = 'SUBS' ORDER BY EVENTTIME";
                param.Clear();
                param.Add(SetParams("HEAT_ID", general.HeatId));
                reader = Execute(sql, param);
                while (reader.Read())
                {
                    var time = CheckDateForNull(reader[0].ToString());
                    var z = GetZondInTime(zond, time);
                    z.ZondStartCommand = 1;
                }

                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return zond;
        }

        public SublanceControlInTimeExport GetOrAddSublanceControlInTimeExport( List<SublanceControlInTimeExport> list,DateTime time)
        {
            if (list.Count(x=>x.Time == time)==0)
            {
               list.Add(new SublanceControlInTimeExport{Time = time}); 
            }
            return list.First(x => x.Time == time);
        }
        private List<SublanceControlInTimeExport> GetSublanceInTime(General general)
        {
                var sql = "SELECT EVENTTIME, PAR1 " +
                          "FROM MODEL_TELEGRAMS WHERE OPERATION_ID=26 AND HEAT_NO=:HEAT_NO ORDER BY EVENTTIME";
                var param = new List<OracleParameter>
                    {
                        SetParams("HEAT_NO", general.HeatNumber),
                    };
                var reader = Execute(sql, param);
                var listDelay = new List<SublanceDelay>();
                while (reader.Read())
                {
                    listDelay.Add(new SublanceDelay
                        {

                            Time = CheckDateForNull(reader[0].ToString()),
                            Value = CheckNubmerForNullAsDouble(reader[1].ToString())

                        });

                }
                reader.Close();
                sql = "SELECT EVENTTIME, PAR1, PAR2,PAR3 " +
                      "FROM MODEL_TELEGRAMS WHERE OPERATION_ID=26 AND HEAT_NO=:HEAT_NO ORDER BY EVENTTIME";
                reader = Execute(sql, param);
                var listInTime = new List<SublanceControlInTime>();
                while (reader.Read())
                {
                    listInTime.Add(new SublanceControlInTime
                        {
                            Time = CheckDateForNull(reader[0].ToString()),
                            O2Switch = CheckBoolForNullAsInt(reader[1].ToString()),
                            BathDelay = CheckBoolForNullAsInt(reader[2].ToString()),
                            Mes = CheckBoolForNullAsInt(reader[3].ToString())
                        });
                }

                var res =
                    listDelay.Select(
                        sublanceControlInTime =>
                        new SublanceControlInTimeExport
                            {
                                Time = sublanceControlInTime.Time,
                                Delay = sublanceControlInTime.Value
                            }).ToList();
                bool lanceBathStart = false;
                bool lanceBathDelay = false;
                bool o2Bath = false;
                bool lanceMes = false;
                bool o2Mes = false;
                foreach (var sublanceControlInTime in listInTime.OrderBy(x => x.Time))
                {
                    if (sublanceControlInTime.O2Switch == 1 && sublanceControlInTime.Mes == 1)
                    {
                        var x = GetOrAddSublanceControlInTimeExport(res, sublanceControlInTime.Time);
                        x.LanceBath = 1;
                        lanceBathStart = true;
                    }
                    if (sublanceControlInTime.BathDelay == 0 && sublanceControlInTime.Mes == 1 && lanceBathStart)
                    {
                        var x = GetOrAddSublanceControlInTimeExport(res, sublanceControlInTime.Time);
                        x.LanceBath = 0;
                        lanceBathStart = false;
                    }

                    if (sublanceControlInTime.BathDelay == 1 && sublanceControlInTime.Mes == 1)
                    {
                        var x = GetOrAddSublanceControlInTimeExport(res, sublanceControlInTime.Time);
                        x.BathDelay = 1;
                        lanceBathDelay = true;
                    }
                    if (sublanceControlInTime.BathDelay == 0 && sublanceControlInTime.Mes == 1 && lanceBathDelay)
                    {
                        var x = GetOrAddSublanceControlInTimeExport(res, sublanceControlInTime.Time);
                        x.BathDelay = 0;
                        lanceBathDelay = false;

                    }

                    if (sublanceControlInTime.O2Switch == 1 && sublanceControlInTime.Mes == 1)
                    {
                        var x = GetOrAddSublanceControlInTimeExport(res, sublanceControlInTime.Time);
                        x.O2Bath = 1;
                        o2Bath = true;
                    }
                    if (sublanceControlInTime.O2Switch == 0 && sublanceControlInTime.Mes == 1 && o2Bath)
                    {
                        var x = GetOrAddSublanceControlInTimeExport(res, sublanceControlInTime.Time);
                        x.O2Bath = 0;
                        o2Bath = false;
                    }

                    if (sublanceControlInTime.BathDelay == 0 && sublanceControlInTime.Mes == 1)
                    {
                        var x = GetOrAddSublanceControlInTimeExport(res, sublanceControlInTime.Time);
                        x.LanceMes = 1;
                        lanceMes = true;
                    }
                    if (sublanceControlInTime.Mes == 0 && lanceMes)
                    {
                        var x = GetOrAddSublanceControlInTimeExport(res, sublanceControlInTime.Time);
                        x.LanceMes = 0;
                        lanceMes = false;
                    }

                    if (sublanceControlInTime.O2Switch == 0 && sublanceControlInTime.Mes == 1)
                    {
                        var x = GetOrAddSublanceControlInTimeExport(res, sublanceControlInTime.Time);
                        x.O2Mes = 1;
                        o2Mes = true;
                    }
                    if (sublanceControlInTime.Mes == 0 && o2Mes)
                    {
                        var x = GetOrAddSublanceControlInTimeExport(res, sublanceControlInTime.Time);
                        x.LanceMes = 0;
                        o2Mes = false;
                    }

                }

            return res.OrderBy(x=>x.Time).ToList();
        }
        private List<HeatInTime> GetHeatinTime(General general)
        {
            int iCnvNr = general.CnvNo;
            DateTime start = general.HeatDuration.Start;
            DateTime end = general.HeatDuration.End;
            var min = general.HeatDuration.Start;

            var max = general.HeatDuration.End;
            InstantLogger.msg("{0} get balace", general.HeatNumber);
            var balance =  GetBalancOnHeat(iCnvNr, start, end);
            InstantLogger.msg("{0} get lance", general.HeatNumber);
            var lanse = GetLanceOnHeat(iCnvNr, start, end);
            InstantLogger.msg("{0} get off gas", general.HeatNumber);
            var offGas = GetOffGasOnHeat(iCnvNr, start, end);
            InstantLogger.msg("{0} get off gas analysis", general.HeatNumber);
            var offGasAnalysis = GetOffGasAnalysisOnHeat(iCnvNr, start, end);
            InstantLogger.msg("{0} get modes", general.HeatNumber);
            var modes = GetModesOnHeat(iCnvNr, start, end);
            InstantLogger.msg("{0} get statistic model", general.HeatNumber);
            var statistic = GetStatisticOnHeat(iCnvNr, start, end);
            InstantLogger.msg("{0} get gas flow", general.HeatNumber);
            var gasFlow = GetGasFlow(iCnvNr, start, end);
            InstantLogger.msg("{0} get tor", general.HeatNumber);
            var dur = GetTor(general);
            InstantLogger.msg("{0} get curk", general.HeatNumber);
            var curk = GetCurk(iCnvNr, start, end);
            InstantLogger.msg("{0} get universalCPlus", general.HeatNumber);
            var UniversalCPlusResult = GetCurkUniversalC(iCnvNr, start, end);
            InstantLogger.msg("{0} get temperatureLinear", general.HeatNumber);
            var TemperatureLinear = GetTemperatureLinear(iCnvNr, start, end);
            InstantLogger.msg("{0} get correctionCT result", general.HeatNumber);
            var CorrectionCT = GetCorrectionCT_Result(iCnvNr, start, end);
        //    InstantLogger.msg("{0} get referencePoint", general.HeatNumber);
         //   var RefPoint = GetReferencePoint(iCnvNr, start, end); 
            InstantLogger.msg("{0}. get reference point", general.HeatNumber);
            var RefPoint = GetReferencePoint(general);

            InstantLogger.msg("{0} get additions", general.HeatNumber);
            var templ = GetTemplates(general,lanse);
            var sublances = GetZondInTime(general);

            var subControl = GetSublanceInTime(general);
            InstantLogger.msg("{0} get template", general.HeatNumber);

            List<AddMaterialTrend> additions;
            if (dur != null)
            {
                additions = GetAdditionsMatTrends(iCnvNr, dur.End, end);
            }
            else
            {
                  additions = GetAdditionsMatTrends(iCnvNr, start, end);
            }
              
          /*  if (lanse != null && lanse.Count != 0)
            {
                min = lanse.Min(x => x.O2);
                max = lanse.Max(x => x.O2);
            }
            if (offGas != null && offGas.Count != 0)
            {
                var minOg = offGas.Min(x => x.O2);
                min = minOg < min ? minOg : min;
                var maxOg = offGas.Max(x => x.O2);
                max = maxOg > max ? maxOg : max;
            }
            if (offGasAnalysis != null && offGasAnalysis.Count != 0)
            {
                var minT = offGasAnalysis.Min(x => x.O2);
                min = minT < min ? minT : min;
                var maxT = offGasAnalysis.Max(x => x.O2);
                max = maxT > max ? maxT : max;
            }
            if (balance != null && balance.Count != 0)
            {
                var minT = balance.Min(x => x.O2);
                min = minT < min ? minT : min;
                var maxT = balance.Max(x => x.O2);
                max = maxT > max ? maxT : max;
            }
            if (modes != null && modes.Count != 0)
            {
                var minT = modes.Min(x => x.O2);
                min = minT < min ? minT : min;
                var maxT = modes.Max(x => x.O2);
                max = maxT > max ? maxT : max;
            }
            if (statistic != null && statistic.Count != 0)
            {
                var minT = statistic.Min(x => x.O2);
                min = minT < min ? minT : min;
                var maxT = statistic.Max(x => x.O2);
                max = maxT > max ? maxT : max;
            }
            if (gasFlow != null && gasFlow.Count != 0)
            {
                var minT = gasFlow.Min(x => x.O2);
                min = minT < min ? minT : min;
                var maxT = gasFlow.Max(x => x.O2);
                max = maxT > max ? maxT : max;
            }

            if (additions != null && additions.Count != 0)
            {
                var minT = additions.Min(x => x.O2);
                min = minT < min ? minT : min;
                var maxT = additions.Max(x => x.O2);
                max = maxT > max ? maxT : max;
            }
            if (curk != null && curk.Count != 0)
            {
                var minT = curk.Min(x => x.O2);
                min = minT < min ? minT : min;
                var maxT = curk.Max(x => x.O2);
                max = maxT > max ? maxT : max;
            }
            if (templ!=null&&templ.Count!=0)
            {
                var minT = templ.Min(x => x.O2);
                min = minT < min ? minT : min;
                var maxT = templ.Max(x => x.O2);
                max = maxT > max ? maxT : max; 
            }*/
            var heatinTime = new List<HeatInTime>();
            if (min == DateTime.MinValue || max == DateTime.MinValue) return null;
            var t = min;
            while (t != max.AddSeconds(1))
            {
                var b = balance.FirstOrDefault(x => x.Time == t);
                var l = lanse.FirstOrDefault(x => x.Time == t);
                var m = modes.FirstOrDefault(x => x.Time == t);
                var og = offGas.FirstOrDefault(x => x.Time == t);
                var oga = offGasAnalysis.FirstOrDefault(x => x.Time == t);
                var s = statistic.FirstOrDefault(x => x.Time == t);
                var gf = gasFlow.FirstOrDefault(x => x.Time == t);
                var ad = additions.FirstOrDefault(x => x.Time == t);
                var cu = curk.FirstOrDefault(x => x.Time == t);
                var universalCPl = UniversalCPlusResult.FirstOrDefault(x => x.TimeC == t);
                var temperatureLinear = TemperatureLinear.FirstOrDefault(x => x.TimeTemp == t);
                var correctionCT = CorrectionCT.FirstOrDefault(x => x.Time == t);
                
               // var refPoint = RefPoint.FirstOrDefault(x => x.TimeRefPoint == t);
                var temp = templ.FirstOrDefault(x => x.Time == t);
                var subCtrl = subControl.FirstOrDefault(x => x.Time == t);
             //   while (b != null || l != null || m != null || og != null || oga != null || s != null || gf != null)
              //  {
                var sub = sublances.FirstOrDefault(x=>x.Time==t);
                    heatinTime.Add(new HeatInTime
                                       {
                                           Time = t,
                                           Balance = b,
                                           Lance = l,
                                           Modes = m,
                                           OffGas = og,
                                           OffGasAnalysis = oga,
                                           Statistic = s,
                                           GasFlow = gf,
                                           AddMaterial = ad,
                                           Curkin = cu,
                                           UniversalCPl = universalCPl,
                                           TempLiner = temperatureLinear,
                                           CorrectionCT = correctionCT,
                                           RefPoint= RefPoint,
                                           Template = temp,
                                           Zond = sub,
                                           SublanceControlInTime = subCtrl
                                       });

               /*  //   balance.Remove(b);
                 //   lanse.Remove(l);
                 //   modes.Remove(m);
                 //   offGas.Remove(og);
                //    offGasAnalysis.Remove(oga);
                //    statistic.Remove(s);
                //    gasFlow.Remove(gf);
                //   if (ad!=null)
                //    additions.Remove(ad);
                //    if (cu!= null)
                  //      curk.Remove(cu);
                //    if (temp != null)
                //        templ.Remove(temp);
                    b = balance.FirstOrDefault(x => x.O2 == t);
                    l = lanse.FirstOrDefault(x => x.O2 == t);
                    m = modes.FirstOrDefault(x => x.O2 == t);
                    og = offGas.FirstOrDefault(x => x.O2 == t);
                    oga = offGasAnalysis.FirstOrDefault(x => x.O2 == t);
                    s = statistic.FirstOrDefault(x => x.O2 == t);
                    gf = gasFlow.FirstOrDefault(x => x.O2 == t);
                    ad = additions.FirstOrDefault(x => x.O2 == t);
                
                 cu = curk.FirstOrDefault(x => x.O2 == t);
                    temp = templ.FirstOrDefault(x => x.O2 == t);
                }*/
                t = t.AddSeconds(1);
            }
            return heatinTime;
        }

        private List<LanceEvent> GetLanceOnHeat(int iCnvNr, DateTime start, DateTime end)
        {
            var res = new List<LanceEvent>();
          
                const string sql =
                    "SELECT O2TOTALVOL, O2FLOW, O2PRESSURE, LANCEHEIGHT, LANCEMODE, O2FLOWMODE, BATHLEVEL, " +
                    "LEFTWATERINP, LEFTWATEROUT, LEFTWATERTEMPINP, LEFTWATERTEMPOUTP, LEFTLECK, LEFTWATERPRESS, LEFTWEIGHT, LEFTGOAT, " +
                    "RIGHTWATERINP, RIGHTWATEROUT, RIGHTWATERTEMPINP, RIGHTWATERTEMPOUTP, RIGHTLECK, RIGHTWATERPRESS, RIGHTWEIGHT, RIGHTGOAT, INSERTTIME " +
                    "FROM TREND_LANCE WHERE CNV_NO = :CNV_NO AND INSERTTIME BETWEEN :DSTART AND :DEND ";
                var param = MandatoryParams(iCnvNr, start, end);
                var reader = Execute(sql, param);
           try
           {
                while (reader.Read())
                {
                    res.Add(new LanceEvent
                        {
                            O2TotalVol = CheckNubmerForNullAsInt(reader[0].ToString()),
                            O2Flow = CheckNubmerForNullAsDouble(reader[1].ToString()),
                            O2Pressure = CheckNubmerForNullAsDouble(reader[2].ToString()),
                            LanceHeight = CheckNubmerForNullAsInt(reader[3].ToString()),
                            LanceMode = CheckNubmerForNullAsInt(reader[4].ToString()),
                            O2FlowMode = CheckNubmerForNullAsInt(reader[5].ToString()),
                            BathLevel = CheckNubmerForNullAsInt(reader[6].ToString()),
                            O2LeftLanceWaterInput = CheckNubmerForNullAsDouble(reader[7].ToString()),
                            O2LeftLanceWaterOutput = CheckNubmerForNullAsDouble(reader[8].ToString()),
                            O2LeftLanceWaterTempInput = CheckNubmerForNullAsDouble(reader[9].ToString()),
                            O2LeftLanceWaterTempOutput = CheckNubmerForNullAsDouble(reader[10].ToString()),
                            O2LeftLanceLeck = CheckNubmerForNullAsInt(reader[11].ToString()),
                            O2LeftLanceWaterPressure = CheckNubmerForNullAsDouble(reader[12].ToString()),
                            O2LeftLanceGewWeight = CheckNubmerForNullAsDouble(reader[13].ToString()),
                            O2LeftLanceGewBaer = CheckNubmerForNullAsDouble(reader[14].ToString()),
                            O2RightLanceWaterInput = CheckNubmerForNullAsDouble(reader[15].ToString()),
                            O2RightLanceWaterOutput = CheckNubmerForNullAsDouble(reader[16].ToString()),
                            O2RightLanceWaterTempInput = CheckNubmerForNullAsDouble(reader[17].ToString()),
                            O2RightLanceWaterTempOutput = CheckNubmerForNullAsDouble(reader[18].ToString()),
                            O2RightLanceLeck = CheckNubmerForNullAsInt(reader[19].ToString()),
                            O2RightLanceWaterPressure = CheckNubmerForNullAsDouble(reader[20].ToString()),
                            O2RightLanceGewWeight = CheckNubmerForNullAsDouble(reader[21].ToString()),
                            O2RightLanceGewBaer = CheckNubmerForNullAsDouble(reader[22].ToString()),
                            Time = CheckDateForNull(reader[23].ToString())
                        }
                        );
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return res;
        }

        private List<OffGasTrend> GetOffGasOnHeat(int iCnvNr, DateTime start, DateTime end)
        {
            var res = new List<OffGasTrend>();
           
                const string sql =
                    "SELECT FLOW, TEMP, HOODPOS, FILTER, CNT, TEMPONEXIT, TEMPPRECOLLING, TEMPSTEP1, TEMPSTEP2, DECOMPRESS, INSERTTIME " +
                    "FROM TREND_OFFGAS WHERE CNV_NO = :CNV_NO AND INSERTTIME BETWEEN :DSTART AND :DEND ";
                var param = MandatoryParams(iCnvNr, start, end);
                var reader = Execute(sql, param);
          try
          {
                while (reader.Read())
                {
                    res.Add(new OffGasTrend
                        {
                            OffGas = new OffGasEvent
                                {
                                    OffGasFlow = CheckNubmerForNullAsDouble(reader[0].ToString()),
                                    OffGasTemp = CheckNubmerForNullAsInt(reader[1].ToString()),
                                    OffGasHoodPos = CheckNubmerForNullAsInt(reader[2].ToString()),
                                    OffGasFilterControlPos = CheckNubmerForNullAsInt(reader[3].ToString()),
                                    OffGasCounter = CheckNubmerForNullAsInt(reader[4].ToString())
                                },
                            BoilerWaterCooling = new BoilerWaterCoolingEvent
                                {
                                    GasTemperatureOnExit = CheckNubmerForNullAsFloat(reader[5].ToString()),
                                    PrecollingGasTemperature = CheckNubmerForNullAsFloat(reader[6].ToString()),
                                    GasTemperatureAfter1Step = CheckNubmerForNullAsFloat(reader[7].ToString()),
                                    GasTemperatureAfter2Step = CheckNubmerForNullAsFloat(reader[8].ToString())
                                },
                            DecompressionOffGas = new DecompressionOffGasEvent
                                {
                                    Decompression = CheckNubmerForNullAsInt(reader[9].ToString()),
                                },
                            Time = CheckDateForNull(reader[10].ToString())
                        });
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return res;
        }

        private List<OffGasAnalysis> GetOffGasAnalysisOnHeat(int iCnvNr, DateTime start, DateTime end)
        {
            var res = new List<OffGasAnalysis>();
           
                const string sql =
                    "SELECT CO, CO2, O2, N2, H2, AR,  INSERTTIME, TRANSPORTDELAY1,TRANSPORTDELAY2,DIAGBYTE,BRANCH " +
                    "FROM TREND_OFFGASANALYSE WHERE CNV_NO = :CNV_NO AND INSERTTIME BETWEEN :DSTART AND :DEND ";
                var param = MandatoryParams(iCnvNr, start, end);
                var reader = Execute(sql, param);
           try
           {
                while (reader.Read())
                {
                    res.Add(new OffGasAnalysis
                        {
                            CO = CheckNubmerForNullAsDouble(reader[0].ToString()),
                            CO2 = CheckNubmerForNullAsDouble(reader[1].ToString()),
                            O2 = CheckNubmerForNullAsDouble(reader[2].ToString()),
                            N2 = CheckNubmerForNullAsDouble(reader[3].ToString()),
                            H2 = CheckNubmerForNullAsDouble(reader[4].ToString()),
                            Ar = CheckNubmerForNullAsDouble(reader[5].ToString()),
                            Time = CheckDateForNull(reader[6].ToString()),
                            Delay1 = CheckNubmerForNullAsDouble(reader[7].ToString()),
                            Delay2 = CheckNubmerForNullAsDouble(reader[8].ToString()),
                            Error = CheckNubmerForNullAsDouble(reader[9].ToString()),
                            Brantch = CheckNubmerForNullAsDouble(reader[10].ToString()),

                        });
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return res;
        }

        

        private List<ModesTrend> GetModesOnHeat(int iCnvNr, DateTime start, DateTime end)
        {
            var res = new List<ModesTrend>();
           
                const string sql = "SELECT IGNITION, SLAGOUT, LANCE, O2FLOW, TRACT, ANGLE, INSERTTIME " +
                                   "FROM TREND_MODES WHERE CNV_NO = :CNV_NO AND INSERTTIME BETWEEN :DSTART AND :DEND ";
                var param = MandatoryParams(iCnvNr, start, end);
                var reader = Execute(sql, param);
           try
           {
                while (reader.Read())
                {
                    res.Add(new ModesTrend
                        {
                            Ignition = CheckNubmerForNullAsInt(reader[0].ToString()),
                            SlagOut = CheckNubmerForNullAsInt(reader[1].ToString()),
                            Lanse = CheckNubmerForNullAsInt(reader[2].ToString()),
                            O2Flow = CheckNubmerForNullAsInt(reader[3].ToString()),
                            Tract = CheckNubmerForNullAsInt(reader[4].ToString()),
                            Angle = CheckNubmerForNullAsInt(reader[5].ToString()),
                            Time = CheckDateForNull(reader[6].ToString())
                        });
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return res;
        }

        private List<BalanceTrend> GetBalancOnHeat(int iCnvNr, DateTime start, DateTime end)
        {
            var res = new List<BalanceTrend>();
            
                /*  const string sql = "SELECT CASE WHEN to_char(C,'9') = '##' THEN 0 ELSE ROUND(C, 16) END AS C, " +
                                     "CASE WHEN to_char(T,'9') = '##' THEN 0 ELSE ROUND(T, 16) END AS T, " +
                                     "CASE WHEN to_char(SI,'9') = '##' THEN 0 ELSE ROUND(SI, 16) END AS SI, " +
                                     "CASE WHEN to_char(MN,'9') = '##' THEN 0 ELSE ROUND(MN, 16) END AS MN, " +
                                     "CASE WHEN to_char(AL,'9') = '##' THEN 0 ELSE ROUND(AL, 16) END AS AL, " +
                                     "CASE WHEN to_char(CR,'9') = '##' THEN 0 ELSE ROUND(CR, 16) END AS CR, " +
                                     "CASE WHEN to_char(P,'9') = '##' THEN 0 ELSE ROUND(P, 16) END AS P, " +
                                     "CASE WHEN to_char(TI,'9') = '##' THEN 0 ELSE ROUND(TI, 16) END AS TI, " +
                                     "CASE WHEN to_char(V,'9') = '##' THEN 0 ELSE ROUND(V, 16) END AS V, " +
                                     "CASE WHEN to_char(FE,'9') = '##' THEN 0 ELSE ROUND(FE, 16) END AS FE, " +
                                     "CASE WHEN to_char(CAO,'9') = '##' THEN 0 ELSE ROUND(CAO, 16) END AS CAO, " +
                                     "CASE WHEN to_char(FEO,'9') = '##' THEN 0 ELSE ROUND(FEO, 16) END AS FEO, " +
                                     "CASE WHEN to_char(SIO2,'9') = '##' THEN 0 ELSE ROUND(SIO2, 16) END AS SIO2, " +
                                     "CASE WHEN to_char(MNO,'9') = '##' THEN 0 ELSE ROUND(MNO, 16) END AS MNO, " +
                                     "CASE WHEN to_char(MGO,'9') = '##' THEN 0 ELSE ROUND(MGO, 16) END AS MGO, INSERTTIME " +
                                     "FROM TREND_BALANCE WHERE CNV_NO = :CNV_NO AND INSERTTIME BETWEEN :DSTART AND :DEND ";*/
                const string sql = "SELECT  C, " +
                                   " T, " +
                                   "ROUND(SI,25), " +
                                   "ROUND(MN,25), " +
                                   "ROUND(AL,25), " +
                                   "ROUND(CR,25), " +
                                   "ROUND(P, 25), " +
                                   "ROUND(TI, 25), " +
                                   "ROUND(V,25), " +
                                   "ROUND(FE, 25), " +
                                   "ROUND(CAO, 25), " +
                                   "ROUND(FEO, 25), " +
                                   "ROUND( SIO2,25),  " +
                                   "ROUND(MNO, 25), " +
                                   " ROUND(MGO,25), INSERTTIME " +
                                   "FROM TREND_BALANCE WHERE CNV_NO = :CNV_NO AND INSERTTIME BETWEEN :DSTART AND :DEND ";

                var param = MandatoryParams(iCnvNr, start, end);
                var reader = Execute(sql, param);
          try
          {
                while (reader.Read())
                {
                    try
                    {


                        var balance = new BalanceTrend();
                        try
                        {
                            balance.C = CheckNubmerForNullAsDouble(reader[0].ToString());
                        }
                        catch (Exception e)
                        {

                            InstantLogger.err("C " + e.ToString());
                        }
                        try
                        {
                            balance.T = CheckNubmerForNullAsDouble(reader[1].ToString());
                        }
                        catch (Exception e)
                        {

                            InstantLogger.err("t " + e.ToString());
                        }
                        try
                        {
                            balance.Si = CheckNubmerForNullAsDouble(reader[2].ToString());
                        }
                        catch (Exception e)
                        {

                            InstantLogger.err("si " + e.ToString());
                        }
                        try
                        {
                            balance.Mn = CheckNubmerForNullAsDouble(reader[3].ToString());
                        }
                        catch (Exception e)
                        {

                            InstantLogger.err("mn " + e.ToString());
                        }
                        try
                        {
                            balance.Al = CheckNubmerForNullAsDouble(reader[4].ToString());
                        }
                        catch (Exception e)
                        {

                            InstantLogger.err("al " + e.ToString());
                        }
                        try
                        {
                            balance.Cr = CheckNubmerForNullAsDouble(reader[5].ToString());
                        }
                        catch (Exception e)
                        {

                            InstantLogger.err("Cr " + e.ToString());
                        }
                        try
                        {
                            balance.P = CheckNubmerForNullAsDouble(reader[6].ToString());
                        }
                        catch (Exception e)
                        {

                            InstantLogger.err("P " + e.ToString());
                        }
                        try
                        {
                            balance.Ti = CheckNubmerForNullAsDouble(reader[7].ToString());
                        }
                        catch (Exception e)
                        {

                            InstantLogger.err("Ti " + e.ToString());
                        }
                        try
                        {
                            balance.V = CheckNubmerForNullAsDouble(reader[8].ToString());
                        }
                        catch (Exception e)
                        {

                            InstantLogger.err("V " + e.ToString());
                        }
                        try
                        {
                            balance.Fe = CheckNubmerForNullAsDouble(reader[9].ToString());
                        }
                        catch (Exception e)
                        {

                            InstantLogger.err("Fe " + e.ToString());
                        }
                        try
                        {
                            balance.CaO = CheckNubmerForNullAsDouble(reader[10].ToString());
                        }
                        catch (Exception e)
                        {

                            InstantLogger.err("Cao " + e.ToString());
                        }
                        try
                        {
                            balance.FeO = CheckNubmerForNullAsDouble(reader[11].ToString());
                        }
                        catch (Exception e)
                        {

                            InstantLogger.err("FeO " + e.ToString());
                        }

                        try
                        {
                            balance.SiO2 = CheckNubmerForNullAsDouble(reader[12].ToString());
                        }
                        catch (Exception e)
                        {

                            InstantLogger.err("SiO2 " + e.ToString());
                        }


                        try
                        {
                            balance.MnO = CheckNubmerForNullAsDouble(reader[13].ToString());
                        }
                        catch (Exception e)
                        {

                            InstantLogger.err("MnO " + e.ToString());
                        }

                        try
                        {
                            balance.MgO = CheckNubmerForNullAsDouble(reader[14].ToString());
                        }
                        catch (Exception e)
                        {

                            InstantLogger.err("MgO " + e.ToString());
                        }
                        balance.Time = CheckDateForNull(reader[15].ToString());
                        res.Add(balance);
                    }
                    catch (Exception e)
                    {


                    }
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return res;
        }

        private List<StatisticTrend> GetStatisticOnHeat(int iCnvNr, DateTime start, DateTime end)
        {
            var res = new List<StatisticTrend>();
           
                const string sql = "SELECT C, T, TEMPINBOILER, DTEMPINBOILER, DTEMPINBLOW, INSERTTIME " +
                                   "FROM TREND_STATISTIC WHERE CNV_NO = :CNV_NO AND INSERTTIME BETWEEN :DSTART AND :DEND ";
                var param = MandatoryParams(iCnvNr, start, end);
                var reader = Execute(sql, param);
          try
          {
                while (reader.Read())
                {
                    res.Add(new StatisticTrend
                        {
                            C = CheckNubmerForNullAsDouble(reader[0].ToString()),
                            T = CheckNubmerForNullAsDouble(reader[1].ToString()),
                            TempInBoiler = CheckNubmerForNullAsDouble(reader[2].ToString()),
                            DeltaTempinBoiler = CheckNubmerForNullAsDouble(reader[3].ToString()),
                            DeltaTempInBlow = CheckNubmerForNullAsDouble(reader[4].ToString()),
                            Time = CheckDateForNull(reader[5].ToString())
                        });
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return res;
        }

        private List<GasFlow> GetGasFlow(int iCnvNr, DateTime start, DateTime end)
        {
            var res = new List<GasFlow>();
            
                const string sql = "SELECT INSERTTIME, O2FLOW, O2PRESSURE, O2DROP, O2TEMP, LANCE, " +
                                   "N2FLOWWND, N2PRESSUREWND, N2DROPWND, N2TEMPWND," +
                                   "N2FLOWBLR, N2PRESSUREBLR, N2DROPBLR, N2TEMPBLR, " +
                                   "N2FLOWLEAK, N2PRESSURELEAK, N2DROPLEAK, N2TEMPLEAK FROM TREND_GASFLOW " +
                                   "WHERE CNV_NO = :CNV_NO AND INSERTTIME BETWEEN :DSTART AND :DEND ";
                var param = MandatoryParams(iCnvNr, start, end);
                var reader = Execute(sql, param);
          try
          {
                while (reader.Read())
                {
                    res.Add(new GasFlow
                        {
                            Time = CheckDateForNull(reader[0].ToString()),
                            O2Flow = CheckNubmerForNullAsDouble(reader[1].ToString()),
                            O2Pressure = CheckNubmerForNullAsDouble(reader[2].ToString()),
                            O2Drop = CheckNubmerForNullAsDouble(reader[3].ToString()),
                            O2Temp = CheckNubmerForNullAsDouble(reader[4].ToString()),
                            Lance = reader[5].ToString(),
                            N2FlowWnd = CheckNubmerForNullAsDouble(reader[6].ToString()),
                            N2PressureWnd = CheckNubmerForNullAsDouble(reader[7].ToString()),
                            N2DropWnd = CheckNubmerForNullAsDouble(reader[8].ToString()),
                            N2TempWnd = CheckNubmerForNullAsDouble(reader[9].ToString()),
                            N2FlowBlr = CheckNubmerForNullAsDouble(reader[10].ToString()),
                            N2PressureBlr = CheckNubmerForNullAsDouble(reader[11].ToString()),
                            N2DropBlr = CheckNubmerForNullAsDouble(reader[12].ToString()),
                            N2TempBlr = CheckNubmerForNullAsDouble(reader[13].ToString()),
                            N2FlowLeak = CheckNubmerForNullAsDouble(reader[14].ToString()),
                            N2PressureLeak = CheckNubmerForNullAsDouble(reader[15].ToString()),
                            N2DropLeak = CheckNubmerForNullAsDouble(reader[16].ToString()),
                            N2TempLeak = CheckNubmerForNullAsDouble(reader[17].ToString()),
                        });
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return res;
        }

        private int GetSlagOut (int iCnvNr, DateTime start, DateTime end)
        {
            var prevH = 0;
            var count = 0;
            var first = true;
            
                var sql = "SELECT SLAGOUT, LAG (SLAGOUT) OVER (ORDER BY CNV_NO, INSERTTIME) AS PREV " +
                          "FROM TREND_MODES " +
                          "WHERE  SLAGOUT IS NOT NULL AND CNV_NO = :CNV_NO AND INSERTTIME BETWEEN :DSTART AND :DEND " +
                          "ORDER BY  INSERTTIME ";
                var param = MandatoryParams(iCnvNr, start, end);
                var reader = Execute(sql, param);
           try
           {
                sql = "SELECT SLAGOUT, MAX(INSERTTIME) " +
                      "FROM TREND_MODES " +
                      "WHERE  SLAGOUT IS NOT NULL AND CNV_NO = :CNV_NO AND INSERTTIME < :DSTART " +
                      "GROUP BY SLAGOUT ";
                param.RemoveAt(2);
                var reader2 = Execute(sql, param);
                if (reader2.Read())
                {
                    prevH = CheckNubmerForNullAsInt(reader2[0].ToString());
                }
                reader2.Close();
                while (reader.Read())
                {
                    if (!first)
                    {
                        prevH = CheckNubmerForNullAsInt(reader[1].ToString());
                    }
                    if (CheckNubmerForNullAsInt(reader[0].ToString()) != prevH)
                    {
                        count++;
                    }
                    first = false;
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return count;
        }

        private int GetIgnition (int iCnvNr, DateTime start, DateTime end)
        {
            var res = -1;
            
                const string sql = "SELECT MAX(IGNITION) FROM TREND_MODES " +
                                   "WHERE  IGNITION IS NOT NULL AND CNV_NO = :CNV_NO AND INSERTTIME BETWEEN :DSTART AND :DEND ";
                var param = MandatoryParams(iCnvNr, start, end);
                var reader = Execute(sql, param);
           try
           {
                if (reader.Read())
                {
                    res = CheckNubmerForNullAsInt(reader[0].ToString());
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return res;
        }

        private int GetLanceMode(int iCnvNr, DateTime start, DateTime end)
        {
            var res = 1;
            var lance = GetLanceOnHeat(iCnvNr, start, end).Where(x => x.O2Flow > 0);
            if (lance.Any(lanceEvent =>  (lanceEvent.LanceMode != 3)))
            {
                res = 0;
            }
            return res;
        }

        private DateTime GetTempLinerZondT(int iCnvNr, DateTime start, DateTime end, int heatId)
        {
            DateTime res = DateTime.MinValue;
            DateTime startTime =
                GetLanceOnHeat(iCnvNr, start, end).Where(x => x.O2Flow > 0).Select(x => x.Time).FirstOrDefault();
            startTime = GetLanceOnHeat(iCnvNr, startTime, end).Where(x => x.O2Flow == 0).Select(x => x.Time.AddSeconds(-1)).FirstOrDefault();
            res = startTime;
            return res;
        }


        private DateTime ZondStTime(int heatId)
        {
            DateTime time;
            time = DateTime.MinValue;
            var sql =
                "SELECT EVENTTIME FROM  HEAT_EVENTS " +
                " WHERE  HEAT_ID = :HEAT_ID AND TYPE = 'SUBS' ORDER BY EVENTTIME DESC";
            var param = new List<OracleParameter>
                    {
                        SetParams("HEAT_ID", heatId),
                    };
            var reader = Execute(sql, param);
            try
            {
                while (reader.Read())
                {
                    time = CheckDateForNull(reader[0].ToString());
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return time;
        }


        private int GetLanceModeT(int iCnvNr, DateTime start, DateTime end, int heatId)
        {
            var res = -1;
            try
            {
                var time = ZondStTime(heatId);
                DateTime startTime =
                    GetLanceOnHeat(iCnvNr, start, time).Where(x => x.O2Flow > 0).Select(x => x.Time).FirstOrDefault();
                var lance = GetLanceOnHeat(iCnvNr, startTime, time).Where(x => x.O2Flow > 0);
                int avto = 3;
                double O2 = -1;
                try
                {
                    avto = lance.Where(lanceEvent => lanceEvent.LanceMode != 3).Select(x => x.LanceMode).First();
                }
                catch { }
                try
                {
                    O2 = lance.Where(x => x.O2Flow == 0).Select(x => x.O2Flow).First();
                }
                catch { }
                res = 1;
                if (avto != 3 || O2 != -1)
                {
                    res = 0;
                }
            }
            catch (Exception exception)
            {
                InstantLogger.err(exception.ToString());
            }
            return res;
        }


        private DateTime ZamerEndTime(int heatNumber)
        {
          //  SELECT EVENTTIME, PAR1 " +
                  //        "FROM BOF_TELEGRAMS WHERE OPER_ID=1160 AND HEAT_NO=:HEAT_NO ORDER BY EVENTTIME
            DateTime time;
            int count = 0;
            time = DateTime.MinValue;
            var sql = "SELECT EVENTTIME " +
                      "FROM BOF_TELEGRAMS WHERE OPER_ID=1160  AND PAR1 > 0 AND HEAT_NO=:HEAT_NO ORDER BY EVENTTIME ";
            var param = new List<OracleParameter>
                    {
                        SetParams("HEAT_NO", heatNumber),
                    };
            var reader = Execute(sql, param);
            try
            {
                while (reader.Read())
                {
                    
                    time = CheckDateForNull(reader[0].ToString());
                    count++;
                    if(count ==2)
                        break;
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }

            return time;
        }

        private int GetLAutoDovodka(int iCnvNr, DateTime start, DateTime end, int heatNumber)
        {
            var res = -1;
            try
            {
                DateTime time = ZamerEndTime(heatNumber);
                if (time != DateTime.MinValue)
                {
                    var lanceHeight = GetLanceOnHeat(iCnvNr, time, end);
                    int LanceHeightUp = 0;
                    try
                    {
                        LanceHeightUp =
                            lanceHeight.Where(x => x.LanceHeight < 360).Select(x => x.LanceHeight).FirstOrDefault();
                        if (LanceHeightUp == 0)
                             res = 1;
                    }
                    catch{}

                    DateTime TimeDown360 =
                        lanceHeight.Where(x => x.LanceHeight < 360).Select(x => x.Time).FirstOrDefault();




                    if (TimeDown360 != DateTime.MinValue)
                    {
                        var lanceHeightDown360 = GetLanceOnHeat(iCnvNr, TimeDown360, end);
                        DateTime timeUp360 =
                            lanceHeightDown360.Where(x => x.LanceHeight >= 360)
                                              .Select(x => x.Time.AddSeconds(-1))
                                              .FirstOrDefault();
                        var lanceCell = GetLanceOnHeat(iCnvNr, time, timeUp360);
                        int Avto = 3;
                        double O2 = -1;
                        try
                        {
                            Avto =lanceCell.Where(lanceEvent => lanceEvent.LanceMode != 3)
                                         .Select(x => x.LanceMode)
                                         .First();
                            if (Avto != 3)
                                 res = 0;
                        }
                            
                        catch {}
                        try
                        {
                            O2 = lanceCell.Where(x => x.O2Flow == 0).Select(x => x.O2Flow).First();
                            if (O2 != -1)
                                res = 0;
                        }
                        catch{}
                        
                        if (timeUp360 != DateTime.MinValue && Avto == 3 && O2 == -1)
                        {

                            var sql = "SELECT  PAR1 " +
                                      "FROM MODEL_TELEGRAMS WHERE OPERATION_ID=34 AND HEAT_NO=:HEAT_NO AND EVENTTIME BETWEEN :TIME1 AND :TIME2 AND PAR1 = 'True'  ORDER BY EVENTTIME";
                            var param = new List<OracleParameter>
                                    {
                                        SetParams("HEAT_NO", heatNumber)
                                    };
                            param.Add(SetParams("TIME1", time));
                            param.Add(SetParams("TIME2", timeUp360));
                            var reader = Execute(sql, param);
                            try
                            {
                                res = 1;
                                if (reader.Read())
                                {
                                    res = 0;
                                }
                                reader.Close();

                            }
                            catch (Exception exception)
                            {
                                reader.Close();
                                InstantLogger.err(exception.ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                InstantLogger.err(exception.ToString());
            }
            return res;
        }

        private int GetLastTracktMode(int iCnvNo, DateTime start) //1 ручной 3 увм
        {
           
                const string sql = "SELECT  TRACT, INSERTTIME " +
                                   "FROM TREND_MODES WHERE TRACT<>0 AND CNV_NO = :CNV_NO AND INSERTTIME <= :DSTART ORDER BY INSERTTIME DESC";
                var param = new List<OracleParameter>
                    {
                        SetParams("CNV_NO", iCnvNo),
                        SetParams("DSTART", start)
                    };
                var reader = Execute(sql, param);
           try
           {
                while (reader.Read())
                {
                    return CheckNubmerForNullAsInt(reader[0].ToString());
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return -1; 
        }

        private List<ModesTrend> GetTracktMode1(int iCnvNo, DateTime start, DateTime end) //1 ручной 3 увм
        {
            var res = new List<ModesTrend>();
            
                const string sql = "SELECT  TRACT, INSERTTIME " +
                                   "FROM TREND_MODES WHERE TRACT<>0 AND CNV_NO = :CNV_NO AND INSERTTIME BETWEEN :DSTART AND :DEND ORDER BY INSERTTIME DESC";
                var param = MandatoryParams(iCnvNo, start, end);
                var reader = Execute(sql, param);
            try
            {
                while (reader.Read())
                {
                    res.Add(new ModesTrend
                        {
                            Tract = CheckNubmerForNullAsInt(reader[0].ToString()),
                            Time = CheckDateForNull(reader[1].ToString())
                        });
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return res;
        }

        private int GetTractMode(int iCnvNr, DateTime start, DateTime end)
        {
         
            var lance = GetLanceOnHeat(iCnvNr, start, end);
           
           
         
            var time1 = lance.Where(x => x.O2Flow > 0).OrderBy(x => x.Time).Select(x=>x.Time).FirstOrDefault();
            var res = GetLastTracktMode(iCnvNr, time1) == 3;
         //   var time2 = lance.Where(x => x.O2Flow > 0).OrderByDescending(x => x.O2).Select(x => x.O2).FirstOrDefault();
            var time2 = lance.Where(x => x.O2TotalVol < 17000&&x.O2TotalVol>0).OrderByDescending(x => x.Time).Select(x => x.Time).FirstOrDefault();
         
            if (time1 != DateTime.MinValue&&time2!=DateTime.MinValue)
            {
                var x1 = GetTracktMode1(iCnvNr, time1, time2);
                if (x1.Count>0)
                    res = x1.All(x => x.Tract == 3);

            }
           
            return res?1:0;
        }

        private HotMetalXim GetHotMetalXim (int heatNumber, int proba)
        {
            var res = new HotMetalXim();
            
                var sql = "SELECT LISTAGG(XIM_MIX, ', ') WITHIN GROUP (ORDER BY XIM_MIX) " +
                          "FROM XIM_ANAL " +
                          "WHERE  XIM_PFANNE IS NULL  AND XIM_VID = 3 AND XIM_NUM = :NUM AND XIM_PLAVKA = :HEAT_NO";
                var param = new List<OracleParameter>
                    {
                        SetParams("NUM", proba),
                        SetParams("HEAT_NO", heatNumber.ToString())
                    };
                var reader = Execute(sql, param);
          try
          {
                if (reader.Read())
                {
                    res.Torpedos = reader[0].ToString();
                }
                reader.Close();
                sql = "SELECT XIM_DT, XIM_PFANNE, XIM_C,  XIM_SI, XIM_MN, XIM_P, XIM_S " +
                      "FROM XIM_ANAL " +
                      "WHERE  XIM_PFANNE IS NOT NULL  AND XIM_VID = 3 AND XIM_NUM = :NUM AND XIM_PLAVKA = :HEAT_NO ";
                reader = Execute(sql, param);
                if (reader.Read())
                {
                    res.Time = CheckDateForNull(reader[0].ToString());
                    res.Ladles = CheckNubmerForNullAsInt(reader[1].ToString());
                    res.C = CheckNubmerForNullAsDouble(reader[2].ToString());
                    res.Si = CheckNubmerForNullAsDouble(reader[3].ToString());
                    res.Mn = CheckNubmerForNullAsDouble(reader[4].ToString());
                    res.P = CheckNubmerForNullAsDouble(reader[5].ToString());
                    res.S = CheckNubmerForNullAsDouble(reader[6].ToString());
                }
                /*  else
                  {
                      res = null;
                  }*/
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return res;
        }
        
        private SteelXim GetSteelXim(int heatNumber, int proba)
        {
            var res = new SteelXim();
           
                const string sql =
                    "SELECT XIM_DT, XIM_C, XIM_SI, XIM_MN, XIM_P, XIM_S, XIM_CR, XIM_NI, XIM_CU, XIM_AL, XIM_N, XIM_MO " +
                    "FROM  XIM_ANAL " +
                    "WHERE XIM_VID = 1 AND XIM_PLACE BETWEEN 21 AND 23 AND XIM_NUM = :NUM AND XIM_PLAVKA = :HEAT_NO ";
                var param = new List<OracleParameter>
                    {
                        SetParams("NUM", proba),
                        SetParams("HEAT_NO", heatNumber.ToString())
                    };
                var reader = Execute(sql, param);
          try
          {
                if (reader.Read())
                {
                    res.Time = CheckDateForNull(reader[0].ToString());
                    try
                    {
                        res.C = CheckNubmerForNullAsDouble(reader[1].ToString());

                    }
                    catch (Exception)
                    {


                    }
                    try
                    {
                        res.Si = CheckNubmerForNullAsDouble(reader[2].ToString());

                    }
                    catch (Exception)
                    {


                    }
                    try
                    {
                        res.Mn = CheckNubmerForNullAsDouble(reader[3].ToString());

                    }
                    catch (Exception)
                    {


                    }
                    try
                    {
                        res.P = CheckNubmerForNullAsDouble(reader[4].ToString());

                    }
                    catch (Exception)
                    {


                    }
                    try
                    {
                        res.S = CheckNubmerForNullAsDouble(reader[5].ToString());

                    }
                    catch (Exception)
                    {


                    }
                    try
                    {
                        res.Cr = CheckNubmerForNullAsDouble(reader[6].ToString());

                    }
                    catch (Exception)
                    {


                    }
                    try
                    {
                        res.Ni = CheckNubmerForNullAsDouble(reader[7].ToString());

                    }
                    catch (Exception)
                    {


                    }
                    try
                    {
                        res.Cu = CheckNubmerForNullAsDouble(reader[8].ToString());

                    }
                    catch (Exception)
                    {


                    }
                    try
                    {
                        res.Al = CheckNubmerForNullAsDouble(reader[9].ToString());

                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        res.N = CheckNubmerForNullAsDouble(reader[10].ToString());

                    }
                    catch (Exception)
                    {


                    }
                    try
                    {
                        res.Mo = CheckNubmerForNullAsDouble(reader[11].ToString());
                    }
                    catch (Exception)
                    {


                    }

                }
                else
                {
                    res = null;
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return res;
        }

        private SlagXim GetSlagXim(int heatNumber, int proba)
        {
            var res = new SlagXim();
            
                const string sql =
                    "SELECT XIM_DT, XIM_CAO, XIM_SIO2, XIM_FEO, XIM_MGO, XIM_MNO, XIM_AL2O3, XIM_S, XIM_P2O5 " +
                    "FROM  XIM_ANAL " +
                    "WHERE XIM_VID = 2 AND XIM_PLACE BETWEEN 21 AND 23 AND XIM_NUM = :NUM AND XIM_PLAVKA = :HEAT_NO ";
                var param = new List<OracleParameter>
                    {
                        SetParams("NUM", proba),
                        SetParams("HEAT_NO", heatNumber.ToString())
                    };
                var reader = Execute(sql, param);
         try
         {
                if (reader.Read())
                {
                    res.Time = CheckDateForNull(reader[0].ToString());
                    try
                    {
                        res.CaO = CheckNubmerForNullAsDouble(reader[1].ToString());

                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        res.SiO2 = CheckNubmerForNullAsDouble(reader[2].ToString());
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        res.FeO = CheckNubmerForNullAsDouble(reader[3].ToString());

                    }
                    catch (Exception)
                    {


                    }
                    try
                    {
                        res.MgO = CheckNubmerForNullAsDouble(reader[4].ToString());

                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        res.MnO = CheckNubmerForNullAsDouble(reader[5].ToString());

                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        res.Al2O3 = CheckNubmerForNullAsDouble(reader[6].ToString());

                    }
                    catch (Exception)
                    {


                    }
                    try
                    {
                        res.S = CheckNubmerForNullAsDouble(reader[7].ToString());

                    }
                    catch (Exception)
                    {


                    }
                    try
                    {
                        res.P2O5 = CheckNubmerForNullAsDouble(reader[8].ToString());
                    }
                    catch (Exception)
                    {


                    }

                }
                else
                {
                    res = null;
                }
                reader.Close();
                if (res != null && res.SiO2 != 0)
                    res.CaOdivSiO2 = res.CaO/res.SiO2;
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return res;
        }

        private HotMetalXim GetHotMetalXim(int heatNumber)
        {
            var res = new HotMetalXim();
           
                const string sql = "SELECT INSERTTIME, C, SI, MN, P, S FROM TREND_XIMCALC " +
                                   "WHERE  XIM_VID = 3 AND HEAT_NO = :HEAT_NO ";
                var param = new List<OracleParameter> {SetParams("HEAT_NO", heatNumber)};
                var reader = Execute(sql, param);
          try
          {
                if (reader.Read())
                {
                    res.Time = CheckDateForNull(reader[0].ToString());
                    res.C = CheckNubmerForNullAsDouble(reader[1].ToString());
                    res.Si = CheckNubmerForNullAsDouble(reader[2].ToString());
                    res.Mn = CheckNubmerForNullAsDouble(reader[3].ToString());
                    res.P = CheckNubmerForNullAsDouble(reader[4].ToString());
                    res.S = CheckNubmerForNullAsDouble(reader[5].ToString());
                }
                else
                {
                    res = null;
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return res;
        }

        private SteelXim GetSteelXim(int heatNumber)
        {
            var res = new SteelXim();
            
                const string sql =
                    "SELECT INSERTTIME, C, ROUND(SI,24) AS SI , ROUND(MN,24) AS MN, P, S,ROUND(CR,24) AS CR , NI, CU, AL, N, MO FROM TREND_XIMCALC " +
                    "WHERE XIM_VID = 1 AND HEAT_NO = :HEAT_NO ";
                var param = new List<OracleParameter> {SetParams("HEAT_NO", heatNumber)};
                var reader = Execute(sql, param);
           try
           {
                if (reader.Read())
                {
                    res.Time = CheckDateForNull(reader[0].ToString());
                    try
                    {
                        res.C = CheckNubmerForNullAsDouble(reader[1].ToString());
                    }
                    catch
                    {
                        res.C = 0;

                    }

                    //      res.Si = CheckNubmerForNullAsDouble(reader[2].ToString());
                    try
                    {
                        res.Si = CheckNubmerForNullAsDouble(reader[2].ToString());
                    }
                    catch
                    {
                        res.Si = 0;

                    }
                    try
                    {
                        res.Mn = CheckNubmerForNullAsDouble(reader[3].ToString());
                    }
                    catch
                    {
                        res.Mn = 0;
                    }
                    try
                    {
                        res.P = CheckNubmerForNullAsDouble(reader[4].ToString());
                    }
                    catch
                    {
                        res.P = 0;
                    }
                    try
                    {
                        res.S = CheckNubmerForNullAsDouble(reader[5].ToString());
                    }
                    catch
                    {
                        res.S = 0;
                    }
                    try
                    {
                        res.Cr = CheckNubmerForNullAsDouble(reader[6].ToString());
                    }
                    catch
                    {
                        res.Cr = 0;
                    }
                    try
                    {
                        res.Ni = CheckNubmerForNullAsDouble(reader[7].ToString());
                    }
                    catch
                    {
                        res.Ni = 0;
                    }
                    try
                    {
                        res.Cu = CheckNubmerForNullAsDouble(reader[8].ToString());
                    }
                    catch
                    {
                        res.Cu = 0;
                    }
                    try
                    {
                        res.Al = CheckNubmerForNullAsDouble(reader[9].ToString());
                    }
                    catch
                    {
                        res.Al = 0;
                    }
                    try
                    {
                        res.N = CheckNubmerForNullAsDouble(reader[10].ToString());
                    }
                    catch
                    {
                        res.N = 0;
                    }
                    try
                    {
                        res.Mo = CheckNubmerForNullAsDouble(reader[11].ToString());
                    }
                    catch
                    {
                        res.Mo = 0;
                    }



                }
                else
                {
                    res = null;
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return res;
        }

        private SlagXim GetSlagXim(int heatNumber)
        {
            var res = new SlagXim();
            
                const string sql = "SELECT INSERTTIME, CAO, SIO2, FEO, MGO, MNO, AL2O3, S, P2O5 FROM TREND_XIMCALC " +
                                   "WHERE XIM_VID = 2 AND HEAT_NO = :HEAT_NO ";
                var param = new List<OracleParameter> {SetParams("HEAT_NO", heatNumber)};
                var reader = Execute(sql, param);
           try
           {
                if (reader.Read())
                {
                    res.Time = CheckDateForNull(reader[0].ToString());
                    try
                    {
                        res.CaO = CheckNubmerForNullAsDouble(reader[1].ToString());
                    }
                    catch (Exception)
                    {


                    }
                    try
                    {
                        res.SiO2 = CheckNubmerForNullAsDouble(reader[2].ToString());
                    }
                    catch (Exception)
                    {


                    }
                    try
                    {
                        res.FeO = CheckNubmerForNullAsDouble(reader[3].ToString());
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        res.MgO = CheckNubmerForNullAsDouble(reader[4].ToString());

                    }
                    catch (Exception)
                    {


                    }
                    try
                    {
                        res.MnO = CheckNubmerForNullAsDouble(reader[5].ToString());

                    }
                    catch (Exception)
                    {


                    }
                    try
                    {
                        res.Al2O3 = CheckNubmerForNullAsDouble(reader[6].ToString());

                    }
                    catch (Exception)
                    {


                    }
                    try
                    {
                        res.S = CheckNubmerForNullAsDouble(reader[7].ToString());


                    }
                    catch (Exception)
                    {


                    }
                    try
                    {
                        res.P2O5 = CheckNubmerForNullAsDouble(reader[8].ToString());
                    }
                    catch (Exception)
                    {


                    }

                }
                else
                {
                    res = null;
                }
                reader.Close();
                if (res != null && res.SiO2 != 0)
                    res.CaOdivSiO2 = res.CaO/res.SiO2;
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return res;
        }

        public General GetFinalHotMetal(General general)
        {
            
                const string sql =
                    "SELECT INSERTTIME, CHGL_NO, FINAL_WEIGHT,  FINAL_TEMP, HM_WEIGHT FROM TREND_HOTMETAL WHERE HEAT_NO = :HEAT_NO";
                var param = new List<OracleParameter> {SetParams("HEAT_NO", general.HeatNumber)};
                var read = Execute(sql, param);
          try
          {
                if (read.Read())
                {

                    var x = CheckNubmerForNullAsDouble(read[2].ToString());
                    var x1 = CheckNubmerForNullAsDouble(read[4].ToString());

                    if (x != -1)
                    {
                        general.HotMetalWeightTime = CheckDateForNull(read[0].ToString());
                        general.HotMetalLadle = CheckNubmerForNullAsInt(read[1].ToString());
                        general.HotMetalLadleWeight = x;
                        if (general.HotMetalLadleWeight < 1000)
                            general.HotMetalLadleWeight = general.HotMetalLadleWeight*1000;
                    }
                    else
                    {
                        if (x1 != 1)
                        {
                            general.HotMetalWeightTime = CheckDateForNull(read[0].ToString());
                            general.HotMetalLadle = CheckNubmerForNullAsInt(read[1].ToString());
                            general.HotMetalLadleWeight = x1;
                        }
                    }
                    general.ZondCulcTemp = CheckNubmerForNullAsDouble(read[3].ToString());
                }
                read.Close();
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
            return general;
        }
    }
}
