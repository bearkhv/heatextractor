﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using HeatExtractor.Classes;
using Implements;
using Microsoft.Office.Interop.Excel;
using System.IO;

namespace HeatExtractor.Excel
{
    public delegate void HeatInTimeEventHandler(int count, int current);
    public class ExcelExport
    {
        private static Application _app;
        private Workbook _wb;
        private bool _isSave;
        private readonly string _fileName;

        public ExcelExport()
        {
            _fileName = Path.Combine(System.Windows.Forms.Application.StartupPath, ConfigurationManager.OpenExeConfiguration("").AppSettings.Settings["Template"].Value);
        }

        public void ExcelOpen()
        {
            _app = new Application();
            _wb = _app.Workbooks.Open(_fileName, 1, true, 5, "", "", true, XlPlatform.xlWindows, "", false, false, 0, true, false, XlCorruptLoad.xlNormalLoad);
            _wb.Worksheets[2].Select();
            _wb.ActiveSheet.Copy(_wb.Worksheets[3]);
            _wb.ActiveSheet.Name = "Запрос 1";
            _wb.ActiveSheet.Copy(_wb.Worksheets[4]);
            _wb.ActiveSheet.Name = "Запрос 2";
            _wb.ActiveSheet.Copy(_wb.Worksheets[5]);
            _wb.ActiveSheet.Name = "Запрос 1, 2";
            
            //   _wb.ActiveSheet.Name = "Запрос 1, 2";
            _isSave = false;
            
        }
        
        public void ExcelClose()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            if (_wb != null)
            {
                _wb.Saved = true;//_isSave;
                _wb.Close();
                Marshal.FinalReleaseComObject(_wb);
                _wb = null;
            }
            _app.Application.Quit();
            Marshal.FinalReleaseComObject(_app);
            _app = null;
        }
        
        public void ExcelSave(string fileName)
        {
           _wb.SaveCopyAs(fileName);
         //   _wb.SaveAs(fileName, XlFileFormat.xlWorkbookNormal, Type.Missing, Type.Missing, false, Type.Missing, XlSaveAsAccessMode.xlNoChange, 2, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            _isSave = true;
          //  ExcelClose();
        }

        public void ExcelSheetContents(int qG, int q1, int q2, int q12)
        {
            _wb.Worksheets[1].Select();
            _app.Cells[2, 3] = qG;
            _app.Cells[3, 3] = q1;
            _app.Cells[4, 3] = q2;
            _app.Cells[5, 3] = q12;
        }

        public int ExcelSheetGenerel(General general, int qG)
        {
            InstantLogger.msg("{0} Excel sheet general.", general.HeatNumber);
            _wb.Worksheets[2].Select();
            SetGeneral(general, qG + 4);
            qG++;
            InstantLogger.msg("{0} Excel sheet general. Done", general.HeatNumber);
            return qG;
        }
        
        public void ExcelSheetsHeatInTime(General general, bool isLast)
        {
            var count = _wb.Worksheets.Count;
            _wb.Worksheets[count].Select();
            if (general.HeatInTime == null) return;
            if (!isLast)
            {
                _wb.ActiveSheet.Copy(_wb.Worksheets[count]);
            }
            _wb.ActiveSheet.Name = string.Format("№ {0} {1}", general.HeatNumber, general.HeatDuration.Start.ToString().Replace(":", "."));
            if (general.HeatDuration.Period.Hours<3)
            SetHeatInTimel(general.HeatInTime);
        }

        public static bool IsQ1(General general)
        {
            if ((general.HotMetalTemp > 1300 && general.HotMetalTemp <= 1470) &&
                 (general.HotMetalLadleWeight > 260000 && general.HotMetalLadleWeight <= 340000) &&
                 (general.ScrapWeight > 60000 && general.ScrapWeight <= 130000) &&
                 (general.HotMetalXim1 != null && ((general.HotMetalXim1.C > 4 && general.HotMetalXim1.C <= 5) &&
                 (general.HotMetalXim1.Si > 0.03 && general.HotMetalXim1.Si <= 1) &&
                //  (general.HotMetalXim1.Mn > 0.2 && general.HotMetalXim1.Mn <= 0.45) &&
                 (general.HotMetalXim1.P > 0.05 && general.HotMetalXim1.P <= 0.078)))) //&&
            //  (general.HotMetalXim1.S > 0.008 && general.HotMetalXim1.S <= 0.038))))
            {
                return true;
            }
            return false;
        }

        public bool IsChex1(General general)
        {
           
           if (general.HeatQuality!=null)
           {
               if (general.HeatQuality.MaxConverterAngle>145&&!general.HeatQuality.IsQualityScrap
                   &&(general.TractMode==1||general.LanceMode==1)&&general.HotMetalXim1!=null&&general.SlagXim1!=null
                   &&general.HotMetalTemp>0&&(int)general.HotMetalLadleWeight%5000!=0)
               {
                   return true;
               }

           }
            return false;
        }
        public bool IsChex2(General general)
        {

            if (general.HeatQuality != null)
            {
                if (general.HeatQuality.MaxConverterAngle > 145 
                    && (general.TractMode == 1 || general.LanceMode == 1) && general.HotMetalXim1 != null && general.SlagXim1 != null
                    && general.HotMetalTemp > 0 && (int)general.HotMetalLadleWeight % 5000 != 0)
                {
                    return true;
                }

            }
            return false;
        }
        public static bool IsQ2(General general)
        {
            double tempZond1 = 0;
            double tempZond2 = 0;
            if (general.Zond1 != null)
                tempZond1 = general.Zond1.ZondTemp;
            if (general.Zond2 != null)
            {
                tempZond2 = general.Zond2.ZondTemp;
            }
            if (
                    (tempZond1 > 1580 && tempZond1 <= 1760 ||
                    (tempZond2 > 1580 && tempZond2 <= 1760)) &&

                 (general.SteelXim1 != null &&
                    ((general.SteelXim1.C > 0.03 || general.IsDynamicGradeSteel) &&
                // (general.Zond1 != null && (Math.Abs(general.SteelXim1.C - general.Zond1.ZondC) < 0.025)) &&
                    (general.SteelXim1.Si != 0 && general.SteelXim1.Si != -1) &&
                    (general.SteelXim1.Mn != 0 && general.SteelXim1.Mn != -1) &&
                    (general.SteelXim1.P != 0 && general.SteelXim1.P != 0) &&
                    (general.SteelXim1.S != 0 && general.SteelXim1.S != 0) &&
                    general.SteelXim1.Cr != -1 &&
                    general.SteelXim1.Cu != -1 &&
                    general.SteelXim1.Al != -1 &&
                    general.SteelXim1.N != -1 &&
                    general.SteelXim1.Mo != -1)) &&
                (general.SlagXim1 != null &&
                    ((general.SlagXim1.CaO != 0 && general.SlagXim1.CaO != -1) &&
                    (general.SlagXim1.SiO2 != 0 && general.SlagXim1.SiO2 != -1) &&
                    (general.SlagXim1.FeO != 0 && general.SlagXim1.FeO != -1) &&
                    (general.SlagXim1.MgO != 0 && general.SlagXim1.MgO != -1) &&
                    (general.SlagXim1.MnO != 0 && general.SlagXim1.MnO != -1) &&
                    (general.SlagXim1.Al2O3 != 0 && general.SlagXim1.Al2O3 != -1) &&
                    (general.SlagXim1.S != 0 && general.SlagXim1.S != -1) &&
                    (general.SlagXim1.P2O5 != 0 && general.SlagXim1.P2O5 != -1) &&
                    (general.SlagXim1.CaOdivSiO2 != 0 && general.SlagXim1.CaOdivSiO2 != -1)))) //&&
            //  (general.Converter != null && (general.Converter.MatCount > 2 && general.Converter.Izvest != -1 && general.Converter.Izvest != 0)))
            {
                return true;
            }
            return false;
        }
        public static bool IsQ3(General general)
        {
           
            if (
                (general.LanceMode==1||general.TractMode==1))//&&
                //(general.SteelMass>0)&&(general.HeatQuality.ColdMlnz>0)&&
               // (general.HeatQuality.ColdMlnz > 0)&&
              //  (general.Zond1!=null)&&
              //      (general.SteelXim1 != null &&
             //       ((general.SteelXim1.C > 0.03 || general.IsDynamicGradeSteel) &&
             //   (general.Zond1 != null && (Math.Abs(general.SteelXim1.C - general.Zond1.ZondC) < 0.025)) &&
              //      (general.SteelXim1.Si != 0 && general.SteelXim1.Si != -1) &&
              //      (general.SteelXim1.Mn != 0 && general.SteelXim1.Mn != -1) &&
              //      (general.SteelXim1.P != 0 && general.SteelXim1.P != 0) &&
              //      (general.SteelXim1.S != 0 && general.SteelXim1.S != 0) &&
              //      general.SteelXim1.Cr != -1 &&
              //      general.SteelXim1.Cu != -1 &&
               //     general.SteelXim1.Al != -1 &&
               //     general.SteelXim1.N != -1 &&
               //     general.SteelXim1.Mo != -1)) &&
               // (general.SlagXim1 != null &&
               //     ((general.SlagXim1.CaO != 0 && general.SlagXim1.CaO != -1) &&
               //     (general.SlagXim1.SiO2 != 0 && general.SlagXim1.SiO2 != -1) &&
               //     (general.SlagXim1.FeO != 0 && general.SlagXim1.FeO != -1) &&
               //     (general.SlagXim1.MgO != 0 && general.SlagXim1.MgO != -1) &&
               //     (general.SlagXim1.MnO != 0 && general.SlagXim1.MnO != -1) &&
               //     (general.SlagXim1.Al2O3 != 0 && general.SlagXim1.Al2O3 != -1) &&
               //     (general.SlagXim1.S != 0 && general.SlagXim1.S != -1) &&
               //     (general.SlagXim1.P2O5 != 0 && general.SlagXim1.P2O5 != -1) &&
              //      (general.SlagXim1.CaOdivSiO2 != 0 && general.SlagXim1.CaOdivSiO2 != -1)))) //&&
            //  (general.Converter != null && (general.Converter.MatCount > 2 && general.Converter.Izvest != -1 && general.Converter.Izvest != 0)))
            {
                return true;
            }
            return false;
        }
        public int ExcelSheetQ1(General general, int q1)
        {
            InstantLogger.msg("{0} Excel sheet Q1.", general.HeatId);
            _wb.Worksheets[3].Select();
            if (IsQ1(general))
            {
                SetGeneral(general, q1 + 4);
                q1++;
            }
            InstantLogger.msg("{0} Excel sheet Q1. Done", general.HeatId);
            return q1;
        }

        public int ExcelSheetQ2(General general, int q2)
        {
            InstantLogger.msg("{0} Excel sheet Q2.", general.HeatId);
             _wb.Worksheets[4].Select();
           /*  if (IsChex1(general))
             {
                 SetGeneral(general, q2 + 4);
             }
            q2++;*/
            if(IsQ2(general)) {
                SetGeneral(general, q2 + 4);
                q2++;
            }
            InstantLogger.msg("{0} Excel sheet Q2. Done", general.HeatId);
            return q2;
        }

       

        public int ExcelSheetQ3(General general, int q12)
        {
            InstantLogger.msg("{0} Excel sheet Q3.", general.HeatId);
            _wb.Worksheets[5].Select();
          /*  if (IsChex2(general))
            {
                SetGeneral(general, q12 + 4);
                q12++;
            }*/
         /*   if (IsQ1(general)&&IsQ2(general))         {
                SetGeneral(general, q12 + 4);
                q12++;
            }*/
            if (IsQ1(general) && IsQ2(general))
            {
                SetGeneral(general, q12 + 4);
                q12++;
            }
            InstantLogger.msg("{0} Excel sheet Q3. Done", general.HeatId);
            return q12;
        }

      

        private static object CheckForNullData(DateTime value)
        {
            return value == DateTime.MinValue ? (object)"" : value.Date.ToShortDateString()+" "+value.ToLongTimeString();
        }

        private static object CheckForNullData(int value)
        {
            return value == -1 ? (object)"" : value;
        }

        private static object CheckForNullData(double value)
        {
            return value == -1 ? (object)"" : value;
        }

        private static object CheckForNullData01(double value)
        {
            return value == 0 || value == -1 ? (object)"" : value;
        }

        private static object CheckForNullData01(int value)
        {
            return value == 0 || value == -1 ? (object)"" : value;
        }


        private static object CheckForNullData(float value)
        {
            return value == -1 ? (object)"" : value;
        }

        private static object CheckForNullData(TimeSpan value)
        {
            return value.ToString();
        }

        private static int GetQuality(bool where)
        {
            return where ? 1 : 0;
        }

        private static void SetGeneral(General gen, int rowIndex)
        {
            _app.Cells[rowIndex, 1] = CheckForNullData(gen.HeatNumber);
            if (gen.HeatDuration != null)
            {
                _app.Cells[rowIndex, 2] = CheckForNullData(gen.HeatDuration.Start);
                _app.Cells[rowIndex, 3] = CheckForNullData(gen.HeatDuration.End);
                _app.Cells[rowIndex, 4] = CheckForNullData(gen.HeatDuration.Period);
            }
            _app.Cells[rowIndex, 5] = gen.Grade;
            _app.Cells[rowIndex, 6] = CheckForNullData(gen.ConverterLife);

            if (gen.LanceMode != -1)
            _app.Cells[rowIndex, 7] = gen.LanceMode;
            _app.Cells[rowIndex, 8] = gen.TractMode;
            _app.Cells[rowIndex, 9] = gen.ModelInfo.AutoZond;
         //   _app.Cells[rowIndex, 10] = gen.ModelInfo.AutoDovodka;
            if (gen.AvtoDovodka != -1)
            _app.Cells[rowIndex, 10] = gen.AvtoDovodka;

            _app.Cells[rowIndex, 11] = CheckForNullData(gen.TargetC);
            _app.Cells[rowIndex, 12] = CheckForNullData(gen.TargetP);
            _app.Cells[rowIndex, 13] = CheckForNullData(gen.TargetS);
            _app.Cells[rowIndex, 14] = CheckForNullData(gen.TargetCr);
            _app.Cells[rowIndex, 15] = CheckForNullData(gen.TargetNi);
            _app.Cells[rowIndex, 16] = CheckForNullData(gen.TargetCu);
            _app.Cells[rowIndex, 17] = CheckForNullData(gen.TargetMn);
            _app.Cells[rowIndex, 18] = CheckForNullData(gen.TargetMgO);
            _app.Cells[rowIndex, 19] = CheckForNullData(gen.TargetFeO);
            _app.Cells[rowIndex, 20] = CheckForNullData(gen.TargetCaO);
            _app.Cells[rowIndex, 21] = CheckForNullData(gen.TargetT);
            _app.Cells[rowIndex, 22] = CheckForNullData(gen.TargetCaOdivSiO2);
            _app.Cells[rowIndex, 23] = gen.Gang;
            _app.Cells[rowIndex, 24] = gen.Team;
            _app.Cells[rowIndex, 25] = CheckForNullData(gen.HotMetalTime);
            _app.Cells[rowIndex, 26] = CheckForNullData(gen.HotMetalTemp);
            _app.Cells[rowIndex, 27] = CheckForNullData(gen.HotMetalLadle);
            _app.Cells[rowIndex, 28] = CheckForNullData(gen.HotMetalLadleWeight);
            _app.Cells[rowIndex, 29] = CheckForNullData(gen.HotMetalWeightTime);
            _app.Cells[rowIndex, 30] = CheckForNullData(gen.HotMetalKranNumber);
            _app.Cells[rowIndex, 31] = CheckForNullData(gen.HotMetalWeight);
            _app.Cells[rowIndex, 32] = CheckForNullData(gen.ScrapTime);
            _app.Cells[rowIndex, 33] = CheckForNullData(gen.ScrapWeight);
            _app.Cells[rowIndex, 34] = gen.ScrapMix;
            _app.Cells[rowIndex, 35] = gen.ScrapMixWeight;
            _app.Cells[rowIndex, 36] = CheckForNullData(gen.ScrapBucket);
            if (gen.OutDuration != null)
            {
                _app.Cells[rowIndex, 37] = CheckForNullData(gen.OutDuration.Start);
                _app.Cells[rowIndex, 38] = CheckForNullData(gen.OutDuration.End);
                _app.Cells[rowIndex, 39] = CheckForNullData(gen.OutDuration.Period);
            }
            if (gen.OutOtkDuration != null)
            {
                _app.Cells[rowIndex, 40] = CheckForNullData(gen.OutOtkDuration.Start);
                _app.Cells[rowIndex, 41] = CheckForNullData(gen.OutOtkDuration.End);
                _app.Cells[rowIndex, 42] = CheckForNullData(gen.OutOtkDuration.Period);
            }
            _app.Cells[rowIndex, 43] = CheckForNullData(gen.LanceNumber);
           
            _app.Cells[rowIndex, 44] = CheckForNullData(gen.BlowingCount);
            if (gen.Blowing != null)
            {
                if (gen.Blowing.Duration != null)
                {
                    _app.Cells[rowIndex, 45] = CheckForNullData(gen.Blowing.Duration.Start);
                    _app.Cells[rowIndex, 46] = CheckForNullData(gen.Blowing.Duration.End);
                    _app.Cells[rowIndex, 47] = CheckForNullData(gen.Blowing.Duration.Period);
                    _app.Cells[rowIndex, 48] = CheckForNullData(gen.Blowing.O2Flow);
                }
            }
            if (gen.SlagOutDuration != null)
            {
                _app.Cells[rowIndex, 49] = CheckForNullData(gen.SlagOutDuration.Start);
                _app.Cells[rowIndex, 50] = CheckForNullData(gen.SlagOutDuration.End);
                _app.Cells[rowIndex, 51] = CheckForNullData(gen.SlagOutDuration.Period);
            }
            if (gen.Zond1 != null)
            {
                _app.Cells[rowIndex, 52] = CheckForNullData(gen.Zond1.ZondStart);
                _app.Cells[rowIndex, 53] = CheckForNullData(gen.Zond1.ZondC);
                _app.Cells[rowIndex, 55] = CheckForNullData(gen.Zond1.ZondTemp);
            }
         //   if (gen.SteelXimCulc != null)
          //  {
               
                if (gen.ModelInfo!=null)
                _app.Cells[rowIndex, 54] = CheckForNullData( gen.ModelInfo.CarbonSwitcher);
                    // "Вадим";//CheckForNullData(gen.ZondCulcC);
         //   }
            if (gen.Zond2 != null)
            {
                _app.Cells[rowIndex, 56] = CheckForNullData(gen.Zond2.ZondStart);
                _app.Cells[rowIndex, 57] = CheckForNullData(gen.Zond2.ZondC);
                _app.Cells[rowIndex, 58] = CheckForNullData(gen.Zond2.ZondTemp);
            }
            _app.Cells[rowIndex, 59] = CheckForNullData(gen.ZondCulcTemp);
            if (gen.Blowing1 != null)
            {
                if (gen.Blowing1.Duration != null)
                {
                    _app.Cells[rowIndex, 60] = CheckForNullData(gen.Blowing1.Duration.Start);
                    _app.Cells[rowIndex, 61] = CheckForNullData(gen.Blowing1.Duration.End);
                    _app.Cells[rowIndex, 62] = CheckForNullData(gen.Blowing1.Duration.Period);
                }
                _app.Cells[rowIndex, 63] = CheckForNullData(gen.Blowing1.O2Flow);
            }
            if (gen.Blowing2 != null)
            {
                if (gen.Blowing2.Duration != null)
                {
                    _app.Cells[rowIndex, 64] = CheckForNullData(gen.Blowing2.Duration.Start);
                    _app.Cells[rowIndex, 65] = CheckForNullData(gen.Blowing2.Duration.End);
                    _app.Cells[rowIndex, 66] = CheckForNullData(gen.Blowing2.Duration.Period);
                    _app.Cells[rowIndex, 67] = CheckForNullData(gen.Blowing2.O2Flow);
                }
            }
            if (gen.Blowing3 != null)
            {
                if (gen.Blowing3.Duration != null)
                {
                    _app.Cells[rowIndex, 68] = CheckForNullData(gen.Blowing3.Duration.Start);
                    _app.Cells[rowIndex, 69] = CheckForNullData(gen.Blowing3.Duration.End);
                    _app.Cells[rowIndex, 70] = CheckForNullData(gen.Blowing3.Duration.Period);
                    _app.Cells[rowIndex, 71] = CheckForNullData(gen.Blowing3.O2Flow);
                }
            }
            _app.Cells[rowIndex, 72] = CheckForNullData(gen.CorrDeltaT);
            _app.Cells[rowIndex, 73] = CheckForNullData(gen.CorrDeltaC);
            _app.Cells[rowIndex, 74] = CheckForNullData(gen.CorrO2);
            _app.Cells[rowIndex, 75] = CheckForNullData(gen.CorrDolomit);
            if (gen.HotMetalXim1 != null)
            {
                _app.Cells[rowIndex, 76] = CheckForNullData(gen.HotMetalXim1.Time);
                _app.Cells[rowIndex, 77] = gen.HotMetalXim1.Torpedos;
                _app.Cells[rowIndex, 78] = CheckForNullData(gen.HotMetalXim1.Ladles);
                _app.Cells[rowIndex, 79] = CheckForNullData(gen.HotMetalXim1.C);
                _app.Cells[rowIndex, 80] = CheckForNullData(gen.HotMetalXim1.Si);
                _app.Cells[rowIndex, 81] = CheckForNullData(gen.HotMetalXim1.Mn);
                _app.Cells[rowIndex, 82] = CheckForNullData(gen.HotMetalXim1.P);
                _app.Cells[rowIndex, 83] = CheckForNullData(gen.HotMetalXim1.S);
            }
            if (gen.HotMetalXim2 != null)
            {
                _app.Cells[rowIndex, 84] = CheckForNullData(gen.HotMetalXim2.Time);
                _app.Cells[rowIndex, 85] = gen.HotMetalXim2.Torpedos;
                _app.Cells[rowIndex, 86] = CheckForNullData(gen.HotMetalXim2.Ladles);
                _app.Cells[rowIndex, 87] = CheckForNullData(gen.HotMetalXim2.C);
                _app.Cells[rowIndex, 88] = CheckForNullData(gen.HotMetalXim2.Si);
                _app.Cells[rowIndex, 89] = CheckForNullData(gen.HotMetalXim2.Mn);
                _app.Cells[rowIndex, 90] = CheckForNullData(gen.HotMetalXim2.P);
                _app.Cells[rowIndex, 91] = CheckForNullData(gen.HotMetalXim2.S);
            }
            if (gen.HotMetalXim3 != null)
            {
                _app.Cells[rowIndex, 92] = CheckForNullData(gen.HotMetalXim3.Time);
                _app.Cells[rowIndex, 93] = gen.HotMetalXim3.Torpedos;
                _app.Cells[rowIndex, 94] = CheckForNullData(gen.HotMetalXim3.Ladles);
                _app.Cells[rowIndex, 95] = CheckForNullData(gen.HotMetalXim3.C);
                _app.Cells[rowIndex, 96] = CheckForNullData(gen.HotMetalXim3.Si);
                _app.Cells[rowIndex, 97] = CheckForNullData(gen.HotMetalXim3.Mn);
                _app.Cells[rowIndex, 98] = CheckForNullData(gen.HotMetalXim3.P);
                _app.Cells[rowIndex, 99] = CheckForNullData(gen.HotMetalXim3.S);
            }
            if (gen.HotMetalXim4 != null)
            {
                _app.Cells[rowIndex, 100] = CheckForNullData(gen.HotMetalXim4.Time);
                _app.Cells[rowIndex, 101] = gen.HotMetalXim4.Torpedos;
                _app.Cells[rowIndex, 102] = CheckForNullData(gen.HotMetalXim4.Ladles);
                _app.Cells[rowIndex, 103] = CheckForNullData(gen.HotMetalXim4.C);
                _app.Cells[rowIndex, 104] = CheckForNullData(gen.HotMetalXim4.Si);
                _app.Cells[rowIndex, 105] = CheckForNullData(gen.HotMetalXim4.Mn);
                _app.Cells[rowIndex, 106] = CheckForNullData(gen.HotMetalXim4.P);
                _app.Cells[rowIndex, 107] = CheckForNullData(gen.HotMetalXim4.S);
            }
            if (gen.SteelXim1 != null)
            {
                _app.Cells[rowIndex, 108] = CheckForNullData(gen.SteelXim1.Time);
                _app.Cells[rowIndex, 109] = CheckForNullData(gen.SteelXim1.C);
                _app.Cells[rowIndex, 110] = CheckForNullData(gen.SteelXim1.Si);
                _app.Cells[rowIndex, 111] = CheckForNullData(gen.SteelXim1.Mn);
                _app.Cells[rowIndex, 112] = CheckForNullData(gen.SteelXim1.P);
                _app.Cells[rowIndex, 113] = CheckForNullData(gen.SteelXim1.S);
                _app.Cells[rowIndex, 114] = CheckForNullData(gen.SteelXim1.Cr);
                _app.Cells[rowIndex, 115] = CheckForNullData(gen.SteelXim1.Ni);
                _app.Cells[rowIndex, 116] = CheckForNullData(gen.SteelXim1.Cu);
                _app.Cells[rowIndex, 117] = CheckForNullData(gen.SteelXim1.Al);
                _app.Cells[rowIndex, 118] = CheckForNullData(gen.SteelXim1.N);
                _app.Cells[rowIndex, 119] = CheckForNullData(gen.SteelXim1.Mo);
            }
            if (gen.SteelXim2 != null)
            {
                _app.Cells[rowIndex, 120] = CheckForNullData(gen.SteelXim2.Time);
                _app.Cells[rowIndex, 121] = CheckForNullData(gen.SteelXim2.C);
                _app.Cells[rowIndex, 122] = CheckForNullData(gen.SteelXim2.Si);
                _app.Cells[rowIndex, 123] = CheckForNullData(gen.SteelXim2.Mn);
                _app.Cells[rowIndex, 124] = CheckForNullData(gen.SteelXim2.P);
                _app.Cells[rowIndex, 125] = CheckForNullData(gen.SteelXim2.S);
                _app.Cells[rowIndex, 126] = CheckForNullData(gen.SteelXim2.Cr);
                _app.Cells[rowIndex, 127] = CheckForNullData(gen.SteelXim2.Ni);
                _app.Cells[rowIndex, 128] = CheckForNullData(gen.SteelXim2.Cu);
                _app.Cells[rowIndex, 129] = CheckForNullData(gen.SteelXim2.Al);
                _app.Cells[rowIndex, 130] = CheckForNullData(gen.SteelXim2.N);
                _app.Cells[rowIndex, 131] = CheckForNullData(gen.SteelXim2.Mo);
            }
            if (gen.SteelXimCulc != null)
            {
                _app.Cells[rowIndex, 132] = CheckForNullData(gen.SteelXimCulc.C);
                _app.Cells[rowIndex, 133] = CheckForNullData(gen.SteelXimCulc.Si);
                _app.Cells[rowIndex, 134] = CheckForNullData(gen.SteelXimCulc.Mn);
                _app.Cells[rowIndex, 135] = CheckForNullData(gen.SteelXimCulc.P);
                _app.Cells[rowIndex, 136] = CheckForNullData(gen.SteelXimCulc.S);
                _app.Cells[rowIndex, 137] = CheckForNullData(gen.SteelXimCulc.Cr);
                _app.Cells[rowIndex, 138] = CheckForNullData(gen.SteelXimCulc.Ni);
                _app.Cells[rowIndex, 138] = CheckForNullData(gen.SteelXimCulc.Cu);
                _app.Cells[rowIndex, 140] = CheckForNullData(gen.SteelXimCulc.Al);
                _app.Cells[rowIndex, 141] = CheckForNullData(gen.SteelXimCulc.N);
                _app.Cells[rowIndex, 142] = CheckForNullData(gen.SteelXimCulc.Mo);
            }
            _app.Cells[rowIndex, 143] = "";
            _app.Cells[rowIndex, 144] = "";
            _app.Cells[rowIndex, 145] = "";
            _app.Cells[rowIndex, 146] = "";


            _app.Cells[rowIndex, 147] =CheckForNullData( gen.SteelMass);
            _app.Cells[rowIndex, 148] = "";
            _app.Cells[rowIndex, 149] = "";
            if (gen.SlagXim1 != null)
            {
                _app.Cells[rowIndex, 150] = CheckForNullData(gen.SlagXim1.Time);
                _app.Cells[rowIndex, 151] = CheckForNullData(gen.SlagXim1.CaO);
                _app.Cells[rowIndex, 152] = CheckForNullData(gen.SlagXim1.SiO2);
                _app.Cells[rowIndex, 153] = CheckForNullData(gen.SlagXim1.FeO);
                _app.Cells[rowIndex, 154] = CheckForNullData(gen.SlagXim1.MgO);
                _app.Cells[rowIndex, 155] = CheckForNullData(gen.SlagXim1.MnO);
                _app.Cells[rowIndex, 156] = CheckForNullData(gen.SlagXim1.Al2O3);
                _app.Cells[rowIndex, 157] = CheckForNullData(gen.SlagXim1.S);
                _app.Cells[rowIndex, 158] = CheckForNullData(gen.SlagXim1.P2O5);
                _app.Cells[rowIndex, 159] = CheckForNullData(gen.SlagXim1.CaOdivSiO2);
            }
            if (gen.SlagXim2 != null)
            {
                _app.Cells[rowIndex, 160] = CheckForNullData(gen.SlagXim2.Time);
                _app.Cells[rowIndex, 161] = CheckForNullData(gen.SlagXim2.CaO);
                _app.Cells[rowIndex, 162] = CheckForNullData(gen.SlagXim2.SiO2);
                _app.Cells[rowIndex, 163] = CheckForNullData(gen.SlagXim2.FeO);
                _app.Cells[rowIndex, 164] = CheckForNullData(gen.SlagXim2.MgO);
                _app.Cells[rowIndex, 165] = CheckForNullData(gen.SlagXim2.MnO);
                _app.Cells[rowIndex, 166] = CheckForNullData(gen.SlagXim2.Al2O3);
                _app.Cells[rowIndex, 167] = CheckForNullData(gen.SlagXim2.S);
                _app.Cells[rowIndex, 168] = CheckForNullData(gen.SlagXim2.P2O5);
                _app.Cells[rowIndex, 169] = CheckForNullData(gen.SlagXim2.CaOdivSiO2);
            }
            if (gen.SlagXimCulc != null)
            {
                _app.Cells[rowIndex, 170] = CheckForNullData(gen.SlagXimCulc.CaO);
                _app.Cells[rowIndex, 171] = CheckForNullData(gen.SlagXimCulc.SiO2);
                _app.Cells[rowIndex, 172] = CheckForNullData(gen.SlagXimCulc.FeO);
                _app.Cells[rowIndex, 173] = CheckForNullData(gen.SlagXimCulc.MgO);
                _app.Cells[rowIndex, 174] = CheckForNullData(gen.SlagXimCulc.MnO);
                _app.Cells[rowIndex, 175] = CheckForNullData(gen.SlagXimCulc.Al2O3);
                _app.Cells[rowIndex, 176] = CheckForNullData(gen.SlagXimCulc.S);
                _app.Cells[rowIndex, 177] = CheckForNullData(gen.SlagXimCulc.P2O5);
                _app.Cells[rowIndex, 178] = CheckForNullData(gen.SlagXimCulc.CaOdivSiO2);
            }
            if (gen.Gunning != null)
            {
                _app.Cells[rowIndex, 179] = CheckForNullData(gen.Gunning.Koks);
                _app.Cells[rowIndex, 180] = CheckForNullData(gen.Gunning.Izvest);
                _app.Cells[rowIndex, 181] = CheckForNullData(gen.Gunning.Dolomit);
                _app.Cells[rowIndex, 182] = CheckForNullData(gen.Gunning.Doloms);
                _app.Cells[rowIndex, 183] = CheckForNullData(gen.Gunning.MaxG);
                _app.Cells[rowIndex, 184] = CheckForNullData(gen.Gunning.CaC);
            }
            if (gen.Converter != null)
            {
                _app.Cells[rowIndex, 185] = CheckForNullData(gen.Converter.Koks);
                _app.Cells[rowIndex, 186] = CheckForNullData(gen.Converter.Izvest);
                _app.Cells[rowIndex, 187] = CheckForNullData(gen.Converter.Cu);
                _app.Cells[rowIndex, 188] = CheckForNullData(gen.Converter.Nilom);
                _app.Cells[rowIndex, 189] = CheckForNullData(gen.Converter.Dolomit);
                _app.Cells[rowIndex, 190] = CheckForNullData(gen.Converter.Doloms);
                _app.Cells[rowIndex, 191] = CheckForNullData(gen.Converter.MaxG);
                _app.Cells[rowIndex, 192] = CheckForNullData(gen.Converter.Fom);
                _app.Cells[rowIndex, 193] = CheckForNullData(gen.Converter.AlKonc);
                _app.Cells[rowIndex, 194] = CheckForNullData(gen.Converter.Aglom);
                _app.Cells[rowIndex, 195] = CheckForNullData(gen.Converter.Menisp);
                _app.Cells[rowIndex, 196] = CheckForNullData(gen.Converter.CmG);
                _app.Cells[rowIndex, 197] = CheckForNullData(gen.Converter.Feruda);
                _app.Cells[rowIndex, 198] = CheckForNullData(gen.Converter.Flgl);
                _app.Cells[rowIndex, 199] = CheckForNullData(gen.Converter.Teramr);
                _app.Cells[rowIndex, 200] = CheckForNullData(gen.Converter.DolomitLight);
                _app.Cells[rowIndex, 201] = CheckForNullData(gen.Converter.Mmkn75);
                _app.Cells[rowIndex, 202] = CheckForNullData(gen.Converter.Rant70);
                _app.Cells[rowIndex, 203] = CheckForNullData(gen.Converter.Flmg1);
            }

            if (gen.Bucket != null)
            {
                _app.Cells[rowIndex, 204] = CheckForNullData(gen.Bucket.Koks);
                _app.Cells[rowIndex, 205] = CheckForNullData(gen.Bucket.Izvest);
                _app.Cells[rowIndex, 206] = CheckForNullData(gen.Bucket.Cu);
                _app.Cells[rowIndex, 207] = CheckForNullData(gen.Bucket.Nilom);
                _app.Cells[rowIndex, 208] = CheckForNullData(gen.Bucket.Smn17);
                _app.Cells[rowIndex, 209] = CheckForNullData(gen.Bucket.Fesi65);
                _app.Cells[rowIndex, 210] = CheckForNullData(gen.Bucket.Al2);
                _app.Cells[rowIndex, 211] = CheckForNullData(gen.Bucket.AlKat);
                _app.Cells[rowIndex, 212] = CheckForNullData(gen.Bucket.Femn82);
                _app.Cells[rowIndex, 213] = CheckForNullData(gen.Bucket.Femn78);
                _app.Cells[rowIndex, 214] = CheckForNullData(gen.Bucket.Alsech);
                _app.Cells[rowIndex, 215] = CheckForNullData(gen.Bucket.Ni);
                _app.Cells[rowIndex, 216] = CheckForNullData(gen.Bucket.Fev50);
                _app.Cells[rowIndex, 217] = CheckForNullData(gen.Bucket.Fesicr);
                _app.Cells[rowIndex, 218] = CheckForNullData(gen.Bucket.Mn95);
                _app.Cells[rowIndex, 219] = CheckForNullData(gen.Bucket.Femo60);
                _app.Cells[rowIndex, 220] = CheckForNullData(gen.Bucket.Fep);
                _app.Cells[rowIndex, 221] = CheckForNullData(gen.Bucket.Fecr);
                _app.Cells[rowIndex, 222] = CheckForNullData(gen.Bucket.Sica);
                _app.Cells[rowIndex, 223] = CheckForNullData(gen.Bucket.Fenb);
                _app.Cells[rowIndex, 224] = CheckForNullData(gen.Bucket.Sic);
                _app.Cells[rowIndex, 225] = CheckForNullData(gen.Bucket.Albr);
                _app.Cells[rowIndex, 226] = CheckForNullData(gen.Bucket.Fev80);
                _app.Cells[rowIndex, 227] = CheckForNullData(gen.Bucket.Fesi75);
                _app.Cells[rowIndex, 228] = CheckForNullData(gen.Bucket.Alpir);
                _app.Cells[rowIndex, 229] = CheckForNullData(gen.Bucket.Mnbrik);
                _app.Cells[rowIndex, 230] = CheckForNullData(gen.Bucket.Sicbr);
                _app.Cells[rowIndex, 231] = CheckForNullData(gen.Bucket.Femn88);
                _app.Cells[rowIndex, 232] = CheckForNullData(gen.Bucket.Grmagn);
                _app.Cells[rowIndex, 233] = CheckForNullData(gen.Bucket.Ligat2);
                _app.Cells[rowIndex, 234] = CheckForNullData(gen.Bucket.Feni);
                _app.Cells[rowIndex, 235] = CheckForNullData(gen.Bucket.Mnazot);
                _app.Cells[rowIndex, 236] = CheckForNullData(gen.Bucket.Mo99);
                _app.Cells[rowIndex, 237] = CheckForNullData(gen.Bucket.Abk65);
                _app.Cells[rowIndex, 238] = CheckForNullData(gen.Bucket.Feti30);
                _app.Cells[rowIndex, 239] = CheckForNullData(gen.Bucket.Fenb50);
            }
            _app.Cells[rowIndex, 240] = "";
            _app.Cells[rowIndex, 241] = "";
            _app.Cells[rowIndex, 242] = "";
            _app.Cells[rowIndex, 243] = "";
            _app.Cells[rowIndex, 244] = "";
            _app.Cells[rowIndex, 245] = gen.SlagOutbust;
            _app.Cells[rowIndex, 246] = gen.Ignition;
        }


        private static void SetDubleValues(int rowIndex, int i1, int i2)
        {
            for (int i = i1; i <= i2; i++)
            {
                if (rowIndex > 3)
                {
                    _app.Cells[rowIndex, i] = _app.Cells[rowIndex - 1, i];
                    var range = _app.Cells[rowIndex, i] as Range;
                    if (range != null)
                        range.Font.Color = Color.Red;
                }
                else
                {
                    _app.Cells[rowIndex - 1, i] = "";
                }
                var range1 = _app.Cells[rowIndex + 1, i] as Range;
                if (range1 != null)
                    range1.Font.Color = Color.Black;
            }
        }
        private static void SetDubleValue(int rowIndex, int i1, object value)
        {
            if (value.ToString() == "-1")
            {
                if (rowIndex > 3)
                {
                    _app.Cells[rowIndex, i1] = _app.Cells[rowIndex - 1, i1];
                    var range = _app.Cells[rowIndex, i1] as Range;
                    if (range != null)
                        range.Font.Color = Color.Red;
                }
                else
                {
                    _app.Cells[rowIndex - 1, i1] = "";
                }
                var range1 = _app.Cells[rowIndex + 1, i1] as Range;
                if (range1 != null)
                    range1.Font.Color = Color.Black;
            }
            else
            {
                if (value is double)
                    _app.Cells[rowIndex, i1] = CheckForNullData((double) value);
                else
                {
                if (value is float)
                    _app.Cells[rowIndex, i1] = CheckForNullData((float)value);
                else
                {
                    if (value is int)
                        _app.Cells[rowIndex, i1] = CheckForNullData((int)value);
                    else
                    {
                        if (value is DateTime)
                            _app.Cells[rowIndex, i1] = CheckForNullData((DateTime)value);
                        else
                        {
                            _app.Cells[rowIndex, i1] = value.ToString();
                        }
                    }
                }
                }
              
               
            }
        
            }

        public static event HeatInTimeEventHandler HeatInTimeChanged;

        public static void OnHeatInTime(int count, int current)
        {
            HeatInTimeEventHandler handler = HeatInTimeChanged;
            if (handler != null) handler(count, current);
        }

        private static void SetHeatInTimel(IEnumerable<HeatInTime> heatInTimes)
        {
            DateTime prevTime = DateTime.MinValue;
            DateTime timeForRefPoint = DateTime.MinValue;
            bool flagForRefPoint = false;
            bool flagZondStartCommand = false;
            var rowIndex = 3;

            IEnumerable<HeatInTime> inTimes = heatInTimes as List<HeatInTime> ?? heatInTimes.ToList();
            foreach (var time in inTimes)
            {
               // InstantLogger.log(string.Format("process time {0}",time.O2));
            
            if (time.Time.Second == 0)
                {
                    InstantLogger.log(string.Format("process time {0}", time.Time));
                }
            OnHeatInTime(inTimes.Count(), rowIndex-3);
                TimeSpan deltaTime = time.Time - prevTime;
                prevTime = time.Time;
                if (deltaTime.Ticks == 0)
                {
                    continue;
                }
                if (time.Zond!=null)
                {
                    ((Range)_app.Rows[rowIndex]).Interior.Color = time.Zond.GetColor();
                }
                else
                {
                    //((Range)_app.Rows[rowIndex]).Interior.Color = Color.White;
                }

                _app.Cells[rowIndex, 1] = time.Time.ToLongTimeString();
                if (time.Lance != null)
                {
                   
                    _app.Cells[rowIndex, 2] = CheckForNullData(time.Lance.O2TotalVol);
                    _app.Cells[rowIndex, 3] = CheckForNullData(time.Lance.O2Flow);
                    _app.Cells[rowIndex, 4] = CheckForNullData(time.Lance.O2Pressure);
                    _app.Cells[rowIndex, 5] = CheckForNullData(time.Lance.LanceHeight);
                  
                   
                    switch (time.Lance.LanceMode)
                    {
                        case 1 : _app.Cells[rowIndex, 9] = "Ручной";
                            break;
                        case 2 : _app.Cells[rowIndex, 9] = "Автомат";
                            break;
                        case 3 : _app.Cells[rowIndex, 9] = "Компьютер";
                            break;
                        default: _app.Cells[rowIndex, 9] = "";
                            break;
                    }
                    switch (time.Lance.O2FlowMode)
                    {
                        case 1 : _app.Cells[rowIndex, 10] = "Ручной";
                            break;
                        case 2 : _app.Cells[rowIndex, 10] = "Автомат";
                            break;
                        case 3 : _app.Cells[rowIndex, 10] = "Компьютер";
                            break;
                        default: _app.Cells[rowIndex, 10] = "";
                            break;
                    }
                    _app.Cells[rowIndex, 11] = CheckForNullData(time.Lance.O2LeftLanceWaterInput);
                    _app.Cells[rowIndex, 12] = CheckForNullData(time.Lance.O2LeftLanceWaterTempInput);
                    _app.Cells[rowIndex, 13] = CheckForNullData(time.Lance.O2LeftLanceWaterOutput);
                    _app.Cells[rowIndex, 14] = CheckForNullData(time.Lance.O2LeftLanceWaterTempOutput);
                    _app.Cells[rowIndex, 15] = CheckForNullData(time.Lance.O2RightLanceWaterInput);
                    _app.Cells[rowIndex, 16] = CheckForNullData(time.Lance.O2RightLanceWaterTempInput);
                    _app.Cells[rowIndex, 17] = CheckForNullData(time.Lance.O2RightLanceWaterOutput);
                    _app.Cells[rowIndex, 18] = CheckForNullData(time.Lance.O2RightLanceWaterTempOutput);
                    _app.Cells[rowIndex, 19] = CheckForNullData(time.Lance.BathLevel);
                    _app.Cells[rowIndex, 20] = CheckForNullData(time.Lance.O2LeftLanceGewWeight);
                    _app.Cells[rowIndex, 21] = CheckForNullData(time.Lance.O2LeftLanceGewBaer);
                    _app.Cells[rowIndex, 22] = CheckForNullData(time.Lance.O2RightLanceGewWeight);
                    _app.Cells[rowIndex, 23] = CheckForNullData(time.Lance.O2LeftLanceGewBaer);
                }
                else
                {
                    SetDubleValues(rowIndex,2,23);
                }
                if (time.CorrectionCT != null)
                    _app.Cells[rowIndex, 8] = time.CorrectionCT.C;

                if (time.Curkin != null)
                {
                    (_app.Cells[rowIndex, 6] as Range).Font.Color = Color.Black;
                    _app.Cells[rowIndex, 6] = time.Curkin.C;
                }
                if (time.UniversalCPl != null)
                {
                    (_app.Cells[rowIndex, 7] as Range).Font.Color = Color.Black;
                    _app.Cells[rowIndex, 7] = time.UniversalCPl.UniversalCPlusResult;
                }
               
                 if (time.Zond!=null)
                 {
                     _app.Cells[rowIndex, 24] = CheckForNullData(time.Zond.ZondAutoStartCommand);
                     _app.Cells[rowIndex, 25] = CheckForNullData(time.Zond.ZondStartCommand);
                     _app.Cells[rowIndex, 26] = CheckForNullData(time.Zond.Level);
                     _app.Cells[rowIndex, 27] = CheckForNullData(time.Zond.T);
                    /* if (time.Zond.ZondStartCommand > 0 && flagZondStartCommand == false && inTimes.Where(x => x.O2 == time.O2.AddSeconds(-3))
                                        .Select(x => x.TempLiner).First() != null)
                     {
                         TempZondStartCommand =
    inTimes.Where(x => x.O2 == time.O2.AddSeconds(-3))
           .Select(x => x.TempLiner.Temperature).First();
                         flagZondStartCommand = true;
                     }

                     if (time.Zond.T > 0  && inTimes.Where(x => x.O2 == time.O2)
                                       .Select(x => x.TempLiner).First() != null)
                     {
                         TempZondT =
    inTimes.Where(x => x.O2 == time.O2).Select(x => x.TempLiner.Temperature).First();
                     }*/
                 }

                if (time.SublanceControlInTime!=null)
                {
                    _app.Cells[rowIndex, 28] = CheckForNullData(time.SublanceControlInTime.Delay);
                    _app.Cells[rowIndex, 29] = CheckForNullData(time.SublanceControlInTime.BathDelay);
                    _app.Cells[rowIndex, 30] = CheckForNullData(time.SublanceControlInTime.LanceBath);
                    _app.Cells[rowIndex, 31] = CheckForNullData(time.SublanceControlInTime.O2Bath);
                    _app.Cells[rowIndex, 32] = CheckForNullData(time.SublanceControlInTime.LanceMes);
                    _app.Cells[rowIndex, 33] = CheckForNullData(time.SublanceControlInTime.O2Mes);

                }
                if (time.OffGas != null)
                {
                    if(time.OffGas.DecompressionOffGas != null)
                    {
                        SetDubleValue(rowIndex, 35, time.OffGas.DecompressionOffGas.Decompression);
                     //   _app.Cells[rowIndex, 24] = CheckForNullData(time.OffGas.DecompressionOffGas.Decompression);
                    }
                    else
                    {
                        SetDubleValues(rowIndex, 35, 35);
                    }
                    if(time.OffGas.OffGas != null)
                    {
                       
                   
                       
                        SetDubleValue(rowIndex, 34, time.OffGas.OffGas.OffGasFlow);
                       
                         SetDubleValue(rowIndex, 36, time.OffGas.OffGas.OffGasTemp);
                        switch (time.OffGas.OffGas.OffGasHoodPos)
                        {
                            case 0:
                                _app.Cells[rowIndex, 37] = "верх";
                                break;
                            case 1:
                                _app.Cells[rowIndex, 37] = "середина";
                                break;
                            case 2:
                                _app.Cells[rowIndex, 37] = "низ";
                                break;
                            default:
                                SetDubleValue(rowIndex, 37, time.OffGas.OffGas.OffGasHoodPos);
                             //   _app.Cells[rowIndex, 27] = "";
                                break;
                        }
                        switch (time.OffGas.OffGas.OffGasFilterControlPos)
                        {
                                 
                            case 0: 
                                _app.Cells[rowIndex, 42] = "без дожигания";
                                break;
                            case 1: 
                                _app.Cells[rowIndex, 42] = "част.дожигание";
                                break;
                            case 2: 
                                _app.Cells[rowIndex, 42] = "с дожиганием";
                                break;
                            default:
                                SetDubleValue(rowIndex, 42, time.OffGas.OffGas.OffGasFilterControlPos);
                                //_app.Cells[rowIndex, 32] = "";
                                break;

                        }
                        SetDubleValue(rowIndex, 43, time.OffGas.OffGas.OffGasCounter);
                      //  _app.Cells[rowIndex, 33] = CheckForNullData(time.OffGas.OffGas.OffGasCounter);
                    }
                    else
                    {
                        SetDubleValues(rowIndex, 34, 43);
                    }
                    if (time.OffGas.BoilerWaterCooling != null)
                    {
                        SetDubleValue(rowIndex, 38, time.OffGas.BoilerWaterCooling.GasTemperatureOnExit);
                                               //  SetDubleValue(rowIndex, 32, time.OffGas.OffGas.OffGasCounter);
                       // SetDubleValue(rowIndex, 37, time.OffGas.BoilerWaterCooling.PrecollingGasTemperature);
                        SetDubleValue(rowIndex, 39, time.OffGas.BoilerWaterCooling.PrecollingGasTemperature);
                                               //  SetDubleValue(rowIndex, 32, time.OffGas.OffGas.OffGasCounter);
                      //  SetDubleValue(rowIndex, 39, time.OffGas.BoilerWaterCooling.GasTemperatureAfter1Step);
                        SetDubleValue(rowIndex, 40, time.OffGas.BoilerWaterCooling.GasTemperatureAfter1Step);
                                              //  SetDubleValue(rowIndex, 32, time.OffGas.OffGas.OffGasCounter);
                     //   SetDubleValue(rowIndex, 30, time.OffGas.BoilerWaterCooling.GasTemperatureAfter2Step);
                        SetDubleValue(rowIndex, 41, time.OffGas.BoilerWaterCooling.GasTemperatureAfter2Step);
                        /*  //  SetDubleValue(rowIndex, 32, time.OffGas.OffGas.OffGasCounter);
                        SetDubleValue(rowIndex, 27, time.OffGas.BoilerWaterCooling.GasTemperatureOnExit);
                        _app.Cells[rowIndex, 27] = CheckForNullData(time.OffGas.BoilerWaterCooling.GasTemperatureOnExit);
                                               //  SetDubleValue(rowIndex, 32, time.OffGas.OffGas.OffGasCounter);
                        SetDubleValue(rowIndex, 28, time.OffGas.BoilerWaterCooling.PrecollingGasTemperature);
                        _app.Cells[rowIndex, 28] = CheckForNullData(time.OffGas.BoilerWaterCooling.PrecollingGasTemperature);
                                               //  SetDubleValue(rowIndex, 32, time.OffGas.OffGas.OffGasCounter);
                        SetDubleValue(rowIndex, 29, time.OffGas.BoilerWaterCooling.GasTemperatureAfter1Step);
                        _app.Cells[rowIndex, 29] = CheckForNullData(time.OffGas.BoilerWaterCooling.GasTemperatureAfter1Step);
                                              //  SetDubleValue(rowIndex, 32, time.OffGas.OffGas.OffGasCounter);
                        SetDubleValue(rowIndex, 30, time.OffGas.BoilerWaterCooling.GasTemperatureAfter2Step);
                        _app.Cells[rowIndex, 30] = CheckForNullData(time.OffGas.BoilerWaterCooling.GasTemperatureAfter2Step);*/
                    }
                    else
                    {
                       // SetDubleValues(rowIndex, 27, 30);
                    }
                }
                else
                {
                    SetDubleValues(rowIndex, 34, 43);
                }
                    if (time.OffGasAnalysis != null)
                    {
                        _app.Cells[rowIndex, 44] = CheckForNullData(time.OffGasAnalysis.H2);
                        _app.Cells[rowIndex, 45] = CheckForNullData(time.OffGasAnalysis.O2);
                        _app.Cells[rowIndex, 46] = CheckForNullData(time.OffGasAnalysis.CO);
                        _app.Cells[rowIndex, 47] = CheckForNullData(time.OffGasAnalysis.CO2);
                        _app.Cells[rowIndex, 48] = CheckForNullData(time.OffGasAnalysis.N2);
                        _app.Cells[rowIndex, 49] = CheckForNullData(time.OffGasAnalysis.Ar);

                      
                        _app.Cells[rowIndex, 50] = time.OffGasAnalysis.Brantch == 1 ? "Б" : "А";// CheckForNullData(time.OffGasAnalysis.Brantch);
                        _app.Cells[rowIndex, 51] = CheckForNullData(time.OffGasAnalysis.Delay1);
                        _app.Cells[rowIndex, 52] = CheckForNullData(time.OffGasAnalysis.Delay2);
                        _app.Cells[rowIndex, 53] = CheckForNullData(time.OffGasAnalysis.Error);

                    }
                    else
                    {
                        SetDubleValues(rowIndex, 44, 53);
                    }

                    if (time.RefPoint != null)
                    {
                        if (time.RefPoint.TimeRefPoint == time.Time)
                        {
                            ((Range) _app.Rows[rowIndex]).Interior.Color = Color.Gold;
                            _app.Cells[rowIndex, 54] = 1;
                        }
                    }
                    if (time.Lance != null && flagForRefPoint == false)
                    {
                        if (time.Lance.O2TotalVol == time.RefPoint.O2 && time.RefPoint.O2 != 0)
                        {
                            ((Range)_app.Rows[rowIndex]).Interior.Color = Color.Gold;
                            _app.Cells[rowIndex, 55] = 1;
                            _app.Cells[rowIndex, 56] = CheckForNullData(time.RefPoint.C);
                            flagForRefPoint = true;
                        }
                    }
                   

                    if (time.Balance != null)
                    {
                        _app.Cells[rowIndex, 57] = CheckForNullData(time.Balance.C);
                        _app.Cells[rowIndex, 58] = CheckForNullData(time.Balance.T);
                        _app.Cells[rowIndex, 59] = CheckForNullData(time.Balance.Si);
                        _app.Cells[rowIndex, 60] = CheckForNullData(time.Balance.Mn);
                        _app.Cells[rowIndex, 61] = CheckForNullData(time.Balance.Al);
                        _app.Cells[rowIndex, 62] = CheckForNullData(time.Balance.Cr);
                        _app.Cells[rowIndex, 63] = CheckForNullData(time.Balance.P);
                        _app.Cells[rowIndex, 64] = CheckForNullData(time.Balance.Ti);
                        _app.Cells[rowIndex, 65] = CheckForNullData(time.Balance.V);
                        _app.Cells[rowIndex, 66] = CheckForNullData(time.Balance.Fe);
                        _app.Cells[rowIndex, 67] = CheckForNullData(time.Balance.CaO);
                        _app.Cells[rowIndex, 68] = CheckForNullData(time.Balance.FeO);
                        _app.Cells[rowIndex, 69] = CheckForNullData(time.Balance.SiO2);
                        _app.Cells[rowIndex, 70] = CheckForNullData(time.Balance.MnO);
                        _app.Cells[rowIndex, 71] = CheckForNullData(time.Balance.MgO);
                        if (Convert.ToDouble(time.Balance.SiO2) != 0) 
                            _app.Cells[rowIndex, 72] = Convert.ToDouble(time.Balance.CaO)/Convert.ToDouble(time.Balance.SiO2);
                    }
                    if (time.Statistic != null)
                    {
                        _app.Cells[rowIndex, 73] = CheckForNullData(time.Statistic.C);
                        _app.Cells[rowIndex, 75] = CheckForNullData(time.Statistic.T);
                        _app.Cells[rowIndex, 76] = CheckForNullData(time.Statistic.TempInBoiler);
                        _app.Cells[rowIndex, 77] = CheckForNullData(time.Statistic.DeltaTempinBoiler);
                        _app.Cells[rowIndex, 78] = CheckForNullData(time.Statistic.DeltaTempInBlow);
                    }
                    if (time.TempLiner != null)
                    {
                        SetDubleValue(rowIndex, 74, time.TempLiner.Temperature);
                    }

                    if (time.Modes != null)
                    {
                        _app.Cells[rowIndex, 79] = CheckForNullData(time.Modes.SlagOut);
                        _app.Cells[rowIndex, 80] = CheckForNullData(time.Modes.Ignition);
                    }
                    else
                    {

                        //SetDubleValues(rowIndex, 63, 64);
                    }
                    if (time.GasFlow != null)
                    {
                        SetDubleValue(rowIndex, 81, time.GasFlow.O2Pressure);
                       // _app.Cells[rowIndex, 66] = CheckForNullData(time.GasFlow.O2Pressure);
                        SetDubleValue(rowIndex, 82, time.GasFlow.O2Drop);
                     //   _app.Cells[rowIndex, 67] = CheckForNullData(time.GasFlow.O2Drop);
                        SetDubleValue(rowIndex, 83, time.GasFlow.O2Temp);
                     //   _app.Cells[rowIndex, 68] = CheckForNullData(time.GasFlow.O2Temp);
                        SetDubleValue(rowIndex, 84, time.GasFlow.N2FlowWnd);
                    //    _app.Cells[rowIndex, 69] = CheckForNullData(time.GasFlow.N2FlowWnd);
                        SetDubleValue(rowIndex, 85, time.GasFlow.N2FlowBlr);
                    //    _app.Cells[rowIndex, 70] = CheckForNullData(time.GasFlow.N2FlowBlr);
                        //SetDubleValue(rowIndex, 71, time.GasFlow.N2FlowLeak);
                   /*     _app.Cells[rowIndex, 71] = CheckForNullData(time.GasFlow.N2FlowLeak);
                      //  SetDubleValue(rowIndex, 72, time.GasFlow.N2PressureWnd);
                        _app.Cells[rowIndex, 72] = CheckForNullData(time.GasFlow.N2PressureWnd);
                      //  SetDubleValue(rowIndex, 73, time.GasFlow.N2PressureBlr);
                        _app.Cells[rowIndex, 73] = CheckForNullData(time.GasFlow.N2PressureBlr);
                      //  SetDubleValue(rowIndex, 74, time.GasFlow.N2PressureLeak);
                        _app.Cells[rowIndex, 74] = CheckForNullData(time.GasFlow.N2PressureLeak);
                     //   SetDubleValue(rowIndex, 75, time.GasFlow.N2DropWnd);
                        _app.Cells[rowIndex, 75] = CheckForNullData(time.GasFlow.N2DropWnd);
                    //    SetDubleValue(rowIndex, 76, time.GasFlow.N2DropBlr);
                        _app.Cells[rowIndex, 76] = CheckForNullData(time.GasFlow.N2DropBlr);
                     //   SetDubleValue(rowIndex, 77, time.GasFlow.N2DropLeak);
                        _app.Cells[rowIndex, 77] = CheckForNullData(time.GasFlow.N2DropLeak);
                     //   SetDubleValue(rowIndex, 78, time.GasFlow.N2TempWnd);
                        _app.Cells[rowIndex, 78] = CheckForNullData(time.GasFlow.N2TempWnd);
                     //   SetDubleValue(rowIndex, 79, time.GasFlow.N2TempBlr);
                        _app.Cells[rowIndex, 79] = CheckForNullData(time.GasFlow.N2TempBlr);
                      //  SetDubleValue(rowIndex, 80, time.GasFlow.N2TempWnd);
                        _app.Cells[rowIndex, 80] = CheckForNullData(time.GasFlow.N2TempWnd);*/
                    }
                    else
                    {
                        SetDubleValues(rowIndex, 81, 95);
                    }
                    if (time.AddMaterial!= null)
                    {
                        _app.Cells[rowIndex, 96] = time.AddMaterial.MaterialName;
                        _app.Cells[rowIndex, 97] = CheckForNullData(time.AddMaterial.Weight);

                    }
                    if (time.Template!=null)
                    {
                        _app.Cells[rowIndex, 98] = time.Template.TotalO2Flow;
                        _app.Cells[rowIndex, 99] = time.Template.Step;
                        _app.Cells[rowIndex, 100] = time.Template.LancePosition;
                        _app.Cells[rowIndex, 101] = time.Template.O2Flow;
                        foreach (var materialse in time.Template.Materials)
                        {
                            _app.Cells[rowIndex, 102] = materialse.MaterialName;
                            _app.Cells[rowIndex, 103] = materialse.Bunker;
                            _app.Cells[rowIndex, 104] = materialse.Weight;
                            _app.Cells[rowIndex, 105] = materialse.AllowToAdd;
                            _app.Cells[rowIndex, 106] = materialse.NotToGive;
                        }
                    }
                    
                
                rowIndex++;
            }
        }



        public void ExcelSheetCharge(General general, int rowIndex)
        {
            InstantLogger.msg("{0} Export charge data.", general.HeatNumber);
            _wb.Worksheets[7].Select();
            var row = rowIndex + 1;
            _app.Cells[row, 1] = general.HeatNumber;
            _app.Cells[row, 2] = general.Grade;
            _app.Cells[row, 3] = CheckForNullData01(general.Charge.SteelGroup);
            _app.Cells[row, 4] = CheckForNullData01(general.Charge.Result_Iron);
            _app.Cells[row, 5] = CheckForNullData(general.HotMetalTemp);
            _app.Cells[row, 6] = CheckForNullData01(general.Charge.Result_Scrap);
            _app.Cells[row, 7] = CheckForNullData01(general.Charge.Charge_);
            _app.Cells[row, 8] = CheckForNullData01(general.Charge.Target_C);
            _app.Cells[row, 9] = CheckForNullData01(general.Charge.Target_P);
            _app.Cells[row, 10] = CheckForNullData01(general.Charge.Target_MgO);
            _app.Cells[row, 11] = CheckForNullData01(general.Charge.Target_FeO);
            _app.Cells[row, 12] = CheckForNullData01(general.Charge.Target_CaOdivSiO2);
            _app.Cells[row, 13] = CheckForNullData01(general.Charge.Target_Temperature);
            _app.Cells[row, 14] = CheckForNullData01(general.Charge.Balance_Scrap);
            _app.Cells[row, 15] = CheckForNullData01(general.Charge.Balance_Iron);
            _app.Cells[row, 16] = CheckForNullData01(general.Charge.Statistic_AB_Scrap); 
            _app.Cells[row, 17] = CheckForNullData01(general.Charge.Statistic_AB_Iron);
            _app.Cells[row, 18] = CheckForNullData01(general.Charge.Statistic_K_Scrap);
            _app.Cells[row, 19] = CheckForNullData01(general.Charge.Statistic_K_Iron);
            InstantLogger.msg("{0} Export charge data. Done", general.HeatNumber);
        }


        public void ExcelSheetChargeMaterial(General general, int rowIndex)
        {
            InstantLogger.msg("{0} Export charge data.", general.HeatNumber);
            _wb.Worksheets[8].Select();
            var row = rowIndex + 1;
            _app.Cells[row, 1] = general.HeatNumber;
            _app.Cells[row, 2] = general.Grade;
            _app.Cells[row, 3] = CheckForNullData(general.Charge.SteelGroupCell);
            _app.Cells[row, 4] = CheckForNullData(general.HotMetalLadleWeight/1000);
            _app.Cells[row, 5] = CheckForNullData(general.HotMetalTemp);
            _app.Cells[row, 6] = CheckForNullData(general.ScrapWeight/1000);
            _app.Cells[row, 7] = CheckForNullData(general.HotMetalLadleWeight / 1000 + general.ScrapWeight / 1000);
            _app.Cells[row, 8] = CheckForNullData(general.TargetC);
            _app.Cells[row, 9] = CheckForNullData(general.TargetP);
            _app.Cells[row, 10] = CheckForNullData(general.TargetMgO);
            _app.Cells[row, 11] = CheckForNullData(general.TargetFeO);
            _app.Cells[row, 12] = CheckForNullData(general.TargetCaOdivSiO2);
            _app.Cells[row, 13] = CheckForNullData(general.TargetT);
            _app.Cells[row, 14] = CheckForNullData01(general.Charge.FluxWeight1_2);
            _app.Cells[row, 15] = CheckForNullData01(general.Charge.FluxWeight2_2);
            _app.Cells[row, 16] = CheckForNullData01(general.Charge.FluxWeight2_7);
            _app.Cells[row, 17] = CheckForNullData01(general.Charge.FluxWeight1_7);
            InstantLogger.msg("{0} Export charge data. Done", general.HeatNumber);
        }

        public void ExcelSheetModelChemestry(General general, int rowIndex)
        {
            InstantLogger.msg("{0} Export chemestry  models data.", general.HeatNumber);
            _wb.Worksheets[9].Select();
            var row = rowIndex + 2;
            _app.Cells[row, 1] = general.HeatNumber;
            var j = 1;
                j++;
                var modelChemisry = general.ModelInfo.GetChemestry("LIME");
            var limeDic = new List<string>()
                {
                    "С",
                    "CaO",
                    "cp",
                    "eH",
                    "MgO",
                    "ro",
                    "Steel",
                    "T",
                    "TeH",
                    "TOTAL",
                    "Yield"

                };

               if (modelChemisry!=null)
                   foreach (var d in limeDic)
                   {
                       _app.Cells[row, j] = modelChemisry.GetValue(d);
                       j++;
                   }
                j = 13;
                 modelChemisry = general.ModelInfo.GetChemestry("DOLMAX");
            var dolomaxList = new List<string>
                {
                    "Al2O3",
                    "CaO",
                    "cp",
                    "eH",
                    "MgO",
                    "ro",
                    "SiO2",
                    "Steel",
                    "T",
                    "TeH",
                    "TOTAL",
                    "Yield"

                };

            if (modelChemisry!=null)
                foreach (var d in dolomaxList)
                {
                    _app.Cells[row, j] = modelChemisry.GetValue(d);
                    j++;
                }

                 j = 25;
                modelChemisry = general.ModelInfo.GetChemestry("COKE");
            var cokeList = new List<string>
                {
                    "С",
                    "cp",
                    "eH",
                    "ro",
                    "Steel",
                    "T",
                    "TeH",
                    "TOTAL",
                    "Yield"

                };
            
            if (modelChemisry!=null)
                foreach (var d in cokeList)
                {
                    _app.Cells[row, j] = modelChemisry.GetValue(d);
                    j++;
                }
            j = 34;
                modelChemisry = general.ModelInfo.GetChemestry("FOM");
            var fomList = new List<string>
                {
                    "СaO",
                    "cp",
                    "eH",
                    "MgO",
                    "ro",
                    "S",
                    "SiO2",
                    "Steel",
                    "T",
                    "TeH",
                    "TOTAL",
                    "Yield"

                };
            
                if (modelChemisry!=null)
                    foreach (var d in fomList)
                    {
                        _app.Cells[row, j] = modelChemisry.GetValue(d);
                        j++;
                    }
            j = 46;
                modelChemisry = general.ModelInfo.GetChemestry("IRON");
                var dictIron = new List<string>
                {
                    "С",
                    "cp",
                    "Cr",
                    "Cu",
                    "eH",
                    "Fe",
                    "H",
                    "Mn",
                    "Mo",
                    "N",
                    
                    "Ni",
                    "P",
                  
                    "ro",
                    "S",
                    "Sb",
                    "Si",
                    "Sn",
                    "Steel",
                    "T",
                    "TeH",
                    "Ti",
                    "TOTAL",
                    "Yield",
                    "Zn"
                    
                };
            if (modelChemisry!=null)
               
                    foreach (var d in dictIron)
                    {
                        _app.Cells[row, j] = modelChemisry.GetValue(d);
                        j++;
                    }
                
            j = 70;
            modelChemisry = general.ModelInfo.GetChemestry("SCRAP");
            var dict = new List<string>
                {
                    "Al",
                    "C",
                    "CaO",
                    "Co",
                    "cp",
                    "Cr",
                    "Cu",
                    "eH",
                    "Fe",
                    "FeO2",
                    "H",
                    "Mn",
                    "Mo",
                    "N",
                    "Ni",
                    "O",
                    "P",
                    "ro",
                    "S",
                    "SiO2",
                    "Sb",
                    "Si",
                    "Sn",
                    "Steel",
                    "T",
                    "TeH",
                    "Ti",
                    "TOTAL",
                    "V",
                    "W",
                    "Yield",
                    "Zn"
                };
            if (modelChemisry!=null)
            foreach (var d in dict)
            {
                _app.Cells[row, j] =  modelChemisry.GetValue(d);
                j++; 
            }
            
            
            InstantLogger.msg("{0} Export models data. Done", general.HeatNumber);

        }

        private double C;
        private double CPlusFix;
        private double UniversalCPlusDateFix;
        public void ExcelSheetHeatModel(General general, int rowIndex)
        {
            C = 0;
            CPlusFix = 0;
            UniversalCPlusDateFix = 0;
            if (general.SteelXim1 != null && general.SteelXim1.C != -1)
                C = general.SteelXim1.C;
            if (general.ModelInfo != null && general.ModelInfo.CPlusFix != -1)
                CPlusFix = general.ModelInfo.CPlusFix;
            if (general.ModelInfo != null && general.ModelInfo.UniversalCPlusDateFix != -1)
                UniversalCPlusDateFix = general.ModelInfo.UniversalCPlusDateFix;
            
            InstantLogger.msg("{0} Export models data.", general.HeatNumber);
            _wb.Worksheets[12].Select();
            var row = rowIndex + 1;
            _app.Cells[row, 1] = general.HeatNumber;
            _app.Cells[row, 2] = general.ModelInfo.CurrentModelType;
            _app.Cells[row, 3] = general.ModelInfo.Danger;
            _app.Cells[row, 4] = CheckForNullData(general.ModelInfo.ZondDate);
            _app.Cells[row, 5] = CheckForNullData(general.ModelInfo.ZondStartCommandDate);
            _app.Cells[row, 6] = CheckForNullData(general.ModelInfo.RecommendMeteringB);
            if (general.ModelInfo.RecommendBalanceBlow == -3)
                _app.Cells[row, 7] = "";
            else
            {
                _app.Cells[row, 7] = CheckForNullData(general.ModelInfo.RecommendBalanceBlow);

            }
            _app.Cells[row, 8] = CheckForNullData(general.ModelInfo.DolomsStatModel);
            if ((general.ModelInfo.CurrentT <= general.ModelInfo.TargetT + 10 &&
                general.ModelInfo.CurrentT >= general.ModelInfo.TargetT) || (general.ModelInfo.CurrentT <= general.ModelInfo.TargetT &&
                general.ModelInfo.CurrentT >= general.ModelInfo.TargetT - 10))
                _app.Cells[row, 9] = 1;
            _app.Cells[row, 10] = CheckForNullData(general.ModelInfo.TotalBlowingO2);
            _app.Cells[row, 11] = CheckForNullData(general.ModelInfo.RecommendedBalanceBlow);
            _app.Cells[row, 14] = CheckForNullData(general.ModelInfo.CurkinCFix);
            _app.Cells[row, 15] = CheckForNullData(general.ModelInfo.O2Total);
            _app.Cells[row, 16] = CheckForNullData(general.TargetC);
            _app.Cells[row, 17] = CheckForNullData(general.ModelInfo.TargetCu);
            _app.Cells[row, 18] = CheckForNullData(general.ModelInfo.CurrentCurkin);
            _app.Cells[row, 19] = CheckForNullData(general.ModelInfo.CurrentCCheh);
            _app.Cells[row, 20] = CheckForNullData(general.ModelInfo.O2Total);
            _app.Cells[row, 22] = CheckForNullData(general.ModelInfo.StartCommandZondO2);
            if (general.ModelInfo != null && general.SteelXim1 != null)
            {
                _app.Cells[row, 23] = Math.Abs(CPlusFix - C);
                if (Math.Abs(CPlusFix - C) < 0.01)
                    ((Range) _app.Cells[row, 23]).Interior.Color = Color.Green;
                else
                    ((Range)_app.Cells[row, 23]).Interior.Color = Color.Red;
            }
                
            _app.Cells[row, 24] = CheckForNullData(general.ModelInfo.CPlusFix);
            if (general.SteelXim1 != null)
                _app.Cells[row, 25] = CheckForNullData(general.SteelXim1.C);

            _app.Cells[row, 26] = CheckForNullData(general.ModelInfo.UniversalCPlusDateFix);// _app.Cells[rowIndex, 247] = gen.ModelInfo.UniversalCPlusDateFix;
            if (general.ModelInfo != null && general.SteelXim1 != null)
            {
                _app.Cells[row, 27] = Math.Abs(UniversalCPlusDateFix - C);
                if (Math.Abs(UniversalCPlusDateFix - C) < 0.01)
                    ((Range)_app.Cells[row, 27]).Interior.Color = Color.Green;
                else
                    ((Range)_app.Cells[row, 27]).Interior.Color = Color.Red;
            }

            _app.Cells[row, 28] = CheckForNullData(general.ModelInfo.CarbonSwitcher);
            if (general.Zond1 != null)
                _app.Cells[row, 29] = CheckForNullData(general.Zond1.ZondTemp);
            _app.Cells[row, 30] = CheckForNullData(general.ModelInfo.T_TempLinerStartZond);
            if (general.Zond2 != null)
                _app.Cells[row, 32] = CheckForNullData(general.Zond2.ZondTemp);
            _app.Cells[row, 33] = CheckForNullData(general.ModelInfo.T_TempLinerZondT);

            _app.Cells[row, 35] = general.ModelInfo.AutoZond;
            _app.Cells[row, 36] = general.ModelInfo.AutoZondMode;
            _app.Cells[row, 37] = general.ModelInfo.AutoDovodka;
            _app.Cells[row, 38] = general.ModelInfo.AutodovodkaModel;
            InstantLogger.msg("{0} Export models data. Done", general.HeatNumber);
         
          }
        public void ExcelSheetHeatQualityControl(General general, int rowIndex)
        {
            InstantLogger.msg("{0} Export heat control.", general.HeatNumber);
            _wb.Worksheets[11].Select();
            var row = rowIndex + 2;
            _app.Cells[row, 1] = general.HeatNumber;
            _app.Cells[row, 2] = general.Grade;
            _app.Cells[row, 3] = general.GradeGroup;
            var s = "№";
            foreach (var scrap in general.HeatQuality.Scraps)
            {
                s += string.Format("{0},", scrap);
            }
            s = s.Remove(s.Count() - 1);
            _app.Cells[row, 4] = s;
            _app.Cells[row, 5] = GetQuality(general.HeatQuality.IsQualityScrap);
            _app.Cells[row, 6] = CheckForNullData(general.HeatQuality.IronSi);
            _app.Cells[row, 7] = GetQuality(general.HeatQuality.IronSi07);
            _app.Cells[row, 8] = GetQuality(general.HeatQuality.IronSi025);
            _app.Cells[row, 9] = CheckForNullData(general.HeatQuality.SlagCao);
            _app.Cells[row, 10] = CheckForNullData(general.HeatQuality.HeatIzvestWeight);
            _app.Cells[row, 11] = CheckForNullData(general.HeatQuality.HeatIzvWeightCalc);
            _app.Cells[row, 12] = CheckForNullData(general.HeatQuality.MetalLevelByZond);
            _app.Cells[row, 13] = CheckForNullData(general.HeatQuality.MetalLevelByHandInput);
            _app.Cells[row, 14] = GetQuality(general.HeatQuality.MetalLevelInRange);
            _app.Cells[row, 15] = CheckForNullData(general.HeatQuality.AlConWeight);
            _app.Cells[row, 16] = CheckForNullData(general.HeatQuality.AlConInRange1);
            _app.Cells[row, 17] = CheckForNullData(general.HeatQuality.AlConInRange2);
            _app.Cells[row, 18] = general.HeatQuality.SlagButton;
            _app.Cells[row, 19] = CheckForNullData(general.HeatQuality.MaxConverterAngle);
            _app.Cells[row, 20] = GetQuality(general.HeatQuality.ConverterAngleInRange);
            _app.Cells[row, 21] = CheckForNullData(general.HeatQuality.TransADelay);
            _app.Cells[row, 22] = CheckForNullData(general.HeatQuality.TransBDelay);
            _app.Cells[row, 23] = CheckForNullData(general.HeatQuality.Diag);


            string corr = "";
            foreach (var lancecorr in general.HeatQuality.LanceCorrection)
            {
                corr += string.Format("{0}; ", lancecorr);
            }
            _app.Cells[row, 24] = corr;
           corr = "";
            foreach (var lancecorr in general.HeatQuality.IntenceCorrection)
            {
                corr += string.Format("{0}; ", lancecorr);
            }
            _app.Cells[row, 25] = corr;

            corr = "";
            foreach (var lancecorr in general.HeatQuality.O2Correction)
            {
                corr += string.Format("{0}; ", lancecorr);
            }
            _app.Cells[row, 26] = corr;
           if (general.Zond1!=null)
            _app.Cells[row, 27] =CheckForNullData( general.Zond1.Oxigen);
            _app.Cells[row, 28] =CheckForNullData(general.SteelMass);

            _app.Cells[row, 29] = CheckForNullData(general.HeatQuality.GoodMlnz);
            _app.Cells[row, 30] = CheckForNullData(general.HeatQuality.ColdMlnz);
            if ((general.HotMetalLadleWeight + general.ScrapWeight>0)&&(general.HeatQuality.GoodMlnz>0))
            _app.Cells[row, 31] = CheckForNullData(general.HeatQuality.GoodMlnz / (general.HotMetalLadleWeight + general.ScrapWeight));
            if (general.HeatQuality.SublanceControl.Count == 8)
            for (int i = 0; i < 8; i++)
            {
                _app.Cells[row, 32 + i] = CheckForNullData(general.HeatQuality.SublanceControl[i]);
            }
            InstantLogger.msg("{0} Export heat control. done", general.HeatNumber);
        }


        public void ExcelSheetQualityControl(General general, int rowIndex)
        {
            InstantLogger.msg("{0} Export quality control.", general.HeatNumber);
            _wb.Worksheets[10].Select();
            var row = rowIndex + 2;
            _app.Cells[row, 1] = general.HeatNumber;
            string hotMetal = "";
            if (general.HotMetalXim1 != null)
                hotMetal += string.Format("Химия {0}; ", CheckForNullData(general.HotMetalXim1.Time));
            hotMetal += string.Format("Вес {0}",CheckForNullData( general.HotMetalWeightTime));

            _app.Cells[row, 2] = CheckForNullData(general.Blowing.Duration.Start);

            _app.Cells[row, 3] = hotMetal;
            if (general.HotMetalXim1 != null)
                if (general.Blowing.Duration.Start < general.HotMetalWeightTime && general.Blowing.Duration.Start < general.HotMetalXim1.Time)
                {
                    var s = "";
                   
                    if (general.Blowing.Duration.Start<general.HotMetalWeightTime)
                        s += "Вес " + (general.HotMetalWeightTime - general.Blowing.Duration.Start).ToString()+" ";
                    if (general.Blowing.Duration.Start<general.HotMetalWeightTime)
                           s += "Химия " + (general.HotMetalXim1.Time - general.Blowing.Duration.Start).ToString();
                                                      
                    _app.Cells[row, 4] = s;
               }
                
            _app.Cells[row, 5] = CheckForNullData(general.ScrapTime);

            _app.Cells[row, 6] = GetQuality(general.Blowing.Duration.Start < general.ScrapTime);
            

            _app.Cells[row, 7] = GetQuality(general.HotMetalTemp > 1300 && general.HotMetalTemp <= 1470&&general.Blowing.Duration.Start.AddMinutes(5)>general.HotMetalWeightTime);
            _app.Cells[row, 8] = GetQuality( general.HotMetalLadleWeight > 260000 && general.HotMetalLadleWeight <= 340000);
            _app.Cells[row, 9] = GetQuality((general.HotMetalLadleWeight % 5000) == 0);
            _app.Cells[row, 10] = GetQuality( general.ScrapWeight > 60000 && general.ScrapWeight <= 130000);
            _app.Cells[row, 11] = GetQuality(general.HotMetalXim1 != null && (general.HotMetalXim1.C > 4 && general.HotMetalXim1.C <= 5));
            _app.Cells[row, 12] = GetQuality(general.HotMetalXim1 != null && (general.HotMetalXim1.P > 0.05 && general.HotMetalXim1.P <= 0.078));
            _app.Cells[row, 13] = GetQuality(general.HotMetalXim1 != null && (general.HotMetalXim1.Si > 0.03 && general.HotMetalXim1.Si <= 1));
            _app.Cells[row,14] = (general.Quality.MinOffGasFlow>=330000&& general.Quality.MaxOffGasFlow<=450000)?"1": "0";
           // _app.Cells[row, 9] = "";
            _app.Cells[row, 15] = general.Quality.IsAlarmStrop;
            _app.Cells[row, 16] = general.LanceMode;
            _app.Cells[row, 17] = general.TractMode;
            _app.Cells[row, 18] = GetQuality(general.Quality.OffGasQuality);
            _app.Cells[row, 19] = GetQuality(general.HeatQuality.TransDelay<25);
            _app.Cells[row, 20] = CheckForNullData(general.HeatQuality.TransDelay);

            _app.Cells[row, 21] = (general.Quality.MinLanceTempIn >= 10 && general.Quality.MaxLanceTempIn <= 60) ? "1" : "0";
            _app.Cells[row, 22] = (general.Quality.MinLanceTempOut >= 10 && general.Quality.MaxLanceTempOut <= 60) ? "1" : "0";
            if (general.Zond1 == null)
            {
                _app.Cells[row, 23] = 0;
                _app.Cells[row, 24] = 0;
            }
            if (general.Zond1 != null && general.Zond2 == null)
            {
                _app.Cells[row, 23] = GetQuality(general.Zond1.ZondTemp > 1600 && general.Zond1.ZondTemp <= 1760);
                _app.Cells[row, 24] = GetQuality(general.Zond1.ZondC > 0.03);
            }
            if (general.Zond1 != null && general.Zond2 != null)
            {
                _app.Cells[row, 23] = GetQuality((general.Zond1.ZondTemp > 1600 && general.Zond1.ZondTemp <= 1760) && (general.Zond2.ZondTemp > 1600 && general.Zond2.ZondTemp <= 1760));
                _app.Cells[row, 24] = GetQuality(general.Zond1.ZondC > 0.03 && general.Zond2.ZondC > 0.03);
            }
            _app.Cells[row, 25] = GetQuality(general.SteelXim1 != null && general.SteelXim1.C > 0.03);
            _app.Cells[row, 26] = GetQuality(general.SteelXim1 != null &&
                           (general.SteelXim1.Si > -1 && general.SteelXim1.Mn > -1 && general.SteelXim1.P > -1 &&
                            general.SteelXim1.S > -1 && general.SteelXim1.Cr > -1 && general.SteelXim1.Ni > -1 &&
                            general.SteelXim1.C > -1&& general.SteelXim1.Al>-1));
         
            _app.Cells[row, 27] = GetQuality(general.SlagXim1 != null &&
                           (general.SlagXim1.Al2O3 > 0 && general.SlagXim1.CaO > 0 && general.SlagXim1.FeO > 0 &&
                            general.SlagXim1.MgO > 0 && general.SlagXim1.P2O5 > 0 && general.SlagXim1.S > 0 &&
                            general.SlagXim1.SiO2 > 0 && general.SlagXim1.CaOdivSiO2>0&&general.SlagXim1.MnO>0));
            _app.Cells[row, 28] = GetQuality(general.SteelXim2 != null && general.SteelXim2.C > 0.03);
            _app.Cells[row, 29] = GetQuality(general.SteelXim2 != null &&
                           (general.SteelXim2.Si > -1 && general.SteelXim2.Mn > -1 && general.SteelXim2.P > -1 &&
                            general.SteelXim2.S > -1 && general.SteelXim2.Cr > -1 && general.SteelXim2.Ni > -1 &&
                            general.SteelXim2.C > -1 && general.SteelXim2.Al > -1));
            _app.Cells[row, 30] = GetQuality(general.SlagXim2 != null &&
                           (general.SlagXim2.Al2O3 > 0 && general.SlagXim2.CaO > 0 && general.SlagXim2.FeO > 0 &&
                            general.SlagXim2.MgO > 0 && general.SlagXim2.P2O5 > 0 && general.SlagXim2.S > 0 &&
                            general.SlagXim2.SiO2 > 0&&general.SlagXim2.CaOdivSiO2>0&&general.SlagXim2.MnO>0));

            InstantLogger.msg("{0} Export quality control. Done", general.HeatNumber);
        }
        public void ExportChemestries(List<AdditionalChemestry> additionalChemestries, List<ScrapChemestry> scrapChemestries)
        {
            InstantLogger.msg("Export Chemestries.");
            _wb.Worksheets[13].Select();
            for (int index = 0; index < additionalChemestries.Count; index++)
            {
                var additionalChemestry = additionalChemestries[index];
                _app.Cells[index + 3, 1] = additionalChemestry.Code;
                _app.Cells[index + 3, 2] = additionalChemestry.Name;
                _app.Cells[index + 3, 3] = additionalChemestry.Element;
                _app.Cells[index + 3, 4] = CheckForNullData(additionalChemestry.Value);

            }

            for (int index = 0; index < scrapChemestries.Count; index++)
            {
                var scrapChemestry = scrapChemestries[index];
                _app.Cells[index + 3, 5] = scrapChemestry.Code;
                _app.Cells[index + 3, 6] = scrapChemestry.Name;
                _app.Cells[index + 3, 7] = scrapChemestry.Element;
                _app.Cells[index + 3, 8] = CheckForNullData(scrapChemestry.Value);
            }
            InstantLogger.msg("Export Chemestries DONE.");
        }
    }
}
