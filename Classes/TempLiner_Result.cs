﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
  public  class TempLiner_Result
    {
      public DateTime TimeTemp { get; set; }
      /// <summary>
      /// Т расчетная, градусы С
      /// </summary>
      public double Temperature { get; set; }

    }
}
