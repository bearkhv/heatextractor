﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
    public class OffGasAnalysis
    {
            public double H2 { get; set; }

            public double O2 { get; set; }

            public double CO { get; set; }

            public double CO2 { get; set; }

            public double N2 { get; set; }

            public double Ar { get; set; }
            public double Delay1 { get; set; }
            public double Delay2 { get; set; }
            public double Brantch { get; set; }
            public double Error { get; set; }
            public DateTime Time { get; set; }
        
        
    }
}
