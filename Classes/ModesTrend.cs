﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
    public class ModesTrend
    {
        public DateTime Time { get; set; }
        public int Ignition { get; set; }
        public int SlagOut { get; set; }
        public int Lanse { get; set; }
        public int O2Flow { get; set; }
        public int Tract { get; set; }
        public int Angle { get; set; }

    }
}
