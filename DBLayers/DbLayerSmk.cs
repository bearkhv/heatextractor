﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using HeatExtractor.Classes;
using Implements;
using Oracle.DataAccess.Client;

namespace HeatExtractor.DBLayers
{
    public class DbLayerSmk : DbLayer
    {
        private General General { get; set; }

       public  DbLayerSmk()
        {
           // Instance = new DbLayerSmk();
        }

       // public static DbLayerSmk Instance { get; private set; }

        public General GetSmkData(General general)
        {
            InstantLogger.log("",string.Format("{0}. GET MAIN DATA",general.HeatNumber));
            General = general;
            InstantLogger.msg("{0}. get heats", general.HeatNumber);
            GetHeats();
            InstantLogger.msg("{0}. get grade spec", general.HeatNumber);
            GetGradeSpec();
            InstantLogger.msg("{0}. get hot metal", general.HeatNumber);
            GetHeatHotMetal();
            InstantLogger.msg("{0}. get hot metal lades", general.HeatNumber);
            GetHotMetalLadles();
            InstantLogger.msg("{0}. get scrap", general.HeatNumber);
            GetScrap();
            InstantLogger.msg("{0}. get tap", general.HeatNumber);
            GetTap();
            InstantLogger.msg("{0}. get Otk", general.HeatNumber);
            GetOtk();
            InstantLogger.msg("{0}. get blow", general.HeatNumber);
            GetBlow();
            InstantLogger.msg("{0}. get slag", general.HeatNumber);
            GetSlag();
            InstantLogger.msg("{0}. get zond", general.HeatNumber);
            GetZond();
            InstantLogger.msg("{0}. get blowings", general.HeatNumber);
            GetBlowings();
            InstantLogger.msg("{0}. get correction", general.HeatNumber);
            GetCorection();
            InstantLogger.msg("{0}. get addition cv tor", general.HeatNumber);
            GetAdditionCvTor();
            InstantLogger.msg("{0}. get addition cv", general.HeatNumber);
            GetAdditionCv();
            InstantLogger.msg("{0}. get addition ldl", general.HeatNumber);
            GetAdditionLdl();
            InstantLogger.log("", string.Format("{0}. GET MAIN DATA. DONE", general.HeatNumber));
            return General;
        }

        public List<General> GetHeatId(int haetNoStart, int heatNoEnd)
        {
            const string sql = "SELECT HEAT_ID, HEAT_NO, CV_NO, DT_BEGIN, DT_END FROM HEATS " +
                               "WHERE HEAT_NO BETWEEN :ISTART AND :IEND ";
            var param = new List<OracleParameter>
                            {
                                SetParams("ISTART", haetNoStart),
                                SetParams("IEND", heatNoEnd)
                            };
            return GetHeatId(sql, param);
        }

        public List<General> GetHeatId(DateTime start, DateTime end, string cnvNo)
        {
            
            var sql = string.Format("SELECT HEAT_ID, HEAT_NO, CV_NO, DT_BEGIN, DT_END FROM HEATS " +
                                    "WHERE DT_BEGIN BETWEEN :DSTART AND :DEND AND DT_END BETWEEN :DSTART AND :DEND AND CV_NO in ({0})", cnvNo);
            var param = new List<OracleParameter>
                            {
                                SetParams("DSTART", start),
                                SetParams("DEND", end),
                            };
            return GetHeatId(sql, param);
        }

        private List<General> GetHeatId(string sql, List<OracleParameter> param)
        {
            var general = new List<General>();
            
                var reader = Execute(sql, param);
          try
          {
                while (reader.Read())
                {
                    general.Add(new General
                        {
                            HeatId = CheckNubmerForNullAsInt(reader[0].ToString()),
                            HeatNumber = CheckNubmerForNullAsInt(reader[1].ToString()),
                            CnvNo = CheckNubmerForNullAsInt(reader[2].ToString()),
                            HeatDuration = new Duration
                                {
                                    Start = CheckDateForNull(reader[3].ToString()),
                                    End = CheckDateForNull(reader[4].ToString()),
                                }
                        });
                }
                reader.Close();
            }
            catch (Exception exception)
            {
                reader.Close();
                InstantLogger.err(exception.ToString());
            }
            return general;
        }

        private void GetHeats()
        {
            
                const string sql = "SELECT GRADE_ID, TEAM_NAME, LANCE_NO, CONVERTER_LIFE " +
                                   "FROM HEATS WHERE HEAT_ID = :HEAT_ID ";
                var param = new List<OracleParameter> {SetParams("HEAT_ID", General.HeatId)};
                var read = Execute(sql, param);
           try
           {
                if (read.Read())
                {
                    General.HeatDuration.Period = General.HeatDuration.End - General.HeatDuration.Start;
                    General.GradeId = CheckNubmerForNullAsInt(read[0].ToString());
                    General.Team = read[1].ToString();
                    General.LanceNumber = CheckNubmerForNullAsInt(read[2].ToString());
                    General.ConverterLife = CheckNubmerForNullAsInt(read[3].ToString());
                    if (General.HeatDuration.Start.Hour > 7 && General.HeatDuration.End.Hour < 15)
                    {
                        General.Gang = "1";
                    }
                    else if (General.HeatDuration.Start.Hour > 15 && General.HeatDuration.End.Hour < 23)
                    {
                        General.Gang = "2";
                    }
                    else
                    {
                        General.Gang = "3";
                    }
                }
                read.Close();
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
        }

        private void GetGradeSpec()
        {
            
                const string sql = "SELECT NAME_OTHER, GRADE_GROUP FROM GRADE_SPEC WHERE GRADE_ID = :GRADE_ID ";
                var param = new List<OracleParameter> {SetParams("GRADE_ID", General.GradeId)};
                var read = Execute(sql, param);
           try
           {
                if (read.Read())
                {
                    General.Grade = read[0].ToString();
                    //  General.GradeGroup = read[1].ToString();
                }
                read.Close();
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
        }

        private void GetHeatHotMetal()
        {
            
                const string sql = "SELECT CHGTIME, TEMP, WEIGHT, B_DATE, T_CRANE, NETTO, CHGL_ID " +
                                   "FROM HEAT_HOTMETAL WHERE HEAT_ID =  :HEAT_ID ";
                var param = new List<OracleParameter> {SetParams("HEAT_ID", General.HeatId)};
                var read = Execute(sql, param);
           try
           {
                if (read.Read())
                {
                    General.HotMetalTime = CheckDateForNull(read[0].ToString());
                    General.HotMetalTemp = CheckNubmerForNullAsDouble(read[1].ToString());
                    General.HotMetalLadleWeight = CheckNubmerForNullAsDouble(read[2].ToString());
                    General.HotMetalWeightTime = CheckDateForNull(read[3].ToString());
                    General.HotMetalKranNumber = CheckNubmerForNullAsInt(read[4].ToString());
                    General.HotMetalWeight = CheckNubmerForNullAsDouble(read[5].ToString());
                    General.LadleId = CheckNubmerForNullAsInt(read[6].ToString());
                }
                read.Close();
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
        }

        private void GetHotMetalLadles()
        {
            
                const string sql = "SELECT CHGL_NO FROM HOTMETAL_LADLES WHERE CHGL_ID = :CHGL_ID ";
                var param = new List<OracleParameter> {SetParams("CHGL_ID", General.LadleId)};
                var read = Execute(sql, param);
            try
            {
                if (read.Read())
                {
                    General.HotMetalLadle = CheckNubmerForNullAsInt(read[0].ToString());
                }
                read.Close();
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
        }

        private void GetScrap()
        {
            
                var res = new List<Scrap>();
                const string sql = "SELECT SB.CHARGINGTIME, SB.BUCKET_ID, MS.NAME, SBM.WEIGHT, MS.SCRAP_SORT " +
                                   "FROM SCRAP_BUCKETS SB " +
                                   "LEFT OUTER JOIN SCRAP_BUCKETMATS SBM ON SB.BUCKET_ID = SBM.BUCKET_ID " +
                                   "LEFT OUTER JOIN MATERIAL_SPEC MS ON SBM.MAT_ID = MS.MAT_ID " +
                                   "WHERE SB.HEAT_ID = :HEAT_ID";
                var param = new List<OracleParameter> {SetParams("HEAT_ID", General.HeatId)};
                var read = Execute(sql, param);
          try
          {
                while (read.Read())
                {
                    res.Add(new Scrap
                        {
                            Time = CheckDateForNull(read[0].ToString()),
                            BucketId = CheckNubmerForNullAsInt(read[1].ToString()),
                            ScrapName = read[2].ToString(),
                            ScrapWaight = CheckNubmerForNullAsDouble(read[3].ToString()),
                            ScrapType = read[4].ToString()
                        });
                }
                read.Close();
                if (res.Count == 0) return;
                General.ScrapBucket = res.GroupBy(x => x.BucketId).Count();
                General.ScrapTime = res.Max(x => x.Time);
                General.ScrapWeight = res.Sum(x => x.ScrapWaight);
                string mix = null;
                string mixW = null;
                foreach (var scrap in res)
                {
                    mix = mix + ", " + scrap.ScrapName;
                    mixW = mixW + ", " + scrap.ScrapWaight;
                }
                if (mix != null) General.ScrapMix = mix.Remove(0, 2);
                if (mixW != null) General.ScrapMixWeight = mixW.Remove(0, 2);
                General.Scraps = res;
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
        }

        private void GetTap()
        {
            General.OutDuration = new Duration
                                      {
                                          Start = GetEventTimeStart(General.HeatId, "TAPS"),
                                          End = GetEventTimeEnd(General.HeatId, "TAPE"),
                                      };
            General.OutDuration.Period = General.OutDuration.End - General.OutDuration.Start;
        }

        private void GetOtk()
        {
            General.OutOtkDuration = new Duration
                                         {
                                             Start = GetEventTimeStart(General.HeatId, "OTKS"),
                                             End = GetEventTimeEnd(General.HeatId, "OTKE")
                                         };
            General.OutOtkDuration.Period = General.OutOtkDuration.End - General.OutOtkDuration.Start;
        }

        private void GetBlow()
        {
            

            
            General.Blowing = new Blowing
                                  {
                                      Duration = new Duration
                                                     {
                                                         Start = GetEventTimeStart(General.HeatId, "BLOS"),
                                                         End = GetEventTimeEnd(General.HeatId, "BLOE")
                                                     }
                                  };
            General.Blowing.Duration.Period = General.Blowing.Duration.End - General.Blowing.Duration.Start;
            var param = new List<OracleParameter> {SetParams("HEAT_ID", General.HeatId)};
     
            var sql = "SELECT COUNT(EVENT_ID) FROM HEAT_EVENTS WHERE TYPE in ('BLOS') AND HEAT_ID = :HEAT_ID";
            var read = Execute(sql, param);
       try
       {
            if (read.Read())
            {
                General.Blowing.Count = CheckNubmerForNullAsInt(read[0].ToString());
            }
            read.Close();
            string EventTime = "";
            sql =
                "SELECT EVENTTIME FROM HEAT_EVENTS WHERE HEAT_ID = :HEAT_ID AND TYPE = ('MBLS') ORDER BY EVENTTIME";
            read = Execute(sql, param);
            if (read.Read())
            {
                EventTime =  read[0].ToString();
            }
            read.Close();
           
            sql =
                string.Format("SELECT MAX(VALUE1) FROM HEAT_EVENTS WHERE HEAT_ID = :HEAT_ID AND TYPE = ('BLOE') AND PAR_NAME1 = 'ACT_O2VOL' AND EVENTTIME > :DATENEW ");
               var param1 =  new OracleParameter("DATENEW",OracleDbType.Date);
                param1.Value = CheckDateForNull(EventTime);
                param.Add(param1);
                read = Execute(sql, param);
            if (read.Read())
            {
                General.Blowing.O2Flow = CheckNubmerForNullAsInt(read[0].ToString());
            }
            read.Close();


            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
        }

        private void GetSlag()
        {
            General.SlagOutDuration = new Duration
                                          {
                                              Start = GetEventTimeStart(General.HeatId, "DSLS"),
                                              End = GetEventTimeEnd(General.HeatId, "DSLE")
                                          };
            General.SlagOutDuration.Period = General.SlagOutDuration.End - General.SlagOutDuration.Start;
        }

        private DateTime GetEventTimeStart(int heatId, string eventType)
        {
            const string sql = "SELECT MIN(EVENTTIME) FROM HEAT_EVENTS WHERE HEAT_ID = :HEAT_ID AND TYPE = :TYPE";
            return GetEventTime(sql, heatId, eventType);
        }

        private DateTime GetEventTimeEnd(int heatId, string eventType)
        {
            const string sql = "SELECT MAX(EVENTTIME) FROM HEAT_EVENTS WHERE HEAT_ID = :HEAT_ID AND TYPE = :TYPE";
            return GetEventTime(sql, heatId, eventType);
        }

        private DateTime GetEventTime(string sql, int heatId, string eventType)
        {

                var res = DateTime.MinValue;
           
                var param = new List<OracleParameter>
                    {
                        SetParams("HEAT_ID", heatId),
                        SetParams("TYPE", eventType)
                    };
                var read = Execute(sql, param);
           try
           {
                if (read.Read())
                {
                    res = CheckDateForNull(read[0].ToString());
                }
                read.Close();
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
            return res;
        }

        private void GetZond()
        {
            
                var res = new List<Zond>();
                const string sql =
                    "SELECT INSERTTIME, CARBON, TEMP, OXYGEN FROM HEAT_MEASUREMENTS WHERE TYPE = 'AUTO' AND HEAT_ID = :HEAT_ID";
                var param = new List<OracleParameter> {SetParams("HEAT_ID", General.HeatId)};
                var read = Execute(sql, param);
           try
           {
                while (read.Read())
                {
                    res.Add(new Zond
                        {
                            ZondStart = CheckDateForNull(read[0].ToString()),
                            ZondC = CheckNubmerForNullAsDouble(read[1].ToString()),
                            ZondTemp = CheckNubmerForNullAsDouble(read[2].ToString()),
                            Oxigen = CheckNubmerForNullAsDouble(read[3].ToString())
                        }
                        );
                }
                read.Close();
                if (res.Count != 0)
                {
                    General.Zond1 = res[0];
                }
                if (res.Count > 1)
                {
                    General.Zond2 = res[1];
                }
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
        }

        private void GetBlowings()
        {
            
                var res = new List<Blowing>();
                const string sql = "SELECT DTSTART,  DTEND, O2VOL FROM HEAT_BLASENS WHERE  HEAT_ID = :HEAT_ID";
                var param = new List<OracleParameter> {SetParams("HEAT_ID", General.HeatId)};
                var read = Execute(sql, param);
          try
          {
                while (read.Read())
                {
                    res.Add(new Blowing
                        {
                            Duration = new Duration
                                {
                                    Start = CheckDateForNull(read[0].ToString()),
                                    End = CheckDateForNull(read[1].ToString()),
                                    Period = CheckDateForNull(read[1].ToString()) - CheckDateForNull(read[0].ToString())
                                },
                            O2Flow = CheckNubmerForNullAsDouble(read[2].ToString())
                        });
                }
                read.Close();
                General.BlowingCount = res.Count;
                if (res.Count != 0)
                {
                    General.Blowing1 = res[0];
                }
                if (res.Count > 1)
                {
                    General.Blowing2 = res[1];
                }
                if (res.Count > 2)
                {
                    General.Blowing3 = res[2];
                }
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
        }

        private void GetCorection()
        {
            if (General.Zond2 != null)
            {
                General.CorrDeltaT = General.Zond2.ZondTemp - General.Zond1.ZondTemp;
                General.CorrDeltaC = General.Zond2.ZondC - General.Zond1.ZondC;
            }
            if (General.Blowing2 != null && General.Blowing3 != null && General.Blowing2.O2Flow > General.Blowing1.O2Flow)
            {
                General.CorrO2 = General.Blowing3.O2Flow;
            }
            if (General.Blowing2 != null && General.Blowing2.O2Flow < General.Blowing1.O2Flow)
            {
                General.CorrO2 = General.Blowing2.O2Flow;
            }
        }

        private void GetAdditionLdl()
        {
            
                var i = 0;
                //   const string sql = "SELECT MAT_ID, SUM(PORTION_WGT) FROM HEAT_ADDITIONS_ACT WHERE DESTINATION_AGGNO = 'LDL' AND HEAT_ID = :HEAT_ID GROUP BY MAT_ID";
                const string sql =
                    "SELECT MAT_ID, SUM(PORTION_WGT) FROM HEAT_ADDITIONS_ACT WHERE DESTINATION_AGGNO = 'LDL' AND HEAT_ID = :HEAT_ID GROUP BY MAT_ID";

                var param = new List<OracleParameter> {SetParams("HEAT_ID", General.HeatId)};
                var read = Execute(sql, param);
          try
          {
                General.Bucket = new Additon();
                while (read.Read())
                {
                    var matId = CheckNubmerForNullAsInt(read[0].ToString());
                    switch (matId)
                    {
                        case 5:
                            {
                                General.Bucket.Fesi65 = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 49:
                            {
                                General.Bucket.Fesicr = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 67:
                            {
                                General.Bucket.Mn95 = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 89:
                            {
                                General.Bucket.Koks = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 198:
                            {
                                General.Bucket.Izvest = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 226:
                            {
                                General.Bucket.Smn17 = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 235:
                            {
                                General.Bucket.Al2 = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 255:
                            {
                                General.Bucket.AlKat = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 258:
                            {
                                General.Bucket.Femn82 = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 237:
                            {
                                General.Bucket.Femn78 = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 238:
                            {
                                General.Bucket.Alsech = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 239:
                            {
                                General.Bucket.Ni = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 240:
                            {
                                General.Bucket.Fev50 = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 242:
                            {
                                General.Bucket.Femo60 = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 244:
                            {
                                General.Bucket.Fep = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 245:
                            {
                                General.Bucket.Fecr = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 246:
                            {
                                General.Bucket.Sica = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 250:
                            {
                                General.Bucket.Fenb = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 251:
                            {
                                General.Bucket.Sic = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 254:
                            {
                                General.Bucket.Cu = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 261:
                            {
                                General.Bucket.Albr = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 267:
                            {
                                General.Bucket.Fev80 = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 275:
                            {
                                General.Bucket.Fesi75 = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 289:
                            {
                                General.Bucket.Alpir = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 302:
                            {
                                General.Bucket.Mnbrik = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 329:
                            {
                                General.Bucket.Sicbr = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 341:
                            {
                                General.Bucket.Femn88 = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 368:
                            {
                                General.Bucket.Grmagn = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 369:
                            {
                                General.Bucket.Ligat2 = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 372:
                            {
                                General.Bucket.Feni = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 373:
                            {
                                General.Bucket.Mnazot = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 374:
                            {
                                General.Bucket.Nilom = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 379:
                            {
                                General.Bucket.Mo99 = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 380:
                            {
                                General.Bucket.Abk65 = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 381:
                            {
                                General.Bucket.Feti30 = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 382:
                            {
                                General.Bucket.Fenb50 = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                    }
                    i++;
                }
                read.Close();
                General.Bucket.MatCount = i;
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
        }

        private void GetAdditionCvTor()
        {
            
                var tor = GetTor();
                if (tor == null) return;
                const string sql = "SELECT MAT_ID, SUM(PORTION_WGT) FROM HEAT_ADDITIONS_ACT " +
                                   "WHERE DESTINATION_AGGNO = 'CV' AND INSERTTIME BETWEEN :DSTART AND :DEND AND HEAT_ID = :HEAT_ID " +
                                   "GROUP BY MAT_ID";
                var param = new List<OracleParameter>
                    {
                        SetParams("DSTART", tor.Start),
                        SetParams("DEND", tor.End),
                        SetParams("HEAT_ID", General.HeatId)
                    };
                var read = Execute(sql, param);
           try
           {
                General.Gunning = new Additon();
                var i = 0;
                while (read.Read())
                {
                    var matId = CheckNubmerForNullAsInt(read[0].ToString());
                    switch (matId)
                    {
                        case 89:
                            {
                                General.Gunning.Koks = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 198:
                            {
                                General.Gunning.Izvest = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 200:
                            {
                                General.Gunning.Doloms = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 252:
                            {
                                General.Gunning.Dolomit = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 272:
                            {
                                General.Gunning.CaC = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 383:
                            {
                                General.Gunning.MaxG = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                    }
                    i++;
                }
                read.Close();
                General.Gunning.MatCount = i;
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
        }
        
        private void GetAdditionCv()
        {
            
                string sql;
                var i = 0;
                var param = new List<OracleParameter>();
                var tor = GetTor();
                if (tor != null)
                {
                    sql = "SELECT MAT_ID, SUM(PORTION_WGT) FROM HEAT_ADDITIONS_ACT " +
                          "WHERE DESTINATION_AGGNO = 'CV' AND INSERTTIME > :DEND AND HEAT_ID = :HEAT_ID " +
                          "GROUP BY MAT_ID";
                    param.Add(SetParams("DEND", tor.End));
                    param.Add(SetParams("HEAT_ID", General.HeatId));
                }
                else
                {
                    sql = "SELECT MAT_ID, SUM(PORTION_WGT) FROM HEAT_ADDITIONS_ACT " +
                          "WHERE DESTINATION_AGGNO = 'CV' AND HEAT_ID = :HEAT_ID " +
                          "GROUP BY MAT_ID";
                    param.Add(SetParams("HEAT_ID", General.HeatId));
                }
                var read = Execute(sql, param);
           try
           {
                General.Converter = new Additon();
                while (read.Read())
                {
                    var matId = CheckNubmerForNullAsInt(read[0].ToString());
                    switch (matId)
                    {
                        case 89:
                            {
                                General.Converter.Koks = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 198:
                            {
                                General.Converter.Izvest = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 200:
                            {
                                General.Converter.Doloms = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 252:
                            {
                                General.Converter.Dolomit = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 254:
                            {
                                General.Converter.Cu = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 261:
                            {
                                General.Converter.Albr = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 262:
                            {
                                General.Converter.Fom = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 264:
                            {
                                General.Converter.AlKonc = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 265:
                            {
                                General.Converter.Aglom = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 275:
                            {
                                General.Converter.Fesi75 = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 342:
                            {
                                General.Converter.Feruda = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 374:
                            {
                                General.Converter.Nilom = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 383:
                            {
                                General.Converter.MaxG = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 384:
                            {
                                General.Converter.CmG = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 387:
                            {
                                General.Converter.Flgl = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 388:
                            {
                                General.Converter.Teramr = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 389:
                            {
                                General.Converter.DolomitLight = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 392:
                            {
                                General.Converter.Mmkn75 = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 393:
                            {
                                General.Converter.Rant70 = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                        case 394:
                            {
                                General.Converter.Flmg1 = CheckNubmerForNullAsDouble(read[1].ToString());
                                break;
                            }
                    }
                    i++;
                }
                read.Close();
                General.Converter.MatCount = i;
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
        }

        private Duration GetTor()
        {
           
                const string sql = "SELECT STARTTIME, ENDTIME FROM  SLAGBLOWING  WHERE  HEAT_ID = :HEAT_ID ";
                var param = new List<OracleParameter> {SetParams("HEAT_ID", General.HeatId)};
                var read = Execute(sql, param);
           try
           {
                if (read.Read())
                {
                    return new Duration
                        {
                            Start = CheckDateForNull(read[0].ToString()),
                            End = CheckDateForNull(read[1].ToString())
                        };
                }
                read.Close();
            }
            catch (Exception exception)
            {
                read.Close();
                InstantLogger.err(exception.ToString());
            }
            return null;
        }
    }
}
