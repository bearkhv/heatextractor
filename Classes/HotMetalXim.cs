﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
    /// <summary>
    /// Чугун. Проба.
    /// </summary>
    public class HotMetalXim
    {
        /// <summary>
        ///  Дата ввода
        /// </summary>
        public DateTime Time { get; set; }
        /// <summary>
        /// Миксер
        /// </summary>
        public string Torpedos { get; set; }
        /// <summary>
        /// Ковш
        /// </summary>
        public int Ladles { get; set; }
        /// <summary>
        /// C
        /// </summary>
        public double C { get; set; }
        /// <summary>
        /// SI
        /// </summary>
        public double Si { get; set; }
        /// <summary>
        /// MN
        /// </summary>
        public double Mn { get; set; }
        /// <summary>
        /// P
        /// </summary>
        public double P { get; set; }
        /// <summary>
        /// S
        /// </summary>
        public double S { get; set; }
    }
}
