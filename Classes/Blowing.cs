﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
    /// <summary>
    /// Продувка
    /// </summary>
    public class Blowing
    {
        /// <summary>
        /// время
        /// </summary>
        public Duration Duration { get; set; }
        /// <summary>
        /// Расход кислорода
        /// </summary>
        public double O2Flow { get; set; }
        /// <summary>
        /// Количество продувок
        /// </summary>
        public int Count { get; set; }
    }
}
