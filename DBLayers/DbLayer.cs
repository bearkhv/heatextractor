﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Implements;
using Oracle.DataAccess.Client;

namespace HeatExtractor.DBLayers
{
    public class DbLayer
    {

        private static OracleConnection Connection { get; set; }

        public DbLayer()
        {
           Connection = new OracleConnection(System.Configuration.ConfigurationManager.OpenExeConfiguration("").AppSettings.Settings["ConnectionString"].Value);
        }


        protected int CheckNubmerForNullAsInt(string value)
        {
            return !string.IsNullOrEmpty(value) ? Convert.ToInt32(value) :  -1;
        }

        protected int CheckBoolForNullAsInt(string value)
        {
           if (!string.IsNullOrEmpty(value))
           {
               bool result;
              if  (bool.TryParse(value, out result))
                  return result ? 1 : 0;

           }
            return -1;
        }

        protected float CheckNubmerForNullAsFloat(string value)
        {
            return (float) (!string.IsNullOrEmpty(value) ? Convert.ToDouble(value) : -1);
        }

        protected double CheckNubmerForNullAsDouble (string value)
        {
            return !string.IsNullOrEmpty(value) ? Convert.ToDouble(value) : -1;
        }

        protected DateTime CheckDateForNull (string value)
        {
            return !string.IsNullOrEmpty(value) ? Convert.ToDateTime(value) : DateTime.MinValue;
        }

        protected static List<OracleParameter> MandatoryParams(int iCnvNo, DateTime start, DateTime end)
        {
            return new List<OracleParameter>
                       {
                           SetParams("CNV_NO", iCnvNo),
                           SetParams("DSTART", start),
                           SetParams("DEND", end)
                       };
        }
        private List<OracleCommand> oracleCommands = new List<OracleCommand>(); 
        protected OracleDataReader Execute(string sql, List<OracleParameter> parameters)
        {
            try
            {
            if (Connection.State != ConnectionState.Open)
            {
                Connection.Open();
            }
            var currentCommand = oracleCommands.Where(x => x.CommandText == sql).FirstOrDefault();
            if (currentCommand == null)
            {
                currentCommand = Connection.CreateCommand();
                currentCommand.CommandText = sql;
                currentCommand.Parameters.Clear();
                currentCommand.Parameters.AddRange(parameters.ToArray());
                oracleCommands.Add(currentCommand);
                currentCommand.Prepare();
            }
            foreach (var oracleParameter in parameters)
            {
                currentCommand.Parameters[oracleParameter.ParameterName].Value = oracleParameter.Value;
            }
           
                var result = currentCommand.ExecuteReader();
                return result;

            }
            catch (OracleException exception)
            {
                Connection.Close();
                InstantLogger.err(exception.ToString());
                throw;
            }
           
        }

      
        #region SetParams

        public static OracleParameter SetParams(string name, double value)
        {
            return new OracleParameter
                       {
                           ParameterName = name,
                           Direction = ParameterDirection.Input,
                           OracleDbType = OracleDbType.Double,
                           Value = value
                       };
        }

        public static OracleParameter SetParams(string name, decimal value)
        {
            return new OracleParameter
                       {
                           ParameterName = name,
                           Direction = ParameterDirection.Input,
                           OracleDbType = OracleDbType.Decimal,
                           Value = value
                       };
        }

        public static OracleParameter SetParams(string name, float value)
        {
            return new OracleParameter
                       {
                           ParameterName = name,
                           Direction = ParameterDirection.Input,
                           OracleDbType = OracleDbType.Single,
                           Value = value
                       };
        }

        public static OracleParameter SetParams(string name, string value)
        {
            return new OracleParameter
                       {
                           ParameterName = name,
                           Direction = ParameterDirection.Input,
                           OracleDbType = OracleDbType.Varchar2,
                           Value = value
                       };
        }

        public static OracleParameter SetParams(string name, Int16 value)
        {
            return new OracleParameter
                       {
                           ParameterName = name,
                           Direction = ParameterDirection.Input,
                           OracleDbType = OracleDbType.Int16,
                           Value = value
                       };
        }

        public static OracleParameter SetParams(string name, Int32 value)
        {
            return new OracleParameter
                       {
                           ParameterName = name,
                           Direction = ParameterDirection.Input,
                           OracleDbType = OracleDbType.Int32,
                           Value = value
                       };
        }

        public static OracleParameter SetParams(string name, Int64 value)
        {
            return new OracleParameter
                       {
                           ParameterName = name,
                           Direction = ParameterDirection.Input,
                           OracleDbType = OracleDbType.Int64,
                           Value = value
                       };
        }

        public static OracleParameter SetParams(string name, DateTime value)
        {
            return new OracleParameter
                       {
                           ParameterName = name,
                           Direction = ParameterDirection.Input,
                           OracleDbType = OracleDbType.Date,
                           Value = value
                       };
        }

        public static OracleParameter SetParams(string name, bool value)
        {
            return new OracleParameter
                       {
                           ParameterName = name,
                           Direction = ParameterDirection.Input,
                           OracleDbType = OracleDbType.Int32,
                           Value = value ? 1 : 0
                       };
        }

        # endregion

    }
}
