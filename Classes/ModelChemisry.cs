﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
    public class ModelChemisry
    {
        public string Type;
        public Dictionary<string, string> Values = new Dictionary<string, string>();

        public string GetValue(string code)
        {
            if (Values.ContainsKey(code))
                return Values[code];
            return "";
        }
    }
}
