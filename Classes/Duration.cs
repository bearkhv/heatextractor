﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
    /// <summary>
    /// Временные отрезки
    /// </summary>
    public class Duration
    {
        /// <summary>
        /// Начало
        /// </summary>
        public DateTime Start { get; set; }
        /// <summary>
        /// Конец
        /// </summary>
        public DateTime End { get; set; }
        /// <summary>
        /// Длительность
        /// </summary>
        public TimeSpan Period { get; set; }
    }
}
