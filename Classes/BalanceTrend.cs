﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
    public class BalanceTrend
    {
        public DateTime Time { get; set; }
        public double C { get; set; }
        public double T { get; set; }
        public double Si { get; set; }
        public double Mn { get; set; }
        public double Al { get; set; }
        public double Cr { get; set; }
        public double P { get; set; }
        public double Ti { get; set; }
        public double V { get; set; }
        public double Fe { get; set; }
        public double CaO { get; set; }
        public double FeO { get; set; }
        public double SiO2 { get; set; }
        public double MnO { get; set; }
        public double MgO { get; set; }
    }
}
