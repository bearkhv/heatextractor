﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
    /// <summary>
    /// Химия стали
    /// </summary>
    public class SteelXim
    {
        /// <summary>
        /// Дата ввода
        /// </summary>
        public DateTime Time { get; set; }
        /// <summary>
        /// C
        /// </summary>
        public double C { get; set; }
        /// <summary>
        /// SI
        /// </summary>
        public double Si { get; set; }
        /// <summary>
        /// MN
        /// </summary>
        public double Mn { get; set; }
        /// <summary>
        /// P
        /// </summary>
        public double P { get; set; }
        /// <summary>
        /// S
        /// </summary>
        public double S { get; set; }
        /// <summary>
        /// CR
        /// </summary>
        public double Cr { get; set; }
        /// <summary>
        /// NI
        /// </summary>
        public double Ni { get; set; }
        /// <summary>
        /// CU
        /// </summary>
        public double Cu { get; set; }
        /// <summary>
        /// AL
        /// </summary>
        public double Al { get; set; }
        /// <summary>
        /// N
        /// </summary>
        public double N { get; set; }
        /// <summary>
        /// MO
        /// </summary>
        public double Mo { get; set; }

    }
}
