﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
    public class QualityControl
    {
        public double MinOffGasFlow;
        public double MaxOffGasFlow;

        public double MinLanceTempIn;
        public double MaxLanceTempIn;
        public double MinLanceTempOut;
        public double MaxLanceTempOut;

        public string IsAlarmStrop;

       

        public bool OffGasQuality;
    }
}
