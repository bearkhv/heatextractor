﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Converter;

namespace HeatExtractor.Classes
{
    public class OffGasTrend
    {
        public DateTime Time { get; set; }
        public OffGasEvent OffGas { get; set; }
        public BoilerWaterCoolingEvent BoilerWaterCooling { get; set; }
        public DecompressionOffGasEvent DecompressionOffGas { get; set; }
    }
}
