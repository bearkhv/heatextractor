﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
   public class Charge
    {
       public DateTime TimeCharge1 { get; set; }
       /// <summary>
       /// Номер плавки
       /// </summary>
       public int HeatNumber { get; set; }
       /// <summary>
       /// Марка стали (20 символов)
       /// </summary>
       public string SteelGrade1 { get; set; }
       /// <summary>
       /// Марка стали (оставшиеся символы)
       /// </summary>
       public string SteelGrade2 { get; set; }
       /// <summary>
       /// Группа стали
       /// </summary>
       public int SteelGroup { get; set; }
       /// <summary>
       /// Группа стали (Сергея)
       /// </summary>
       public int SteelGroupCell { get; set; }
       /// <summary>
       /// Углерод
       /// </summary>
       public double Target_C { get; set; }
       /// <summary>
       /// Фосфор
       /// </summary>
       public double Target_P { get; set; }
       /// <summary>
       /// FeO
       /// </summary>
       public double Target_FeO { get; set; }
       /// <summary>
       /// MgO
       /// </summary>
       public double Target_MgO { get; set; }
       /// <summary>
       /// CaOdivSiO2
       /// </summary>
       public double Target_CaOdivSiO2 { get; set; }
       /// <summary>
       /// Темпиратура
       /// </summary>
       public double Target_Temperature { get; set; }
       /// <summary>
       /// Текущий чугун
       /// </summary>
       public double Result_Iron { get; set; }
       /// <summary>
       /// Текущий лом
       /// </summary>
       public double Result_Scrap { get; set; }
       /// <summary>
       /// Шихта
       /// </summary>
       public int Charge_ { get; set; }
       /// <summary>
       /// Шихта 1, чугун
       /// </summary>
       public double Balance_Iron { get; set; }
       /// <summary>
       /// Шихта 1, лом
       /// </summary>
       public double Balance_Scrap { get; set; }
       /// <summary>
       /// Шихта 6, чугун (Куркин)
       /// </summary>
       public double Statistic_K_Iron { get; set; }
       /// <summary>
       /// Шихта 6, лом (Куркин)
       /// </summary>
       public double Statistic_K_Scrap { get; set; }
       /// <summary>
       /// Шихта 6, чугун (Анатолий)
       /// </summary>
       public double Statistic_AB_Iron { get; set; }
       /// <summary>
       /// Шихта 6, лом (Анатолий)
       /// </summary>
       public double Statistic_AB_Scrap { get; set; }
       /// <summary>
       /// Шихта 2, вес материала (Известь)
       /// </summary>
       public double FluxWeight1_2 { get; set; }
       /// <summary>
       /// Шихта 2, вес материала (Фом)
       /// </summary>
       public double FluxWeight2_2 { get; set; }
       /// <summary>
       /// Шихта 7, вес материала (Фом)
       /// </summary>
       public double FluxWeight1_7 { get; set; }
       /// <summary>
       /// Шихта 7, вес материала (Известь)
       /// </summary>
       public double FluxWeight2_7 { get; set; }
    }
}
