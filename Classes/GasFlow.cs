﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Converter;

namespace HeatExtractor.Classes
{
    public class GasFlow
    {
        public DateTime Time { get; set; }
        public double O2Flow { get; set; }
        public double O2Pressure { get; set; }
        public double O2Drop { get; set; }
        public double O2Temp { get; set; }
        public string Lance { get; set; }
        public double N2FlowWnd { get; set; }
        public double N2PressureWnd { get; set; }
        public double N2DropWnd { get; set; }
        public double N2TempWnd { get; set; }
        public double N2FlowBlr { get; set; }
        public double N2PressureBlr { get; set; }
        public double N2DropBlr { get; set; }
        public double N2TempBlr { get; set; }
        public double N2FlowLeak { get; set; }
        public double N2PressureLeak { get; set; }
        public double N2DropLeak { get; set; }
        public double N2TempLeak { get; set; }
    }
}
