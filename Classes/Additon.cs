﻿namespace HeatExtractor.Classes
{
    /// <summary>
    /// Материалы
    /// </summary>
    public class Additon
    {
        public Additon()
        {
            Smn17 = -1;
            Fesi65 = -1;
            Al2 = -1;
            AlKat = -1;
            Femn82 = -1;
            Femn78 = -1;
            Alsech = -1;
            Ni = -1;
            Fev50 = -1;
            Fesicr = -1;
            Mn95 = -1;
            Femo60 = -1;
            Fep = -1;
            Fecr = -1;
            Sica = -1;
            Dolomit = -1;
            Fenb = -1;
            Sic = -1;
            Koks = -1;
            Izvest = -1;
            Doloms = -1;
            Cu = -1;
            Albr = -1;
            Fom = -1;
            AlKonc = -1;
            Aglom = -1;
            Fev80 = -1;
            CaC = -1;
            Fesi75 = -1;
            Alpir = -1;
            Mnbrik = -1;
            Sicbr = -1;
            Femn88 = -1;
            Grmagn = -1;
            Ligat2 = -1;
            Feni = -1;
            Mnazot = -1;
            Nilom = -1;
            Menisp = -1;
            Mo99 = -1;
            Abk65 = -1;
            Feti30 = -1;
            Fenb50 = -1;
            MaxG = -1;
            CmG = -1;
            Feruda = -1;
            Flgl = -1;
            Teramr = -1;
            DolomitLight = -1;
            Mmkn75 = -1;
            Rant70 = -1;
            Flmg1 = -1;
        }
        
        /// <summary>
        /// SMN17 - ковш
        /// </summary>
        public double Smn17 { get; set; }
        /// <summary>
        /// FESI65 - ковш
        /// </summary>
        public double Fesi65 { get; set; }
        /// <summary>
        /// AL2 - ковш
        /// </summary>
        public double Al2 { get; set; }
        /// <summary>
        /// AL KAT - ковш
        /// </summary>
        public double AlKat { get; set; }
        /// <summary>
        /// FEMN82 - ковш
        /// </summary>
        public double Femn82 { get; set; }
        /// <summary>
        /// FEMN78 - ковш
        /// </summary>
        public double Femn78 { get; set; }
        /// <summary>
        /// ALSECH - ковш
        /// </summary>
        public double Alsech { get; set; }
        /// <summary>
        /// NI - ковш
        /// </summary>
        public double Ni { get; set; }
        /// <summary>
        /// FEV50 - ковш
        /// </summary>
        public double Fev50 { get; set; }
        /// <summary>
        /// FESICR - ковш
        /// </summary>
        public double Fesicr { get; set; }
        /// <summary>
        /// MN95 - ковш
        /// </summary>
        public double Mn95 { get; set; }
        /// <summary>
        /// FEMO60 - ковш
        /// </summary>
        public double Femo60 { get; set; }
        /// <summary>
        /// FEP - ковш
        /// </summary>
        public double Fep { get; set; }
        /// <summary>
        /// FECR - ковш
        /// </summary>
        public double Fecr { get; set; }
        /// <summary>
        /// SICA - ковш
        /// </summary>
        public double Sica { get; set; }
        /// <summary>
        /// ДОЛМИТ - конвертер, теркретирование
        /// </summary>
        public double Dolomit { get; set; }
        /// <summary>
        /// FENB - ковш
        /// </summary>
        public double Fenb { get; set; }
        /// <summary>
        /// SIC - ковш
        /// </summary>
        public double Sic { get; set; }
        /// <summary>
        /// KOKS - ковш, конвертер, теркретирование
        /// </summary>
        public double Koks { get; set; }
        /// <summary>
        /// ИЗВЕСТЬ - ковш, конвертер, теркретирование
        /// </summary>
        public double Izvest { get; set; }
        /// <summary>
        /// ДОЛОМС - конвертер, теркретирование
        /// </summary>
        public double Doloms { get; set; }
        /// <summary>
        /// CU - ковш, конвертер
        /// </summary>
        public double Cu { get; set; }
        /// <summary>
        /// AL BR - ковш
        /// </summary>
        public double Albr { get; set; }
        /// <summary>
        /// ФОМ - конвертер
        /// </summary>
        public double Fom { get; set; }
        /// <summary>
        /// ALКонц - конвертер
        /// </summary>
        public double AlKonc { get; set; }
        /// <summary>
        /// АГЛОМ - конвертер
        /// </summary>
        public double Aglom { get; set; }
        /// <summary>
        /// FEV80 - ковш
        /// </summary>
        public double Fev80 { get; set; }
        /// <summary>
        /// CAC - теркретирование
        /// </summary>
        public double CaC { get; set; }
        /// <summary>
        /// FESI75 - ковш
        /// </summary>
        public double Fesi75 { get; set; }
        /// <summary>
        /// ALPIR - ковш
        /// </summary>
        public double Alpir { get; set; }
        /// <summary>
        /// MNBRIK - ковш
        /// </summary>
        public double Mnbrik { get; set; }
        /// <summary>
        /// SIC BR - ковш
        /// </summary>
        public double Sicbr { get; set; }
        /// <summary>
        /// FEMN88 - ковш
        /// </summary>
        public double Femn88 { get; set; }
        /// <summary>
        /// GRMAGN - ковш
        /// </summary>
        public double Grmagn { get; set; }
        /// <summary>
        /// ЛИГАТ2 - ковш
        /// </summary>
        public double Ligat2 { get; set; }
        /// <summary>
        /// FENI - ковш
        /// </summary>
        public double Feni { get; set; }
        /// <summary>
        /// MNAZOT - ковш
        /// </summary>
        public double Mnazot { get; set; }
        /// <summary>
        /// NI LOM - ковш, конвертер
        /// </summary>
        public double Nilom { get; set; }
        /// <summary>
        /// MENISP - конвертер
        /// </summary>
        public double Menisp { get; set; }
        /// <summary>
        /// MO99 - ковш
        /// </summary>
        public double Mo99 { get; set; }
        /// <summary>
        /// ABK65 - ковш
        /// </summary>
        public double Abk65 { get; set; }
        /// <summary>
        /// FETI30 - ковш
        /// </summary>
        public double Feti30 { get; set; }
        /// <summary>
        /// FENB50 - ковш
        /// </summary>
        public double Fenb50 { get; set; }
        /// <summary>
        /// МАХГ - конвертер, теркретирование
        /// </summary>
        public double MaxG { get; set; }
        /// <summary>
        /// СМГ - конвертер
        /// </summary>
        public double CmG { get; set; }
        /// <summary>
        /// FERUDA - конвертер
        /// </summary>
        public double Feruda { get; set; }
        /// <summary>
        /// FLGL - конвертер
        /// </summary>
        public double Flgl { get; set; }
        /// <summary>
        /// TERAMR - конвертер
        /// </summary>
        public double Teramr { get; set; }
        /// <summary>
        /// Доломит мягкого обжига - конвертер
        /// </summary>
        public double DolomitLight { get; set; }
        /// <summary>
        /// MMKN75 - конвертер
        /// </summary>
        public double Mmkn75 { get; set; }
        /// <summary>
        /// RANT70 - конвертер
        /// </summary>
        public double Rant70 { get; set; }
        /// <summary>
        /// FLMG1 - конвертер
        /// </summary>
        public double Flmg1 { get; set; }

        public int  MatCount { get; set; }
    }
}
