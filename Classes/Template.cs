﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatExtractor.Classes
{
    public class Materials
    {
        public string MaterialName;
        public int Bunker;
        public string NotToGive { get; set; }          
        
        
        public string AllowToAdd { get; set; }
        public double Weight;
    }
    public class Template
    {
        public int Step;
        public DateTime Time;
        public int TotalO2Flow;
        public int LancePosition;
        public int O2Flow;
        public List<Materials> Materials = new List<Materials>();
    }
}
